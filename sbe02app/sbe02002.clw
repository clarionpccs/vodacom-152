

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBE02002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBE02003.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Sub_Accounts PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
main_account_temp    STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ViewAccounts         STRING('A')
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?main_account_temp
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Main_Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:Branch)
                       PROJECT(sub:Postcode)
                       PROJECT(sub:Telephone_Number)
                       PROJECT(sub:Fax_Number)
                       PROJECT(sub:Contact_Name)
                       PROJECT(sub:Enquiry_Source)
                       PROJECT(sub:Labour_Discount_Code)
                       PROJECT(sub:Labour_VAT_Code)
                       PROJECT(sub:Account_Type)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:Branch             LIKE(sub:Branch)               !List box control field - type derived from field
sub:Postcode           LIKE(sub:Postcode)             !List box control field - type derived from field
sub:Telephone_Number   LIKE(sub:Telephone_Number)     !List box control field - type derived from field
sub:Fax_Number         LIKE(sub:Fax_Number)           !List box control field - type derived from field
sub:Contact_Name       LIKE(sub:Contact_Name)         !List box control field - type derived from field
sub:Enquiry_Source     LIKE(sub:Enquiry_Source)       !List box control field - type derived from field
sub:Labour_Discount_Code LIKE(sub:Labour_Discount_Code) !List box control field - type derived from field
sub:Labour_VAT_Code    LIKE(sub:Labour_VAT_Code)      !List box control field - type derived from field
sub:Account_Type       LIKE(sub:Account_Type)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Sub Account File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Sub Account File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,122,344,176),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(InsertKey),ALRT(MouseLeft2),FORMAT('66L(2)|M~Account Number~@s15@80L(2)|M~Main Account Number~@s15@80L(2)|M~Company ' &|
   'Name~@s40@107L(2)|M~Branch~@s30@64L(2)|M~Postcode~@s15@68L(2)|M~Telephone Number' &|
   '~@s15@64L(2)|M~Fax Number~@s15@80L(2)|M~Contact Name~@s30@80L(2)|M~Enquiry Sourc' &|
   'e~@s30@56L(2)|M~Discount Code~@s2@36L(2)|M~VAT Code~@s2@52L(2)|M~Account Type~@s' &|
   '6@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,302),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,302),USE(?Change),TRN,FLAT,LEFT,ICON('edtheadp.jpg')
                       BUTTON,AT(304,302),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(372,302),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       OPTION,AT(156,110,213,15),USE(ViewAccounts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         RADIO('Active'),AT(168,112),USE(?ViewAccounts:Radio1),TRN,VALUE('A')
                         RADIO('Inactive'),AT(232,112),USE(?ViewAccounts:Radio2),TRN,VALUE('I')
                         RADIO('All'),AT(296,112),USE(?ViewAccounts:Radio3),TRN,VALUE('L')
                       END
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:White,09A6A7CH),SPREAD
                         TAB('By Account Number'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(168,98,64,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Company Name'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Branch'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(sub:Branch),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           COMBO(@s15),AT(296,98,124,10),USE(main_account_temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Main Account'),AT(424,98),USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020302'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Sub_Accounts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SUBTRACC.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Sub_Accounts')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sub:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sub:Company_Name,sub:Company_Name,1,BRW1)
  BRW1.AddSortOrder(,sub:Main_Branch_Key)
  BRW1.AddRange(sub:Main_Account_Number,main_account_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sub:Branch,sub:Branch,1,BRW1)
  BRW1.AddSortOrder(,sub:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sub:Account_Number,sub:Account_Number,1,BRW1)
  BRW1.AddField(sub:Account_Number,BRW1.Q.sub:Account_Number)
  BRW1.AddField(sub:Main_Account_Number,BRW1.Q.sub:Main_Account_Number)
  BRW1.AddField(sub:Company_Name,BRW1.Q.sub:Company_Name)
  BRW1.AddField(sub:Branch,BRW1.Q.sub:Branch)
  BRW1.AddField(sub:Postcode,BRW1.Q.sub:Postcode)
  BRW1.AddField(sub:Telephone_Number,BRW1.Q.sub:Telephone_Number)
  BRW1.AddField(sub:Fax_Number,BRW1.Q.sub:Fax_Number)
  BRW1.AddField(sub:Contact_Name,BRW1.Q.sub:Contact_Name)
  BRW1.AddField(sub:Enquiry_Source,BRW1.Q.sub:Enquiry_Source)
  BRW1.AddField(sub:Labour_Discount_Code,BRW1.Q.sub:Labour_Discount_Code)
  BRW1.AddField(sub:Labour_VAT_Code,BRW1.Q.sub:Labour_VAT_Code)
  BRW1.AddField(sub:Account_Type,BRW1.Q.sub:Account_Type)
  BRW1.AddField(sub:RecordNumber,BRW1.Q.sub:RecordNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB10.Init(main_account_temp,?main_account_temp,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(tra:Account_Number_Key)
  FDCB10.AddField(tra:Account_Number,FDCB10.Q.tra:Account_Number)
  FDCB10.AddField(tra:Company_Name,FDCB10.Q.tra:Company_Name)
  FDCB10.AddField(tra:RecordNumber,FDCB10.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By Account Number'
    ?Tab3{PROP:TEXT} = 'By Company Name'
    ?Tab2{PROP:TEXT} = 'By Branch'
    ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SUBTRACC.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Sub_Accounts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          If SecurityCheck('TRADE ACCOUNTS - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of changerecord
          If SecurityCheck('TRADE ACCOUNTS - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end
      of deleterecord
          If SecurityCheck('TRADE ACCOUNTS - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSUBTRACC
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ViewAccounts
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccounts, Accepted)
      BRW1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccounts, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020302'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020302'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020302'&'0')
      ***
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      If SecurityCheck('TRADE ACCOUNTS - INSERT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = false
      Else
          ThisWindow.Update
          Clear(tra:Record)
          Clear(sub:Record)
          GlobalRequest = InsertRecord
          UpdateTRADEACC
          ThisWindow.Reset
      end
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      If SecurityCheck('TRADE ACCOUNTS - CHANGE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = false
      Else
          ThisWindow.Update
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
              GlobalRequest = ChangeRecord
              UpdateTRADEACC
              ThisWindow.Reset
          end!if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
          brw1.ResetSort(1)
      end
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?Change:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change:2, Accepted)
      If SecurityCheck('TRADE ACCOUNTS - CHANGE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = false
      Else
          ThisWindow.Update
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
      
              Globalrequest = ChangeRecord
      
              If tra:Use_Sub_Accounts = 'YES'
                  If tra:Invoice_Sub_Accounts = 'YES'
                      glo:Select2 = 'FINANCIAL'
                  Else !If tra:Invoice_Sub_Accounts = 'YES'
                      glo:Select2 = ''
                  End !If tra:Invoice_Sub_Accounts = 'YES'
                  UpdateSUBTRACC
              Else !If tra:Use_Sub_Accounts = 'YES'
                  UpdateTRADEACC
              End !If tra:Use_Sub_Accounts = 'YES'
              ThisWindow.Reset
          end!if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
          Brw1.ResetSort(1)
      end
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change:2, Accepted)
    OF ?main_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?main_account_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?main_account_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case Keycode()
          Of Insertkey
              Post(event:accepted,?Insert)
          Of MouseLeft2
              Post(Event:Accepted,?Change)
      End!Case Keycode()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab:4{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Company Name~@s40@#3#66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab3{PROP:TEXT} = 'By Company Name'
        OF 3
          ?Browse:1{PROP:FORMAT} ='107L(2)|M~Branch~@s30@#4#66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab2{PROP:TEXT} = 'By Branch'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sub:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
    OF ?sub:Branch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?sub:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Loop While Keyboard()
          Ask
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  !TB12488 Part 2 : Hide inactive trade accounts   J 26/06/2012
  access:TradeAcc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = sub:Main_Account_Number
  if access:TradeAcc.fetch(tra:Account_Number_Key) then return(Record:filtered).
  
  Case ViewAccounts
      of 'A'  !Active
          if tra:Stop_Account = 'YES' then return(record:filtered).
      of 'I'  !inactive
          if tra:Stop_Account <> 'YES' then return(record:filtered).
      !of 'L'  !all not needed
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults




Browse_Loan PROCEDURE (f_stock_type)                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::29:TAGFLAG         BYTE(0)
DASBRW::29:TAGMOUSE        BYTE(0)
DASBRW::29:TAGDISPSTATUS   BYTE(0)
DASBRW::29:QUEUE          QUEUE
Pointer20                     LIKE(glo:Pointer20)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
excludedMan          STRING(300)
pos                  STRING(255)
save_loa_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(40)
Stock_Type_Temp      STRING(30)
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:allstock         BYTE(0)
tmp:Location         STRING(30)
tmp:AllLocations     BYTE(0)
tmp:FranchiseOnly    BYTE
save_WEBJOB_id       USHORT
save_JOBSE_id        USHORT
LocalTag             STRING(1)
SendingLocation      STRING(30)
SendingStockType     STRING(30)
BRW1::View:Browse    VIEW(LOAN)
                       PROJECT(loa:ESN)
                       PROJECT(loa:Ref_Number)
                       PROJECT(loa:Model_Number)
                       PROJECT(loa:Location)
                       PROJECT(loa:MSN)
                       PROJECT(loa:Colour)
                       PROJECT(loa:Job_Number)
                       PROJECT(loa:Stock_Type)
                       PROJECT(loa:Available)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
loa:ESN                LIKE(loa:ESN)                  !List box control field - type derived from field
loa:Ref_Number         LIKE(loa:Ref_Number)           !List box control field - type derived from field
loa:Model_Number       LIKE(loa:Model_Number)         !List box control field - type derived from field
loa:Location           LIKE(loa:Location)             !List box control field - type derived from field
loa:MSN                LIKE(loa:MSN)                  !List box control field - type derived from field
loa:Colour             LIKE(loa:Colour)               !List box control field - type derived from field
status_temp            LIKE(status_temp)              !List box control field - type derived from local data
loa:Job_Number         LIKE(loa:Job_Number)           !List box control field - type derived from field
loa:Stock_Type         LIKE(loa:Stock_Type)           !List box control field - type derived from field
loa:Available          LIKE(loa:Available)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK22::loa:Available       LIKE(loa:Available)
HK22::loa:Location        LIKE(loa:Location)
HK22::loa:Model_Number    LIKE(loa:Model_Number)
HK22::loa:Stock_Type      LIKE(loa:Stock_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Loan Units File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         ITEM('Generate Loan Waybills'),USE(?menuGenerateLoanWaybills)
                       END
                       PROMPT('Browse The Loan Unit File'),AT(8,12),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(8,104,592,256),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('10L(2)|MI@s1@66L(2)|M~I.M.E.I. Number~@s15@38R(2)|M~Unit No~L@s8@94L(2)|M~Model ' &|
   'Number~@s30@94L(2)|M~Location~@s30@50L(2)|M~M.S.N.~@s15@76L(2)|M~Colour~@s30@130' &|
   'L(2)|M~Status~@s60@32L(2)|M~Job Number~@p<<<<<<<<<<<<<<#p@0L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(608,156),USE(?NotAvailable),TRN,FLAT,ICON('uninavp.jpg')
                       PANEL,AT(4,30,672,40),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Stock Type'),AT(8,54),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(80,54,124,10),USE(Stock_Type_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       BUTTON,AT(208,48),USE(?LookupStockType),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Site Location'),AT(8,34),USE(?Prompt2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(80,34,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Location'),TIP('Location'),UPR
                       BUTTON,AT(208,32),USE(?LookupSiteLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       CHECK('All Locations'),AT(244,34),USE(tmp:AllLocations),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Locations'),TIP('All Locations'),VALUE('1','0')
                       CHECK('All Stock Types'),AT(244,54),USE(tmp:allstock),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Stock Types'),TIP('All Stock Types'),VALUE('1','0')
                       OPTION,AT(560,38,108,20),USE(all_temp),BOXED,FONT(,,,,CHARSET:ANSI),COLOR(0D6E7EFH)
                         RADIO('Available'),AT(572,46),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('AVL')
                         RADIO('All'),AT(628,46),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                       END
                       BUTTON,AT(608,130),USE(?ReturnUnitToStock),TRN,FLAT,ICON('uniavp.jpg')
                       BUTTON,AT(608,184),USE(?MoveStockType),TRN,FLAT,ICON('movestop.jpg')
                       BUTTON,AT(608,210),USE(?btnRapidInsertWizard),TRN,FLAT,ICON('rapinwzp.jpg')
                       BUTTON,AT(608,238),USE(?Order_Button),TRN,FLAT,ICON('ordstop.jpg')
                       BUTTON,AT(608,264),USE(?ReplaceLoan),TRN,FLAT,ICON('reploanp.jpg')
                       BUTTON,AT(608,292),USE(?ShowHistory),TRN,FLAT,ICON('unihistp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       BUTTON,AT(608,324),USE(?btnReturnLoan),TRN,FLAT,ICON('retloanp.jpg')
                       PROMPT('SRN:0000000'),AT(596,12),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,10,644,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(8,362),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(76,362),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(144,362),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(4,74,672,320),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('I.M.E.I. Number'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,90,124,10),USE(loa:ESN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(272,362),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(336,362),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(400,362),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('&Rev tags'),AT(497,372,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(497,383,1,1),USE(?DASSHOWTAG),HIDE
                         END
                         TAB('By Loan Unit Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@p<<<<<<<#pb),AT(8,90,124,10),USE(loa:Ref_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Model Number'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@p<<<<<<<<#pb),AT(212,90,124,10),USE(loa:Ref_Number,,?LOA:Ref_Number:2),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@s30),AT(8,90,125,10),USE(Model_Number_Temp),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           BUTTON,AT(140,87),USE(?buttonLookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('By M.S.N.'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,90,124,10),USE(loa:MSN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(608,102),USE(?Select),TRN,FLAT,ICON('selectp.jpg')
                       BUTTON,AT(608,396),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(532,362),USE(?btnAvailableQuantity),TRN,FLAT,ICON('availqtp.jpg')
                       BUTTON,AT(608,362),USE(?btnImportLoanSellingPrice),TRN,FLAT,ICON('imploanp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort16:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort17:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort18:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort19:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort20:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort21:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort22:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort23:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort24:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort25:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort26:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort27:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort28:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort29:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort30:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort31:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort32:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
locSavePath         CSTRING(255)
locImportFile       CSTRING(255),STATIC
ImportFile          File,DRIVER('BASIC'),PRE(impfil),Name(locImportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
SellingPrice                STRING(30)
RRCOrderCap                 STRING(30)
                        END
                    END

locExceptionFile    CSTRING(255),STATIC
ExceptionFile          File,DRIVER('BASIC'),PRE(excfil),Name(locExceptionFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
SellingPrice                STRING(30)
RRCOrderCap                 STRING(30)
PedanticBlank               STRING(1)
Reason                      STRING(255)
                        END
                    END

locExceptionReason  STRING(255)
!Save Entry Fields Incase Of Lookup
look:Stock_Type_Temp                Like(Stock_Type_Temp)
look:tmp:Location                Like(tmp:Location)
!End Save Entry Fields Incase Of Lookup
! ===========================================================================================
myProg       CLASS
! ===========================================================================================
ProgressThermometer     BYTE(),PRIVATE
RecordsToProcess        LONG(),PRIVATE
RecordsProcessed        LONG(),PRIVATE
PercentProgress         BYTE(),PRIVATE
UserString              STRING(100),PRIVATE
PercentString           STRING(60),PRIVATE
CancelButtonFEQ         LONG(),PRIVATE
SkipRecords             LONG(),PRIVATE
SkipCounter             LONG(),PRIVATE
Init                    PROCEDURE(LONG totalRecords,BYTE hidePercent=0,LONG skipRecords=0)
Kill                    PROCEDURE()
UpdateProgressBar       PROCEDURE(<STRING userString>,<BYTE hideMessage>),BYTE
Update                  PROCEDURE(<STRING userString>,<BYTE hideMessage>),BYTE
ClearVariables          PROCEDURE(),PRIVATE
                    END
myProg:Window      WINDOW('Progress...'),AT(,,210,64),CENTER,FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),DOUBLE
                        PROGRESS, AT(17,15,175,12), USE(?myProg:ProgressThermometer), RANGE(0,100)
                        STRING(''), AT(0,3,211,10), USE(?myProg:UserString), CENTER
                        STRING(''), AT(0,30,208,10), USE(?myProg:PercentString), CENTER
                        BUTTON('Cancel'), AT(80,44,50,15), USE(?myProg:ProgressCancel)
                        END
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !                !Xplore: Column displaying LocalTag
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying LocalTag
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying loa:ESN
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying loa:ESN
Xplore1Step3         StepLongClass   !LONG            !Xplore: Column displaying loa:Ref_Number
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying loa:Ref_Number
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying loa:Model_Number
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying loa:Model_Number
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying loa:Location
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying loa:Location
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying loa:MSN
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying loa:MSN
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying loa:Colour
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying loa:Colour
Xplore1Step8         StepCustomClass !                !Xplore: Column displaying status_temp
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying status_temp
Xplore1Step9         StepLongClass   !LONG            !Xplore: Column displaying loa:Job_Number
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying loa:Job_Number
Xplore1Step10        StepStringClass !STRING          !Xplore: Column displaying loa:Stock_Type
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying loa:Stock_Type
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying loa:Available
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying loa:Available
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::29:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue20.Pointer20 = loa:Ref_Number
   GET(glo:Queue20,glo:Queue20.Pointer20)
  IF ERRORCODE()
     glo:Queue20.Pointer20 = loa:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
    LocalTag = '*'
  ELSE
    DELETE(glo:Queue20)
    LocalTag = ''
  END
    Queue:Browse:1.LocalTag = LocalTag
  IF (LocalTag = '*')
    Queue:Browse:1.LocalTag_Icon = 2
  ELSE
    Queue:Browse:1.LocalTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::29:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue20)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue20.Pointer20 = loa:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::29:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue20)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::29:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::29:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue20)
    GET(glo:Queue20,QR#)
    DASBRW::29:QUEUE = glo:Queue20
    ADD(DASBRW::29:QUEUE)
  END
  FREE(glo:Queue20)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::29:QUEUE.Pointer20 = loa:Ref_Number
     GET(DASBRW::29:QUEUE,DASBRW::29:QUEUE.Pointer20)
    IF ERRORCODE()
       glo:Queue20.Pointer20 = loa:Ref_Number
       ADD(glo:Queue20,glo:Queue20.Pointer20)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::29:DASSHOWTAG Routine
   CASE DASBRW::29:TAGDISPSTATUS
   OF 0
      DASBRW::29:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::29:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::29:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
RemoveLoanFromJob       Routine
    DATA
locAuditNotes   STRING(255)
locAction STRING(100)
    CODE

!this routine is virtually a copy from ViewLoanUnit in SBJ01APP - routine PickLoanUnit
!got at via the Customer Service Screen - UPDATEJOBS in SBJ01app
!only this assumes that you only want to return the loan phone from that job and nothing more


    !will need the job details to complete this
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = loa:job_number
    If access:jobs.fetch(job:Ref_number_key) THEN
        Miss# = MIssive('Cannot find the job details from the loan record. You will need to remove this loan manually from job '&clip(Loa:job_number),|
                        'ServiceBase 3g','mexclam.jpg','OK')
        EXIT     !NO JOB record can't do a thing
    END

    locAuditNotes     = 'UNIT HAD BEEN DESPATCHED: ' & |
                        '<13,10>UNIT NUMBER: ' & Clip(job:Loan_unit_number) & |
                        '<13,10>COURIER: ' & CLip(job:loan_Courier) & |
                        '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                        '<13,10>DATE DESPATCHED: ' & Clip(Format(job:loan_despatched,@d6)) &|
                        '<13,10>DESPATCH USER: ' & CLip(job:loan_despatched_user) &|
                        '<13,10>DESPATCH NUMBER: ' & CLip(job:loan_despatch_number)



!TB13158 - ammended - do not check for the accessories at this stage
    locAction    = 'RETURN LOAN: RE-STOCKED - ACCESSORIES UNCHECKED'
    !Was     locAction    = 'RETURN LOAN: RE-STOCKED' to be ammended by loop below

!    Loop
!        If AccessoryCheck('LOAN')         
!            Case Missive('Do you wish to RE-ENTER the Accessories, CHARGE the customer for the mismatch, or IGNORE the mismatch?','ServiceBase 3g',|
!                           'mquest.jpg','\Cancel|Ignore|Charge|Re-Enter')
!                Of 4 ! Re-Enter Button
!                Of 3 ! Charge Button
!                    job:Courier_Cost += InsertLoanCharge()
!                    locAction = Clip(locAction) & ' - ACCESSORY MISMATCH. CUSTOMER CHARGED'
!                    Break
!                Of 2 ! Ignore Button
!                    locAction = Clip(locAction) & ' - ACCESSORY MISMATCH. IGNORED'
!                    Break
!                Of 1 ! Cancel Button
!                    EXIT
!            End ! Case Missive
!        Else
!            Break
!        End !If AccessoryCheck('LOAN')
!    End !Loop

    ! Change loan status to 816 RETURN TO STOCK - TrkBs: 6570 (DBH: 31-10-2005)
    GetStatus(816,1,'LOA')  ! Previously 101

    !record this in the audit trail
    IF (AddToAudit(job:Ref_Number,'LOA',locAction,locAuditNotes))
    END ! IF

    ! If job status is at "READY TO DESPATCH (LOAN)", revert to "READY TO DESPATCH" - TrkBs: 6570 (DBH: 31-10-2005)
    If Sub(job:Current_Status,1,3) = '811'
        GetStatus(810,1,'JOB')
    End !If Sub(job:Current_Status,1,3) = '811'

    !Remove the loan from the job table.
    job:loan_unit_number = ''
    job:loan_accessory = ''
    job:loan_consignment_number =''
    job:loan_despatched         =''
    job:loan_despatched_user    =''
    job:loan_despatch_number    =''
    job:Loaservice              =''
    job:loan_issued_date        = ''
    job:loan_user               = ''

    !Remove from web, or fat despatch table - 4485 (DBH: 20-07-2004)
    If job:despatch_type = 'LOA'
        job:despatched = 'NO'
        job:despatch_type = ''
    Else
        save_JOBSE_id = Access:JOBSE.SaveFile()
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            If jobe:DespatchType = 'LOA'
                jobe:DespatchType = ''
                Access:JOBSE.RestoreFile(save_JOBSE_id)
                save_WEBJOB_id = Access:WEBJOB.SaveFile()
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    wob:ReadyToDespatch = 0
                    Access:WEBJOB.TryUpdate()
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                Access:WEBJOB.RestoreFile(save_WEBJOB_id)

            End !If jobe:DespatchType = 'LOA'
            Access:JOBSE.TryUpdate()
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    End

    !update the jobs file here as it was opened here
    access:jobs.tryupdate()

    !now deal with the loan itself
    loa:available = 'AVL'
    loa:StatusChangeDate = today()
    loa:job_number = ''
    access:loan.update()

    get(loanhist,0)
    if access:loanhist.primerecord() = level:benign
        loh:ref_number    = old_loan_number#
        loh:date          = Today()
        loh:time          = Clock()
        loh:user          = use:user_code
        loh:status        = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
        if access:loanhist.insert()
            access:loanhist.cancelautoinc()
        end
    end!if access:exchhist.primerecord() = level:benign

    EXIT

SimpleMakeAvailable     Routine

    loa:job_number = ''
    loa:available = 'AVL'

    !Paul 02/06/2009 Log No 10684 add status change date
    loa:StatusChangeDate = today()
    access:loan.update()

    get(loanhist,0)
    if access:loanhist.primerecord() = level:benign
        loh:ref_number    = loa:ref_number
        loh:date          = Today()
        loh:time          = Clock()
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        loh:user = use:user_code
        !loh:status        = 'UNIT RETURNED TO STOCK FROM JOB: ' & Clip(Format(job:ref_number,@p<<<<<#p))   ! #12341 This line doesn't work anymore, as it doesn't on the exchange process. (DBH: 20/01/2012)
        loh:Status = 'UNIT AVAILABLE'
        if access:loanhist.insert()
            access:loanhist.cancelautoinc()
        end
    end!if access:loanhist.primerecord() = level:benign

    exit
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Loan','?Browse:1')        !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020281'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Loan')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFSTOCK.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:STOCKTYP.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  
  !Still wanted to exclude the loan by manufacturer
  !after opening files - before the window comes in
  !crete a limiting list of manufacturers to exclude
  Access:Manufact.clearkey(man:RecordNumberKey)
  set(man:RecordNumberKey,man:RecordNumberKey)
  Loop
      if access:Manufact.next() then break.
      !if man:Notes[1:8] = 'INACTIVE' then
      !TB13214 - change to using field for inactive  - J 05/02/14
      if man:Inactive = 1 then 
          ExcludedMan = clip(ExcludedMan)&'|'&clip(man:Manufacturer)&'|'      !pipes are there to separate manufact names
      END !if marked as inactive
  END !loop through manufact
  
  !TB12488 Disable Manufacturers  J 22/06/2012
  
  !this will mean I can use
  !if instring(clip(man:Manufacturer),excludedMan,1,1) then ... to exclude models and manufacts
  !TB12488 Disable Manufacturers  J 22/06/2012
  
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOAN,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (glo:webJob)
      ?btnImportLoanSellingPrice{prop:Hide} = 1
      ?menuGenerateLoanWaybills{prop:Hide} = 1
  END
  ! Save Window Name
   AddToLog('Window','Open','Browse_Loan')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbe02app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Location{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?Stock_Type_Temp{Prop:Tip} AND ~?LookupStockType{Prop:Tip}
     ?LookupStockType{Prop:Tip} = 'Select ' & ?Stock_Type_Temp{Prop:Tip}
  END
  IF ?Stock_Type_Temp{Prop:Msg} AND ~?LookupStockType{Prop:Msg}
     ?LookupStockType{Prop:Msg} = 'Select ' & ?Stock_Type_Temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,loa:LocStockAvailIMEIKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:ESN_Available_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailLocIMEI)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailIMEIOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockIMEIKey)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:ESN_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:LocIMEIKey)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:ESN_Only_Key)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockAvailRefKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Ref_Available_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailLocRef)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailRefOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockRefKey)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Ref_Number_Stock_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocRefKey)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockAvailModelKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Model_Available_Key)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailLocModel)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(?LOA:Ref_Number:2,loa:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailModOnlyKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockModelKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort21:Locator)
  BRW1::Sort21:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Model_Number_Key)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort22:Locator)
  BRW1::Sort22:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocModelKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:ModelRefNoKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(?LOA:Ref_Number:2,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockAvailMSNKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:MSN_Available_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort26:Locator)
  BRW1::Sort26:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailLocMSN)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort27:Locator)
  BRW1::Sort27:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailMSNOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort28:Locator)
  BRW1::Sort28:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:LocStockMSNKey)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort29:Locator)
  BRW1::Sort29:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:MSN_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort30:Locator)
  BRW1::Sort30:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:LocMSNKey)
  BRW1.AddRange(loa:Location)
  BRW1.AddLocator(BRW1::Sort31:Locator)
  BRW1::Sort31:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:MSN_Only_Key)
  BRW1.AddLocator(BRW1::Sort32:Locator)
  BRW1::Sort32:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:Ref_Available_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?loa:ESN,loa:Available,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('LocalTag',LocalTag)
  BIND('status_temp',status_temp)
  BIND('Model_Number_Temp',Model_Number_Temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(LocalTag,BRW1.Q.LocalTag)
  BRW1.AddField(loa:ESN,BRW1.Q.loa:ESN)
  BRW1.AddField(loa:Ref_Number,BRW1.Q.loa:Ref_Number)
  BRW1.AddField(loa:Model_Number,BRW1.Q.loa:Model_Number)
  BRW1.AddField(loa:Location,BRW1.Q.loa:Location)
  BRW1.AddField(loa:MSN,BRW1.Q.loa:MSN)
  BRW1.AddField(loa:Colour,BRW1.Q.loa:Colour)
  BRW1.AddField(status_temp,BRW1.Q.status_temp)
  BRW1.AddField(loa:Job_Number,BRW1.Q.loa:Job_Number)
  BRW1.AddField(loa:Stock_Type,BRW1.Q.loa:Stock_Type)
  BRW1.AddField(loa:Available,BRW1.Q.loa:Available)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 3
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?tmp:Location{prop:Disable} = 1
          ?tmp:AllLocations{prop:Disable} = 1
          ?LookupSiteLocation{prop:Hide} = 1
      End !If use:StockFromLocationOnly
      tmp:Location    = use:Location
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  if glo:webjob then
      disable(tmp:Location)
      disable(tmp:AllLocations)
  end
  
  If SecurityCheck('LOAN UNITS - ORDER')
      Hide(?Order_Button)
  end
  
  If SecurityCheck('LOAN - RAPID INSERT') THEN
     HIDE(?btnRapidInsertWizard)
  end
  
  if glo:WebJob
      !hide(?ReturnUnitToStock)
      hide(?MoveStockType)
      hide(?Insert:3)
      hide(?Change:3)
      hide(?Delete:3)
      BRW1.InsertControl = 0
      BRW1.ChangeControl = 0
      BRW1.DeleteControl = 0
  end
  
  
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?Browse:1{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab:2{PROP:TEXT} = 'By Loan Unit Number'
    ?Tab:3{PROP:TEXT} = 'By Model Number'
    ?Tab4{PROP:TEXT} = 'By M.S.N.'
    ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#66L(2)|M~I.M.E.I. Number~@s15@#3#38R(2)|M~Unit No~L@s8@#4#94L(2)|M~Model Number~@s30@#5#94L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s15@#7#76L(2)|M~Colour~@s30@#8#130L(2)|M~Status~@s60@#9#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFSTOCK.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Loan','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','Browse_Loan')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
    of insertrecord
        If SecurityCheck('LOAN UNITS - INSERT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            do_update# = false
            access:loan.cancelautoinc()
        end
    of changerecord
        If SecurityCheck('LOAN UNITS - CHANGE')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
  
            do_update# = false
        end
    of deleterecord
        If SecurityCheck('LOAN UNITS - DELETE')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
  
            do_update# = false
        end
  end !case request
  
  If do_update# = true
      If Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3
          If Thiswindow.request = Selectrecord
              Case Missive('You cannot Insert / Change / Delete while you are updating a job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
              If request = Insertrecord
                  access:loan.cancelautoinc()
              End!If request = Insertrecord
          End!If Globalrequest = Selectrecord
      End !If Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3
  End!If do_update# = true
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickLoanStockType
      PickSiteLocations
      UpdateLOAN
    END
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(LocalTag,BRW1.Q.LocalTag)
  Xplore1.AddField(loa:ESN,BRW1.Q.loa:ESN)
  Xplore1.AddField(loa:Ref_Number,BRW1.Q.loa:Ref_Number)
  Xplore1.AddField(loa:Model_Number,BRW1.Q.loa:Model_Number)
  Xplore1.AddField(loa:Location,BRW1.Q.loa:Location)
  Xplore1.AddField(loa:MSN,BRW1.Q.loa:MSN)
  Xplore1.AddField(loa:Colour,BRW1.Q.loa:Colour)
  Xplore1.AddField(status_temp,BRW1.Q.status_temp)
  Xplore1.AddField(loa:Job_Number,BRW1.Q.loa:Job_Number)
  Xplore1.AddField(loa:Stock_Type,BRW1.Q.loa:Stock_Type)
  Xplore1.AddField(loa:Available,BRW1.Q.loa:Available)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Order_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Button, Accepted)
      IF (tmp:AllLocations = 0)
          IF (tmp:Location <> '')
              ExchangeLoanOrderWindow(tmp:Location,'LOA') ! #12347 ! Call the combined order window (DBH: 19/01/2012)
          END
      END
      !
      !if tmp:AllLocations = 0
      !    if tmp:Location <> ''
      !        Loan_Order_Window(tmp:Location)
      !    end
      !end
      !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Button, Accepted)
    OF ?ShowHistory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowHistory, Accepted)
      glo:Select12    = brw1.q.loa:Ref_Number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowHistory, Accepted)
    OF ?btnReturnLoan
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnReturnLoan, Accepted)
      If SecurityCheck('RETURN LOAN UNITS')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnReturnLoan, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?menuGenerateLoanWaybills
      ThisWindow.Update
      LoanWaybillGeneration
      ThisWindow.Reset
    OF ?NotAvailable
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NotAvailable, Accepted)
      If SecurityCheck('LOAN - NOT AVAILABLE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('EXCHANGE - NOT AVAILABLE')
          IF (loa:Available = 'ITF') ! #12228 12528 - If Status is ITF (InTransit To Franchise) Don't allow access (DBH: 05/04/2012)
              ! #12347 Only allow to make "In Transit" available, if has access level. (DBH: 20/01/2012)
              IF (SecurityCheck('AMEND LOAN IN TRANSIT'))
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Error! The selected loan is In Transit.'&|
                      '|You do not have access to make it not available.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END
          END ! IF (loa:Available = 'RTM')
      
          Case Missive('Are you sure you want to make the selected unit "Not Available"?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  brw1.UpdateViewRecord()
                  loa:Available = 'NOA'
                  !Paul 02/06/2009 Log No 10684
                  loa:StatusChangeDate = today()
                  Access:LOAN.Update()
                  get(loanhist,0)
                  if access:loanhist.primerecord() = Level:Benign
                      loh:ref_number      = loa:ref_number
                      loh:date            = Today()
                      loh:time            = Clock()
                      access:users.clearkey(use:password_key)
                      use:password        = glo:password
                      access:users.fetch(use:password_key)
                      loh:user = use:user_code
                      loh:status          = Clip(NotAvailableReason())
                      access:loanhist.insert()
                  end!if access:exchhist.primerecord() = Level:Benign
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('EXCHANGE - NOT AVAILABLE')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NotAvailable, Accepted)
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      IF Stock_Type_Temp OR ?Stock_Type_Temp{Prop:Req}
        stp:Stock_Type = Stock_Type_Temp
        stp:Use_Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:Stock_Type_Temp        = Stock_Type_Temp
        IF Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Stock_Type_Temp = stp:Stock_Type
          ELSE
            CLEAR(stp:Use_Loan)
            !Restore Lookup On Error
            Stock_Type_Temp = look:Stock_Type_Temp
            SELECT(?Stock_Type_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    OF ?LookupStockType
      ThisWindow.Update
      stp:Stock_Type = Stock_Type_Temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Stock_Type_Temp = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?Stock_Type_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Stock_Type_Temp)
    OF ?tmp:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
      BRW1.ResetSort(1)
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loc:Location = tmp:Location
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = tmp:Location
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:AllLocations
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllLocations, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllLocations, Accepted)
    OF ?tmp:allstock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
    OF ?all_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
      Thiswindow.reset
      Select(?Browse:1)
      If thiswindow.request <> Selectrecord
          Case all_temp
              Of 'YES'
                  Unhide(?ReturnUnitToStock)
              Of 'NO'
                  Hide(?ReturnUnitToStock)
          End!Case all_temp
      End!If thiswindow.request <> Selectrecord
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
    OF ?ReturnUnitToStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
      BRW1.UpdateViewRecord()
      
      IF (loa:Available = 'ITF') ! #12228 12528 - If Status is ITF (InTransit To Franchise) Don't allow access (DBH: 05/04/2012)
          ! #12347 Only allow to make "In Transit" available, if has access level. (DBH: 20/01/2012)
          IF (SecurityCheck('AMEND LOAN IN TRANSIT'))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error! The selected loan is In Transit.'&|
                  '|You do not have access to make it available.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      END ! IF (loa:Available = 'RTM')
      
      ! Inserting (DBH 21/07/2008) # 10130 - Cannot make available if it's already available in Loan Stock.
      Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
      xch:ESN = loa:ESN
      If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          !Found
          If xch:Available <> 'NOA'
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Cannot make this unit available. It is already "Available" in Exchange Stock.','ServiceBase 3g',|
                             'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              Cycle
          End ! If xch:Available <> 'NOA'
      Else ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          !Error
      End ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      ! End (DBH 21/07/2008) #10130
      
      IF (loa:Job_Number > 0)
          Case Missive('You are about to remove this unit from job number ' & Clip(loa:Job_Number) & |    
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  CYCLE
          End ! Case Missive
          Do RemoveLoanFromJob
      
      ELSE
          Case Missive('Are you sure you want to make this unit "Available"?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  CYCLE
          End ! Case Missive
          Do SimpleMakeAvailable
      
      END
      
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
    OF ?MoveStockType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
      !TB03229 - J - 06/05/14 - process moved to another window shared with Loans
      
          !do not call if the location or type have not been set
          if tmp:AllLocations = false and clip(tmp:Location)   = '' then cycle.
          if tmp:allstock     = false and clip(Stock_Type_Temp)= '' then cycle.
      
          !call the window (SentLoanOrExchange,SentLocation,SentStockType)
          if tmp:AllLocations then
              SendingLocation = 'ALL'
          ELSE
              SendingLocation =  tmp:Location
          END
          if tmp:allstock then
              SendingStockType = 'ALL'
          ELSE
              SendingStockType = Stock_Type_Temp
          END
          MoveStockType('L',SendingLocation,SendingStockType)
          DO DASBRW::29:DASUNTAGALL
      
      
      
      !TB03229 - original code commented
      !Case Missive('Do you wish to move the selected unit to a new Stock Type, or move ALL units to another Stock Type?','ServiceBase 3g',|
      !               'mquest.jpg','\Cancel|Move All|Move One')
      !    Of 3 ! Move One Button
      !
      !        Case Missive('You have selected to move the SELECTED unit to another Stock Type.'&|
      !          '<13,10>'&|
      !          '<13,10>Are you sure?','ServiceBase 3g',|
      !                       'mquest.jpg','\No|/Yes')
      !            Of 2 ! Yes Button
      !                saverequest#      = globalrequest
      !                globalresponse    = requestcancelled
      !                globalrequest     = selectrecord
      !                glo:select1       = 'LOA'
      !                select_stock_type
      !                if globalresponse = requestcompleted
      !                    access:loan.clearkey(loa:ref_number_key)
      !                    loa:ref_number = brw1.q.loa:ref_number
      !                    if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
      !                        loa:stock_type  = stp:stock_type
      !                        access:loan.update()
      !                        get(loanhist,0)
      !                        if access:loanhist.primerecord() = level:benign
      !                            loh:ref_number      = loa:ref_number
      !                            loh:date            = today()
      !                            loh:time            = clock()
      !                            access:users.clearkey(use:password_key)
      !                            use:password        = glo:password
      !                            access:users.fetch(use:password_key)
      !                            loh:user = use:user_code
      !                            loh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
      !                                                   ' TO ' & Clip(stp:stock_type)
      !                            access:loanhist.insert()
      !                        end!if access:loanhist.primerecord() = level:benign
      !
      !                    End!if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
      !                end
      !                globalrequest     = saverequest#
      !                glo:select1       = ''
      !            Of 1 ! No Button
      !        End ! Case Missive
      !    Of 2 ! Move All Button
      !
      !        Case Missive('You have selected to move ALL the units, no matter what status, to a new Stock Type.'&|
      !          '<13,10>'&|
      !          '<13,10>Are you sure?','ServiceBase 3g',|
      !                       'mquest.jpg','\No|/Yes')
      !            Of 2 ! Yes Button
      !                saverequest#      = globalrequest
      !                globalresponse    = requestcancelled
      !                globalrequest     = selectrecord
      !                glo:select1       = 'LOA'
      !                select_stock_type
      !                if globalresponse = requestcompleted
      !                    setcursor(cursor:wait)
      !                    save_loa_id = access:loan.savefile()
      !                    access:loan.clearkey(loa:ref_number_stock_key)
      !                    loa:stock_type = stock_type_temp
      !                    set(loa:ref_number_stock_key,loa:ref_number_stock_key)
      !                    loop
      !                        if access:loan.next()
      !                           break
      !                        end !if
      !                        if loa:stock_type <> stock_type_temp      |
      !                            then break.  ! end if
      !                        pos = Position(loa:ref_number_stock_key)
      !                        loa:stock_type  = stp:stock_type
      !                        access:loan.update()
      !                        get(loanhist,0)
      !                        if access:loanhist.primerecord() = level:benign
      !                            loh:ref_number      = loa:ref_number
      !                            loh:date            = today()
      !                            loh:time            = clock()
      !                            access:users.clearkey(use:password_key)
      !                            use:password        = glo:password
      !                            access:users.fetch(use:password_key)
      !                            loh:user = use:user_code
      !                            loh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
      !                                                   ' TO ' & Clip(stp:stock_type)
      !                            access:loanhist.insert()
      !                        end!if access:loanhist.primerecord() = level:benign
      !
      !                        Reset(loa:ref_number_stock_key,pos)
      !                    end !loop
      !                    access:loan.restorefile(save_loa_id)
      !                    setcursor()
      !                end
      !                globalrequest     = saverequest#
      !                glo:select1       = ''
      !
      !            Of 1 ! No Button
      !        End ! Case Missive
      !    Of 1 ! Cancel Button
      !End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
    OF ?btnRapidInsertWizard
      ThisWindow.Update
      Rapid_Loan_Unit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnRapidInsertWizard, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnRapidInsertWizard, Accepted)
    OF ?ReplaceLoan
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplaceLoan, Accepted)
      If SecurityCheck('REPLACE LOAN')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('REPLACE LOAN')
          ReplaceLoan(brw1.q.loa:Ref_Number)
      End !SecurityCheck('REPLACE LOAN')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplaceLoan, Accepted)
    OF ?ShowHistory
      ThisWindow.Update
      Browse_Loan_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowHistory, Accepted)
      glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowHistory, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020281'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020281'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020281'&'0')
      ***
    OF ?btnReturnLoan
      ThisWindow.Update
      ReturnLoanProcess
      ThisWindow.Reset
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      BRW1.ResetSort(1)
      !Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    OF ?buttonLookup
      ThisWindow.Update
      ! Bryan Simple Lookup
      CLEAR(mod:Record)
      mod:Model_Number = Model_Number_Temp
      IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key))
          ! Failed, set the key instead
          Clear(mod:Record)
          mod:Model_Number = Model_Number_Temp
          SET(mod:Model_Number_Key,mod:Model_Number_Key)
          Access:MODELNUM.TryNext()
      END ! IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key))
      GlobalRequest = SelectRecord
      SelectModel()
      IF (GlobalResponse = RequestCompleted)
          CHANGE(?Model_Number_Temp,mod:Model_Number)
          POST(Event:Accepted,?Model_Number_Temp)
          SELF.Request = SELF.OriginalRequest
      ELSE ! IF (GlobalResponse = RequestCompleted)
          SELECT(?buttonLookup)
          SELF.Request = SELF.OriginalRequest
      END ! IF (GlobalResponse = RequestCompleted)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
      thiswindow.reset(1)
      error# = 0
      If loa:job_number <> 0
          Case Missive('This unit is already attached to job ' & Clip(job:Ref_Number) & '.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If xch:job_number > 0
      If error# = 0
          glo:select1 = loa:ref_number
          Post(Event:closewindow)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
    OF ?btnAvailableQuantity
      ThisWindow.Update
      LoanAvailableQuantityCriteria
      ThisWindow.Reset
    OF ?btnImportLoanSellingPrice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnImportLoanSellingPrice, Accepted)
      ! #12347 Import Selling Price (DBH: 19/01/2012)
      IF (SecurityCheck('LOAN SELLING & RRC ORDER CAP'))
          ! Failed
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      
      locImportFile = ''
      IF (LookupFileExtended(locImportFile,'CSV Files|*.csv','Choose Import File'))
          CYCLE
      END
      
      
      OPEN(ImportFile)
      IF (ERROR())
          STOP(ERROR())
          CYCLE
      END
      
      ! Quick record count
      SETCURSOR(CURSOR:WAIT)
      records# = 0
      SET(ImportFile,0)
      LOOP
          NEXT(ImportFile)
          IF (ERROR())
              BREAK
          END
          records# += 1
      END
      SETCURSOR()
      
      IF (NOT records# > 1)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('There are no records to import.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Close(ImportFile)
          CYCLE
      END
      
      ! Set exception file path
      locExceptionFile = BHAddBackSlash(GETINI('EXCEPTION','LoanRate',PATH(),PATH() & '\SB2KDEF.INI'))
      
      locExceptionFile = CLIP(locExceptionFile) & 'LoanSellingPrice_' & FORMAT(DAY(TODAY()),@n02) & FORMAT(MONTH(TODAY()),@n02) & YEAR(TODAY()) & FORMAT(CLOCK(),@t02) & '.csv'
      IF (EXISTS(locExceptionFile))
          REMOVE(locExceptionFile)
      END
      
      CREATE(ExceptionFile)
      OPEN(ExceptionFile)
      ! Add Title
      Clear(excfil:Record)
      excfil:Manufacturer = 'Manufacturer'
      excfil:ModelNumber = 'Model Number'
      excfil:SellingPrice = 'Selling Price'
      excfil:RRCOrderCap = 'RRC Order Cap'
      excfil:Reason = 'Reason'
      ADD(ExceptionFile)
      
      myProg.Init(records#)
      skipFirst# = 1
      exceptions# = 0
      imports# = 0
      SET(ImportFile,0)
      LOOP
          NEXT(ImportFile)
          IF (ERROR())
              BREAK
          END
          IF (skipFirst# = 1)
              skipFirst# = 0
              CYCLE
          END
          IF (myProg.Update())
              BREAK
          END
          locExceptionReason = ''
          ! Check for Manufacturer Existance
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer = UPPER(impfil:Manufacturer)
          IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key))
      
              locExceptionReason = CLIP(locExceptionReason) & '-MANUFACTURER NOT SETUP'
          END
      
          ! Check for Model Existance
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number = UPPER(impfil:ModelNumber)
          IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key))
              locExceptionReason = CLIP(locExceptionReason) & '-MODEL NUMBER NOT SETUP'
          END
      
          ! Get Model
          Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
          mod:Manufacturer = impfil:Manufacturer
          mod:Model_Number = impfil:ModelNumber
          IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
              locExceptionReason = CLIP(locExceptionReason) & '-MANUFACTURER MODEL NUMBER MISMATCH'
          END
      
          ! No Exceptions, update values
          IF (locExceptionReason = '')
              mod:LoanReplacementValue = DEFORMAT(impfil:SellingPrice)
              mod:RRCOrderCap = DEFORMAT(impfil:RRCOrderCap)
              IF (Access:MODELNUM.TryUpdate() = Level:Benign)
                  imports# += 1
              ELSE
                  locExceptionReason = '-FAILED TO UPDATE RECORD'
              END
          END ! IF (locExceptionReason = '')
      
          IF (locExceptionReason <> '')
              Clear(excfil:Record)
              excfil:Manufacturer = impfil:Manufacturer
              excfil:ModelNUmber = impfil:ModelNumber
              excfil:SellingPrice = impfil:SellingPrice
              excfil:RRCOrderCap = impfil:RRCOrderCap
              excfil:Reason = CLIP(SUB(locExceptionReason,2,255))
              ADD(ExceptionFile)
              exceptions# += 1
              CYCLE
          END ! IF (locExceptionReason <> '')
      END
      myProg.Kill()
      
      CLOSE(ImportFile)
      
      CLOSE(ExceptionFile)
      
      
      IF (imports# = 0 AND exceptions# = 0)
          ! #12347 Can't see how this can occur, but you never know. (DBH: 19/01/2012)
          REMOVE(ExceptionFile)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('No records where imported, and there were no exceptions.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          
      ELSIF (imports# = 0 AND exceptions# > 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('No records were updated.'&|
              '|Exceptions: ' & Clip(exceptions#) & ''&|
              '|'&|
              '|File: ' & Clip(locExceptionFile) & '','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
      ELSIF (imports# > 1 AND exceptions# = 0)
          REMOVE(ExceptionFile)
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Missive('Records Updated: ' & Clip(imports#) & ''&|
              '|'&|
              '|There were no exception(s).','ServiceBase',|
                         'midea.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
      ELSE ! (imports# = 1 AND exceptions# = 1)
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('Records updated: ' & Clip(imports#) & ''&|
              '|Exceptions: ' & Clip(exceptions#) & ''&|
              '|'&|
              '|File: ' & Clip(locExceptionFile) & '','ServiceBase',|
                         'mexclam.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
      END
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnImportLoanSellingPrice, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
    !Paul 01/06/2009 Log No 10684
    !look up the stock Type and see if the buttons should be hidden for this
    If glo:WebJob > 0 then
        !must be a franchise
    
        !message('IMEI ' & clip(loa:ESN) & '/ stock type ' & clip(loa:Stock_Type))
        Access:StockTyp.Clearkey(stp:Stock_Type_Key)
        stp:Stock_Type = clip(loa:Stock_Type)
        If Access:StockTyp.fetch(stp:Stock_Type_Key) = level:benign then
            If stp:FranchiseViewOnly = 1 then
                !hide the buttons
                ?btnRapidInsertWizard{prop:Disable} = True
                ?ReplaceLoan{prop:Disable} = True
            Else
                !show the buttons
                ?btnRapidInsertWizard{prop:Disable} = False
                ?ReplaceLoan{prop:Disable} = False
            End !If stp:FranchiseViewOnly = 1 then
        End !If Access:StockTyp.fetch(stp:Stock_Type_Key) = level:benign then
    End !If glo:WebJob > 0 then
    
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::29:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#66L(2)|M~I.M.E.I. Number~@s15@#3#38R(2)|M~Unit No~L@s8@#4#94L(2)|M~Model Number~@s30@#5#94L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s15@#7#76L(2)|M~Colour~@s30@#8#130L(2)|M~Status~@s60@#9#'
          ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#38R(2)|M~Unit No~L@s8@#4#66L(2)|M~I.M.E.I. Number~@s15@#3#94L(2)|M~Model Number~@s30@#5#94L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s15@#7#76L(2)|M~Colour~@s30@#8#130L(2)|M~Status~@s60@#9#'
          ?Tab:2{PROP:TEXT} = 'By Loan Unit Number'
        OF 3
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#38R(2)|M~Unit No~L@s8@#4#66L(2)|M~I.M.E.I. Number~@s15@#3#94L(2)|M~Model Number~@s30@#5#94L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s15@#7#76L(2)|M~Colour~@s30@#8#130L(2)|M~Status~@s60@#9#'
          ?Tab:3{PROP:TEXT} = 'By Model Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#50L(2)|M~M.S.N.~@s15@#7#66L(2)|M~I.M.E.I. Number~@s15@#3#38R(2)|M~Unit No~L@s8@#4#94L(2)|M~Model Number~@s30@#5#94L(2)|M~Location~@s30@#6#76L(2)|M~Colour~@s30@#8#130L(2)|M~Status~@s60@#9#'
          ?Tab4{PROP:TEXT} = 'By M.S.N.'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?loa:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Selected)
      !Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Selected)
    OF ?LOA:Ref_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LOA:Ref_Number:2, Selected)
      !Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LOA:Ref_Number:2, Selected)
    OF ?loa:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:MSN, Selected)
      !Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:MSN, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If thiswindow.request = Selectrecord
          Unhide(?Select)
          Hide(?ReturnUnitToStock)
      End
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

myProg.Init  PROCEDURE(LONG totalRecords,BYTE hidePercent=0,LONG skipRecords=0)
locXpos                 LONG
locYpos                 LONG
locHeight               LONG
locWidth                LONG
    code
        SELF.ClearVariables()
        SELF.RecordsToProcess = totalRecords

        IF (skipRecords > 0)
            SELF.SkipRecords = skipRecords
        ELSIF (0 > 0)
            SELF.SkipRecords = 0
        ELSE
            SELF.SkipRecords = ABS(TotalRecords/100)
        END

        ! Get Current Window's Position
        locXpos = 0{PROP:Xpos}
        locYpos = 0{PROP:Ypos}
        locWidth = 0{PROP:Width}
        locHeight = 0{PROP:Height}

        OPEN(myProg:Window)
        SELF.CancelButtonFEQ = ?myProg:ProgressCancel
        display()
        IF (locXpos > 0 OR locYpos > 0 OR locWidth > 0 OR locHeight > 0)
            ! Only move if opening infront of another window
            0{PROP:Xpos} = locXpos + (locWidth / 2) - 105
            0{PROP:Ypos} = locYpos + (locHeight / 2) - 30
        END

        IF (hidePercent = TRUE OR 0 = TRUE)
            ?myProg:PercentString{PROP:Hide} = 1
        end
        IF (0 = TRUE OR 0 = TRUE)
            ?myProg:ProgressCancel{prop:Hide} = 1
            0{prop:Height} = 0{prop:Height} - 14
        END

myProg.Update       PROCEDURE(<STRING userString>,<BYTE hideMessage>)
    CODE
        RETURN myProg.UpdateProgressBar(userString,hideMessage)

myProg.UpdateProgressBar       PROCEDURE(<STRING userString>,<BYTE hideMessage>)
userCancelled                           BYTE(0)
    code
        SELF.UserString = userString
        SELF.RecordsProcessed += 1
        SELF.SkipCounter += 1
        IF (SELF.SkipRecords > 0)
            IF (SELF.SkipCounter > SELF.SkipRecords)
                SELF.SkipCounter = 0
            ELSE
                RETURN 0
            END
        END

        IF (SELF.PercentProgress < 100)
            SELF.PercentProgress = ABS((SELF.RecordsProcessed / SELF.RecordsToProcess) * 100)
            IF (SELF.PercentProgress > 100 OR SELF.PercentProgress < 0)
                SELF.PercentProgress = 0
            END
            IF (SELF.PercentProgress <> SELF.ProgressThermometer)
                SELF.ProgressThermometer = SELF.PercentProgress
                SELF.PercentString = format(SELF.PercentProgress,@n3) & '% Completed'
            END
        END

        ?myProg:UserString{PROP:Text} = SELF.UserString
        ?myProg:PercentString{PROP:Text} = SELF.PercentString
        ?myProg:ProgressThermometer{PROP:Progress} = SELF.ProgressThermometer
        !UPDATE()
        DISPLAY()
        ACCEPT
            CASE EVENT()
            OF EVENT:Timer
                BREAK
            OF EVENT:CloseWindow
                BREAK
            OF EVENT:Accepted
                IF FIELD() = SELF.CancelButtonFEQ
                    userCancelled = TRUE
                END
                BREAK
            END
        END
        IF (userCancelled = TRUE)
            IF (hideMessage = 1 OR 0 = 1)
                userCancelled = TRUE
            ELSE
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('Are you sure you want to cancel?','Cancelled Pressed',|
                    Icon:Question,'&Yes|&No',2)
                Of 1 ! &Yes Button
                Of 2 ! &No Button
                    userCancelled = FALSE
                End!Case Message
            END
        END
        RETURN userCancelled



myProg.ClearVariables        PROCEDURE()
    code
        SELF.RecordsToProcess = 0
        SELF.ProgressThermometer = 0
        SELF.RecordsProcessed = 0
        SELF.UserString = ''
        SELF.PercentString = ''
        SELF.SkipCounter = 0
        SELF.PercentProgress = 0


myProg.Kill  PROCEDURE()
    code
        SELF.PercentProgress = 100
        SELF.PercentString = 'Finished!'
        ?myProg:UserString{PROP:Text} = SELF.UserString
        ?myProg:PercentString{PROP:Text} = SELF.PercentString
        ?myProg:ProgressThermometer{PROP:Progress} = 100
        DISPLAY()

        CLOSE(myProg:Window)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(15,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(16,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(17,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(18,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(19,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(20,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(21,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(22,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(23,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(24,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(25,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(26,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(27,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(28,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(29,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(30,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(31,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(32,Force)
  ELSE
    RETURN SELF.SetSort(33,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = loa:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Status_Temp = LoanStatus(loa:Available,loa:Job_Number)
  !
  !CASE (loa:Available)
  !OF 'AVL'
  !  status_temp = 'AVAILABLE'
  !OF 'LOA'
  !  status_temp = 'LOANED - JOB NO: ' & loa:Job_Number
  !OF 'LOS'
  !    Status_Temp = 'LOAN LOST'
  !OF 'INC'
  !  status_temp = 'INCOMING TRANSIT - JOB NO: ' & loa:Job_Number
  !OF 'FAU'
  !  status_temp = 'FAULTY'
  !Of 'NOA'
  !    Status_Temp = 'NOT AVAILABLE'
  !OF 'REP'
  !  status_temp = 'IN REPAIR - JOB NO: ' & loa:Job_Number
  !OF 'SUS'
  !  status_temp = 'SUSPENDED'
  !OF 'DES'
  !  status_temp = 'DESPATCHED - JOB NO: ' & loa:Job_Number
  !OF 'QA1'
  !  status_temp = 'ELECTRONIC QA REQUIRED - JOB NO: ' & loa:Job_Number
  !OF 'QA2'
  !  status_temp = 'MANUAL QA REQUIRED - JOB NO: ' & loa:Job_Number
  !OF 'QAF'
  !  status_temp = 'QA FAILED'
  !OF 'RTS'
  !  status_temp = 'RETURN TO STOCK'
  !ELSE
  !  status_temp = 'IN REPAIR - JOB NO: ' & loa:Job_Number
  !END
  PARENT.SetQueueRecord
  IF (LocalTag = '*')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  if instring('|'&clip(loa:Manufacturer)&'|',excludedMan,1,1) then RETURN(RECORD:FILTERED).
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = loa:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    EXECUTE DASBRW::29:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !LocalTag
  OF 3 !loa:ESN
  OF 4 !loa:Ref_Number
  OF 5 !loa:Model_Number
  OF 6 !loa:Location
  OF 7 !loa:MSN
  OF 8 !loa:Colour
  OF 9 !status_temp
  OF 10 !loa:Job_Number
  OF 11 !loa:Stock_Type
  OF 12 !loa:Available
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step11)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?loa:ESN,LocalTag,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(?loa:ESN,loa:ESN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(?loa:Ref_Number,loa:Ref_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(?loa:ESN,loa:Model_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(?loa:ESN,loa:Location,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(?loa:MSN,loa:MSN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(?loa:ESN,loa:Colour,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(?loa:ESN,status_temp,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(?loa:ESN,loa:Job_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(?loa:ESN,loa:Stock_Type,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(?loa:ESN,loa:Available,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddResetField(all_temp)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOAN)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('LocalTag')                   !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('LocalTag')                   !Header
                PSTRING('@S20')                       !Picture
                PSTRING('LocalTag')                   !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:ESN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s15')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Unit No')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Unit No')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:MSN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('M.S.N.')                     !Header
                PSTRING('@s15')                       !Picture
                PSTRING('M.S.N.')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Colour')                 !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Colour')                     !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Colour')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('status_temp')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('status_temp')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('status_temp')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Job_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@p<<<<<<<<<<<<<<#p')         !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Stock_Type')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Stock Type')                 !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Stock Type')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Available')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Available')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Available')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(11)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?all_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?LOA:Ref_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Model_Number_Temp, Resize:FixLeft, Resize:LockSize)
  SELF.SetStrategy(?LOA:Ref_Number:2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?LOA:ESN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?LOA:MSN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

UpdateSTOCKTYP PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stp:Record  LIKE(stp:RECORD),STATIC
QuickWindow          WINDOW('Update the STOCKTYP File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Stock Type'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Stock Type'),AT(240,163),USE(?STP:Stock_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,163,124,10),USE(stp:Stock_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           CHECK('Loan Unit Stock Type'),AT(316,179,112,8),USE(stp:Use_Loan),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Exchange Unit Stock Type'),AT(316,194),USE(stp:Use_Exchange),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Available'),AT(316,211),USE(stp:Available),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Available'),TIP('Available'),VALUE('1','0')
                           CHECK('Franchise View Only Loan Unit Stock Type'),AT(316,227),USE(stp:FranchiseViewOnly),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Franchise View Only Stock Type')
                         END
                       END
                       BUTTON,AT(384,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Stock Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Stock Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020273'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSTOCKTYP')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stp:Record,History::stp:Record)
  SELF.AddHistoryField(?stp:Stock_Type,3)
  SELF.AddHistoryField(?stp:Use_Loan,1)
  SELF.AddHistoryField(?stp:Use_Exchange,2)
  SELF.AddHistoryField(?stp:Available,4)
  SELF.AddHistoryField(?stp:FranchiseViewOnly,5)
  SELF.AddUpdateFile(Access:STOCKTYP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCKTYP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateSTOCKTYP')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?stp:Use_Loan{Prop:Checked} = True
    UNHIDE(?stp:FranchiseViewOnly)
  END
  IF ?stp:Use_Loan{Prop:Checked} = False
    stp:FranchiseViewOnly = 0
    HIDE(?stp:FranchiseViewOnly)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSTOCKTYP')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020273'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020273'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020273'&'0')
      ***
    OF ?stp:Use_Loan
      IF ?stp:Use_Loan{Prop:Checked} = True
        UNHIDE(?stp:FranchiseViewOnly)
      END
      IF ?stp:Use_Loan{Prop:Checked} = False
        stp:FranchiseViewOnly = 0
        HIDE(?stp:FranchiseViewOnly)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Stock_Type PROCEDURE                           !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Stock Type File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,114,148,210),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Stock Type'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,100,124,10),USE(stp:Stock_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,188),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,218),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,247),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020268'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock_Type')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCKTYP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Stock_Type')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stp:Stock_Type_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stp:Stock_Type,stp:Stock_Type,1,BRW1)
  BRW1.AddField(stp:Stock_Type,BRW1.Q.stp:Stock_Type)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Stock_Type')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSTOCKTYP
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020268'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020268'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020268'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults




Browse_Exchange PROCEDURE (f_stock_type)              !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::32:TAGDISPSTATUS   BYTE(0)
DASBRW::32:QUEUE          QUEUE
Pointer20                     LIKE(glo:Pointer20)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
ExcludedMan          STRING(300)
save_xch_id          USHORT,AUTO
pos                  STRING(255)
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(40)
Stock_Type_Temp      STRING(30)
Model_Number_Temp    STRING(30)
tmp:allstock         BYTE(0)
tmp:Location         STRING(30)
tmp:AllLocations     BYTE(0)
locCostType          STRING(1)
locMainStoreSelected BYTE
LocalTag             STRING(1)
count                LONG
LocalReason          STRING(100)
LocalLocationFlag    BYTE
ImportFilename       STRING(255),STATIC
CountBadIMEI         LONG
CountBadLocation     LONG
CountBadIMEILocation LONG
CountErrorString     STRING(255)
SendingLocation      STRING(30)
SendingStockType     STRING(30)
BRW1::View:Browse    VIEW(EXCHANGE)
                       PROJECT(xch:ESN)
                       PROJECT(xch:Ref_Number)
                       PROJECT(xch:Model_Number)
                       PROJECT(xch:Location)
                       PROJECT(xch:MSN)
                       PROJECT(xch:Audit_Number)
                       PROJECT(xch:Job_Number)
                       PROJECT(xch:Stock_Type)
                       PROJECT(xch:Available)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
xch:ESN                LIKE(xch:ESN)                  !List box control field - type derived from field
xch:Ref_Number         LIKE(xch:Ref_Number)           !List box control field - type derived from field
xch:Model_Number       LIKE(xch:Model_Number)         !List box control field - type derived from field
xch:Location           LIKE(xch:Location)             !List box control field - type derived from field
xch:MSN                LIKE(xch:MSN)                  !List box control field - type derived from field
locCostType            LIKE(locCostType)              !List box control field - type derived from local data
status_temp            LIKE(status_temp)              !List box control field - type derived from local data
xch:Audit_Number       LIKE(xch:Audit_Number)         !List box control field - type derived from field
xch:Job_Number         LIKE(xch:Job_Number)           !List box control field - type derived from field
xch:Stock_Type         LIKE(xch:Stock_Type)           !Browse key field - type derived from field
xch:Available          LIKE(xch:Available)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK26::xch:Available       LIKE(xch:Available)
HK26::xch:Location        LIKE(xch:Location)
HK26::xch:Model_Number    LIKE(xch:Model_Number)
HK26::xch:Stock_Type      LIKE(xch:Stock_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Exchange Units File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(608,292),USE(?buttonReturnExchangeUnits),TRN,FLAT,ICON('rtnexcp.jpg')
                       PROMPT('Browse The Exchange Unit File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(8,104,600,254),USE(?Browse:1),IMM,HSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('10L(2)|MI@s1@68L(2)|M~I.M.E.I. Number~@s16@38R(2)|M~Unit No~L@s8@75L(2)|M~Model ' &|
   'Number~@s30@80L(2)|M~Location~@s30@50L(2)|M~M.S.N.~@s16@35C|M~Cost Type~@s1@130L' &|
   '(2)|M~Status~@s40@34L(2)|M~Audit No~@s8@32L(2)|M~Job Number~@p<<<<<<<<<<<<<<#pb@'),FROM(Queue:Browse:1)
                       BUTTON,AT(608,198),USE(?btnRapidInsertWizard),TRN,FLAT,ICON('rapinwzp.jpg')
                       PANEL,AT(4,28,672,40),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(8,32),USE(?Prompt1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(84,34,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Loc'),TIP('Loc'),UPR
                       BUTTON,AT(212,30),USE(?LookupSiteLocation),TRN,FLAT,ICON('lookupp.jpg')
                       CHECK('All Locations'),AT(244,34),USE(tmp:AllLocations),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Locations'),TIP('All Locations'),VALUE('1','0')
                       PROMPT('Stock Type'),AT(8,52),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(84,52,124,10),USE(Stock_Type_Temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       BUTTON,AT(212,48),USE(?LookupStockType),TRN,FLAT,ICON('lookupp.jpg')
                       CHECK('All Stock Types'),AT(244,52),USE(tmp:allstock),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Stock Types'),TIP('All Stock Types'),VALUE('1','0')
                       OPTION,AT(560,36,108,20),USE(all_temp),BOXED,COLOR(0D6E7EFH)
                         RADIO('Available'),AT(572,44),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('AVL')
                         RADIO('All'),AT(628,44),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                       END
                       BUTTON,AT(284,362),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(348,362),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(412,362),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(480,370,1,1),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(492,370,1,1),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(476,362),USE(?UnavailCSV),FLAT,ICON('Unavlcsv.jpg')
                       SHEET,AT(4,72,672,322),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By I.M.E.I. Number'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,89,124,10),USE(xch:ESN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Exchange Unit Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@p<<<<<<<<#pb),AT(8,89,64,10),USE(xch:Ref_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Unit Number')
                         END
                         TAB('By Model Number'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@p<<<<<<<<#pb),AT(224,89,64,10),USE(xch:Ref_Number,,?XCH:Ref_Number:2),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY,MSG('Unit Number')
                           ENTRY(@s30),AT(8,90,124,10),USE(Model_Number_Temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(140,86),USE(?buttonLookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('By M.S.N.'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,89,64,10),USE(xch:MSN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(608,134),USE(?ReturnUnitToStock),TRN,FLAT,ICON('uniavp.jpg')
                       BUTTON,AT(608,160),USE(?NotAvailable),TRN,FLAT,ICON('uninavp.jpg')
                       BUTTON,AT(608,226),USE(?MoveStockType),TRN,FLAT,ICON('movestop.jpg')
                       BUTTON,AT(608,262),USE(?Order_Button),TRN,FLAT,ICON('ordstop.jpg')
                       BUTTON,AT(608,330),USE(?UnitHistory),TRN,FLAT,ICON('unihistp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(596,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,644,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(8,362),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(76,362),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(144,362),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(608,104),USE(?Select),TRN,FLAT,ICON('selectp.jpg')
                       BUTTON,AT(608,396),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(608,362),USE(?buttonAvailableQuantity),TRN,FLAT,ICON('availqtp.jpg')
                       BUTTON,AT(8,396),USE(?Button:UnitsInTransit),TRN,FLAT,HIDE,ICON('unittrnp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort16:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort17:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort18:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort19:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort20:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort21:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort22:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort23:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort24:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort25:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort26:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort27:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort28:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort29:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
BRW1::Sort30:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
BRW1::Sort31:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
BRW1::Sort32:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FileLookup33         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile    File,Driver('BASIC'),Pre(impfile),Name(ImportFilename),Create,Bindable,Thread
Record       Record
Manufacturer String(30)
Model        String(30)
IMEI         String(30)
Reason       String(1000)
Location     String(1000)
             End
             End

!Save Entry Fields Incase Of Lookup
look:tmp:Location                Like(tmp:Location)
look:Stock_Type_Temp                Like(Stock_Type_Temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !                !Xplore: Column displaying LocalTag
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying LocalTag
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying xch:ESN
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying xch:ESN
Xplore1Step3         StepLongClass   !LONG            !Xplore: Column displaying xch:Ref_Number
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying xch:Ref_Number
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying xch:Model_Number
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying xch:Model_Number
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying xch:Location
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying xch:Location
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying xch:MSN
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying xch:MSN
Xplore1Step7         StepCustomClass !                !Xplore: Column displaying locCostType
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying locCostType
Xplore1Step8         StepCustomClass !                !Xplore: Column displaying status_temp
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying status_temp
Xplore1Step9         StepRealClass   !REAL            !Xplore: Column displaying xch:Audit_Number
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying xch:Audit_Number
Xplore1Step10        StepLongClass   !LONG            !Xplore: Column displaying xch:Job_Number
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying xch:Job_Number
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying xch:Stock_Type
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying xch:Stock_Type
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying xch:Available
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying xch:Available
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::32:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue20.Pointer20 = xch:Ref_Number
   GET(glo:Queue20,glo:Queue20.Pointer20)
  IF ERRORCODE()
     glo:Queue20.Pointer20 = xch:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
    LocalTag = '*'
  ELSE
    DELETE(glo:Queue20)
    LocalTag = ''
  END
    Queue:Browse:1.LocalTag = LocalTag
  IF (LocalTag = '*')
    Queue:Browse:1.LocalTag_Icon = 2
  ELSE
    Queue:Browse:1.LocalTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::32:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue20)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue20.Pointer20 = xch:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::32:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue20)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::32:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::32:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue20)
    GET(glo:Queue20,QR#)
    DASBRW::32:QUEUE = glo:Queue20
    ADD(DASBRW::32:QUEUE)
  END
  FREE(glo:Queue20)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::32:QUEUE.Pointer20 = xch:Ref_Number
     GET(DASBRW::32:QUEUE,DASBRW::32:QUEUE.Pointer20)
    IF ERRORCODE()
       glo:Queue20.Pointer20 = xch:Ref_Number
       ADD(glo:Queue20,glo:Queue20.Pointer20)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::32:DASSHOWTAG Routine
   CASE DASBRW::32:TAGDISPSTATUS
   OF 0
      DASBRW::32:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::32:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::32:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Exchange','?Browse:1')    !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020263'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Exchange')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?buttonReturnExchangeUnits
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:USERS_ALIAS.Open
  Access:EXCHHIST.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:STOCKTYP.UseFile
  Access:LOAN.UseFile
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  !after opening files - before the window comes in
  !create a limiting list of manufacturers to exclude
  Access:Manufact.clearkey(man:RecordNumberKey)
  set(man:RecordNumberKey,man:RecordNumberKey)
  Loop
      if access:Manufact.next() then break.
      !if man:Notes[1:8] = 'INACTIVE' then
      !TB13214 - change to using field for inactive  - J 05/02/14
      if man:Inactive = 1 then 
          ExcludedMan = clip(ExcludedMan)&'|'&clip(man:Manufacturer)&'|'      !pipes are there to separate manufact names
                                                                              !NB this means missing manufacturers are considered "Excluded" as || will match
      END !if marked as inactive
  END !loop through manufact
  
  !TB12448 Disable Manufacturers  J 22/06/2012
  
  !this will mean I can use
  !if instring(clip(man:Manufacturer),excludedMan,1,1) then ... to exclude models and manufacts
  !TB12448 Disable Manufacturers  J 22/06/2012
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHANGE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Exchange')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbe02app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Location{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?Stock_Type_Temp{Prop:Tip} AND ~?LookupStockType{Prop:Tip}
     ?LookupStockType{Prop:Tip} = 'Select ' & ?Stock_Type_Temp{Prop:Tip}
  END
  IF ?Stock_Type_Temp{Prop:Msg} AND ~?LookupStockType{Prop:Msg}
     ?LookupStockType{Prop:Msg} = 'Select ' & ?Stock_Type_Temp{Prop:Msg}
  END
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?tmp:Location{prop:Disable} = 1
          ?tmp:AllLocations{prop:Disable} = 1
          ?LookupSiteLocation{prop:Hide} = 1
      End !If use:StockFromLocationOnly
      tmp:Location    = use:Location
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
  If SecurityCheck('EXCHANGE UNITS - ORDER')
      Hide(?Order_Button)
  end
  
  If SecurityCheck('EXCHANGE - RAPID INSERT') THEN
     HIDE(?btnRapidInsertWizard)
  end
  
  if glo:WebJob
      hide(?ReturnUnitToStock)
      hide(?NotAvailable)
      hide(?MoveStockType)
      hide(?Insert:3)
      hide(?Change:3)
      hide(?Delete:3)
      BRW1.InsertControl = 0
      BRW1.ChangeControl = 0
      BRW1.DeleteControl = 0
  ! Insert --- Show in transit button (DBH: 17/03/2009) #10473
      !TRB12461 added these buttons which should not be available to webmaster - J - 26/7/12
      hide(?DASTAG)
      hide(?DASTAGAll)
      hide(?DASUNTAGALL)
      hide(?UnavailCSV)
  else
      ?Button:UnitsInTransit{prop:Hide} = 0
  ! end --- (DBH: 17/03/2009) #10473
  end
  
  ! #12127 Record if Main Store selected in a variable to save the file lookup (Bryan: 05/07/2011)
  IF (tmp:Location = MainStoreLocation())
      locMainStoreSelected = 1
  ELSE
      locMainSToreSelected = 0
  END
  
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,xch:LocStockAvailIMEIKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:ESN_Available_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailLocIMEI)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailIMEIOnlyKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockIMEIKey)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:ESN_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:LocIMEIKey)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:ESN_Only_Key)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?xch:ESN,xch:ESN,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockAvailRefKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:Ref_Available_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailLocRef)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailRefOnlyKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockRefKey)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:Ref_Number_Stock_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocRefKey)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(?xch:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockAvailModelKey)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:Model_Available_Key)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailLocModel)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(?XCH:Ref_Number:2,xch:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailModOnlyKey)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockModelKey)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort21:Locator)
  BRW1::Sort21:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:Model_Number_Key)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort22:Locator)
  BRW1::Sort22:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocModelKey)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:ModelRefNoKey)
  BRW1.AddRange(xch:Model_Number)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(?XCH:Ref_Number:2,xch:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockAvailMSNKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:MSN_Available_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort26:Locator)
  BRW1::Sort26:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailLocMSN)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort27:Locator)
  BRW1::Sort27:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:AvailMSNOnlyKey)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort28:Locator)
  BRW1::Sort28:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:LocStockMSNKey)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort29:Locator)
  BRW1::Sort29:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:MSN_Key)
  BRW1.AddRange(xch:Stock_Type)
  BRW1.AddLocator(BRW1::Sort30:Locator)
  BRW1::Sort30:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:LocMSNKey)
  BRW1.AddRange(xch:Location)
  BRW1.AddLocator(BRW1::Sort31:Locator)
  BRW1::Sort31:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:MSN_Only_Key)
  BRW1.AddLocator(BRW1::Sort32:Locator)
  BRW1::Sort32:Locator.Init(?xch:MSN,xch:MSN,1,BRW1)
  BRW1.AddSortOrder(,xch:Ref_Available_Key)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?xch:ESN,xch:Stock_Type,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('LocalTag',LocalTag)
  BIND('locCostType',locCostType)
  BIND('status_temp',status_temp)
  BIND('Model_Number_Temp',Model_Number_Temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  BIND('all_temp',all_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(LocalTag,BRW1.Q.LocalTag)
  BRW1.AddField(xch:ESN,BRW1.Q.xch:ESN)
  BRW1.AddField(xch:Ref_Number,BRW1.Q.xch:Ref_Number)
  BRW1.AddField(xch:Model_Number,BRW1.Q.xch:Model_Number)
  BRW1.AddField(xch:Location,BRW1.Q.xch:Location)
  BRW1.AddField(xch:MSN,BRW1.Q.xch:MSN)
  BRW1.AddField(locCostType,BRW1.Q.locCostType)
  BRW1.AddField(status_temp,BRW1.Q.status_temp)
  BRW1.AddField(xch:Audit_Number,BRW1.Q.xch:Audit_Number)
  BRW1.AddField(xch:Job_Number,BRW1.Q.xch:Job_Number)
  BRW1.AddField(xch:Stock_Type,BRW1.Q.xch:Stock_Type)
  BRW1.AddField(xch:Available,BRW1.Q.xch:Available)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 3
  FileLookup33.Init
  FileLookup33.Flags=BOR(FileLookup33.Flags,FILE:LongName)
  FileLookup33.SetMask('All Files','*.*')
  FileLookup33.WindowTitle='Select Import'
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab:2{PROP:TEXT} = 'By Exchange Unit Number'
    ?Tab:3{PROP:TEXT} = 'By Model Number'
    ?Tab4{PROP:TEXT} = 'By M.S.N.'
    ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#68L(2)|M~I.M.E.I. Number~@s16@#3#38R(2)|M~Unit No~L@s8@#4#75L(2)|M~Model Number~@s30@#5#80L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s16@#7#35C|M~Cost Type~@s1@#8#130L(2)|M~Status~@s40@#9#34L(2)|M~Audit No~@s8@#10#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Exchange','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','Browse_Exchange')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          If SecurityCheck('EXCHANGE UNITS - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              access:exchange.cancelautoinc()
              do_update# = false
          else
              If stock_type_temp = ''
                  Case Missive('You must select a Stock Type.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  access:exchange.cancelautoinc()
                  do_update# = False
              End!If stock_type_temp = ''
          end
      of changerecord
          If SecurityCheck('EXCHANGE UNITS - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end
      of deleterecord
          If SecurityCheck('EXCHANGE UNITS - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end
  end !case request
  
  If do_update# = true
      If Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3
          If Thiswindow.request = Selectrecord
              Case Missive('You cannot Insert / Change / Delete an item while you are updating a job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
  
              If request = Insertrecord
                  access:exchange.cancelautoinc()
              End!If request = Insertrecord
          End!If Globalrequest = Selectrecord
      End !If Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3
  End!If do_update# = true
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      PickExchangeStockType
      Update_Exchange
    END
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(LocalTag,BRW1.Q.LocalTag)
  Xplore1.AddField(xch:ESN,BRW1.Q.xch:ESN)
  Xplore1.AddField(xch:Ref_Number,BRW1.Q.xch:Ref_Number)
  Xplore1.AddField(xch:Model_Number,BRW1.Q.xch:Model_Number)
  Xplore1.AddField(xch:Location,BRW1.Q.xch:Location)
  Xplore1.AddField(xch:MSN,BRW1.Q.xch:MSN)
  Xplore1.AddField(locCostType,BRW1.Q.locCostType)
  Xplore1.AddField(status_temp,BRW1.Q.status_temp)
  Xplore1.AddField(xch:Audit_Number,BRW1.Q.xch:Audit_Number)
  Xplore1.AddField(xch:Job_Number,BRW1.Q.xch:Job_Number)
  Xplore1.AddField(xch:Stock_Type,BRW1.Q.xch:Stock_Type)
  Xplore1.AddField(xch:Available,BRW1.Q.xch:Available)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,xch:Ref_Available_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(xch:Available)                        !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?buttonReturnExchangeUnits
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnExchangeUnits, Accepted)
      If SecurityCheck('RETURN EXCHANGE UNITS')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonReturnExchangeUnits, Accepted)
    OF ?UnavailCSV
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnavailCSV, Accepted)
      If SecurityCheck('EXCHANGE - NOT AVAILABLE')
          miss# = Missive('You do not have access to this option.','ServiceBase 3g','mstop.jpg','/OK')
          Cycle
      End !SecurityCheck('EXCHANGE - NOT AVAILABLE')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnavailCSV, Accepted)
    OF ?Order_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Button, Accepted)
      IF (tmp:ALLLocations = 0)
          IF (tmp:Location <> '')
              ExchangeLoanOrderWindow(tmp:Location,'EXC')  ! #12347 ! Combine routine into Exchange & Loan (DBH: 19/01/2012)
          END ! IF (tmp:Location <> '')
      END ! IF (tmp:ALLAllocations = 0)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Button, Accepted)
    OF ?UnitHistory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnitHistory, Accepted)
      glo:Select12    = brw1.q.xch:Ref_Number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnitHistory, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?buttonReturnExchangeUnits
      ThisWindow.Update
      ReturnExchangeProcess
      ThisWindow.Reset
    OF ?btnRapidInsertWizard
      ThisWindow.Update
      Rapid_Exchange_Unit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnRapidInsertWizard, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnRapidInsertWizard, Accepted)
    OF ?tmp:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
      ! #12127 Record if Main Store selected in a variable to save the file lookup (Bryan: 05/07/2011)
      IF (tmp:Location = MainStoreLocation())
          locMainStoreSelected = 1
      ELSE
          locMainSToreSelected = 0
      END
      BRW1.ResetSort(1)
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loc:Location = tmp:Location
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = tmp:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:AllLocations
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllLocations, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllLocations, Accepted)
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      IF Stock_Type_Temp OR ?Stock_Type_Temp{Prop:Req}
        stp:Stock_Type = Stock_Type_Temp
        stp:Use_Exchange = 'YES'
        !Save Lookup Field Incase Of error
        look:Stock_Type_Temp        = Stock_Type_Temp
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Stock_Type_Temp = stp:Stock_Type
          ELSE
            CLEAR(stp:Use_Exchange)
            !Restore Lookup On Error
            Stock_Type_Temp = look:Stock_Type_Temp
            SELECT(?Stock_Type_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset(0)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    OF ?LookupStockType
      ThisWindow.Update
      stp:Stock_Type = Stock_Type_Temp
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          Stock_Type_Temp = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?Stock_Type_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Stock_Type_Temp)
    OF ?tmp:allstock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
    OF ?all_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
      Thiswindow.reset
      Select(?Browse:1)
      If thiswindow.request <> Selectrecord
          Case all_temp
              Of 'YES'
                  Unhide(?ReturnUnitToStock)
              Of 'NO'
                  Hide(?ReturnUnitToStock)
          End!Case all_temp
      End!If thiswindow.request <> Selectrecord
      
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?UnavailCSV
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnavailCSV, Accepted)
      ImportFilename = FileLookup33.Ask(1)
      DISPLAY
          !will need the user
          access:users.clearkey(use:password_key)
          use:password        = glo:password
          access:users.fetch(use:password_key)
      
          !Harvey wants some silly counts so
          CountBadIMEI         = 0
          CountBadLocation     = 0
          CountBadIMEILocation = 0
      
          Open(ImportFile)
          set(ImportFile)
      
          Loop
      
              next(ImportFile)
              if error() then break.
      
              !fiind the unit in the exchange file
              Access:Exchange.clearkey(xch:ESN_Only_Key)
              xch:ESN = impfile:IMEI
              if access:Exchange.fetch(xch:ESN_Only_Key)
                  !this has already gone wrong - but harvey wants a complex count so
                  !Second check for location being correct
                  Access:location.clearkey(loc:Location_Key)
                  loc:Location = upper(impfile:Location)
                  if access:location.fetch(loc:Location_Key)
                      !not found
                      CountBadIMEILocation += 1
                  ELSE
                      CountBadIMEI += 1
                  END
      
      
              ELSE
                  !found a reacord - is the location valid?
      
                  !Second check for location being correct
                  Access:location.clearkey(loc:Location_Key)
                  loc:Location = upper(impfile:Location)
                  if access:location.fetch(loc:Location_Key)
                      !not found
                      CountBadLocation += 1
                  ELSE
                      !all is correct
                      xch:Available = 'NOA'
                      xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                      xch:Location = upper(impfile:Location)
                      Access:EXCHANGE.Update()
      
                      get(exchhist,0)
                      if access:exchhist.primerecord() = Level:Benign
                          exh:ref_number      = xch:ref_number
                          exh:date            = Today()
                          exh:time            = Clock()
                          exh:user            = use:user_code
                          exh:status          = UPPER(impfile:Reason)
                          access:exchhist.insert()
                      end!if access:exchhist.primerecord() = Level:Benign
                  END
              END   !if found in the exchange list
          END !loop
      
          close(importFile)
      
          if CountBadIMEI + CountBadLocation + CountBadIMEILocation = 0 then
              miss# = Missive('File imported','ServiceBase 3g','mexclam.jpg','/OK')
          ELSE
              !create a bespoke error report
              CountErrorString = 'File import completed with the following errors:|'
              if CountBadIMEI > 0 then
                  CountErrorString = clip(countErrorString)&' '&clip(CountBadIMEI)&' lines could not be imported due to unknown IMEI|'
              END
              if CountBadLocation > 0 then
                  CountErrorString = clip(countErrorString)&' '&clip(CountBadLocation)&' lines could not be imported due to unknown Site Location|'
              END
              if CountBadIMEILocation > 0 then
                  CountErrorString = clip(countErrorString)&' '&clip(CountBadIMEILocation)&' lines could not be imported due to unknown IMEI and unknown Site Location'
              END
              miss# = Missive(clip(CountErrorString),'ServiceBase 3g','mwarn.jpg','\OK')
          END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnavailCSV, Accepted)
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    OF ?buttonLookup
      ThisWindow.Update
      ! Bryan Simple Lookup
      CLEAR(mod:Record)
      mod:Model_Number = Model_Number_Temp
      IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key))
          ! Failed, set the key instead
          Clear(mod:Record)
          mod:Model_Number = Model_Number_Temp
          SET(mod:Model_Number_Key,mod:Model_Number_Key)
          Access:MODELNUM.TryNext()
      END ! IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key))
      GlobalRequest = SelectRecord
      SelectModel()
      IF (GlobalResponse = RequestCompleted)
          CHANGE(?Model_Number_Temp,mod:Model_Number)
          POST(Event:Accepted,?Model_Number_Temp)
          SELF.Request = SELF.OriginalRequest
      ELSE ! IF (GlobalResponse = RequestCompleted)
          SELECT(?buttonLookup)
          SELF.Request = SELF.OriginalRequest
      END ! IF (GlobalResponse = RequestCompleted)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ReturnUnitToStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
      If SecurityCheck('EXCHANGE - MAKE AVAILABLE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('EXCHANGE - MAKE AVAILABLE')
          brw1.UpdateViewRecord()
          Error# = 0
          If xch:Job_Number <> ''
              Case Missive('You are about to remove this unit from Job Number ' & Clip(xch:Job_Number) & '.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      Error# = 1
              End ! Case Missive
          Else !xch:Job_Number <> ''
              Case Missive('Are you sure you want to make this unit "Available"?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      Error# = 1
              End ! Case Missive
          End !xch:Job_Number <> ''
          If Error# = 0
              ! Inserting (DBH 21/07/2008) # 10130 - Cannot make available if it's already available in Loan Stock.
              Access:LOAN.ClearKey(loa:ESN_Only_Key)
              loa:ESN = xch:ESN
              If Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign
                  !Found
                  If loa:Available <> 'NOA'
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('Cannot make this unit available. It is already "Available" in Loan Stock.','ServiceBase 3g',|
                                     'mstop.jpg','/&OK')
                          Of 1 ! &OK Button
                      End!Case Message
                      Cycle
                  End ! If loa:Available <> 'NOA'
              Else ! If Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign
                  !Error
              End ! If Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign
              ! End (DBH 21/07/2008) #10130
      
              xch:job_number = ''
              xch:available = 'AVL'
              xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
              access:exchange.update()
              get(exchhist,0)
              if access:exchhist.primerecord() = level:benign
                  exh:ref_number    = xch:ref_number
                  exh:date          = Today()
                  exh:time          = Clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  exh:user = use:user_code
                  If xch:Job_Number <> ''
                      exh:status        = 'UNIT RETURNED TO STOCK FROM JOB: ' & Clip(Format(xch:Job_Number,@p<<<<<#p))
                  Else !If xch:Job_Number <> ''
                      exh:Status      = 'UNIT AVAILABLE'
                  End !If xch:Job_Number <> ''
      
                  if access:exchhist.insert()
                      access:exchhist.cancelautoinc()
                  end
              end!if access:loanhist.primerecord() = level:benign
          End!Case MessageEx
      
      End !SecurityCheck('EXCHANGE - MAKE AVAILABLE')
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
    OF ?NotAvailable
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NotAvailable, Accepted)
      If SecurityCheck('EXCHANGE - NOT AVAILABLE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('EXCHANGE - NOT AVAILABLE')
          if records(glo:queue20) = 0 then
              miss# = Missive('You must tag some items before you can use this procedure','ServiceBase 3g','mstop.jpg','/OK')
      
          ELSE
              Case Missive('Are you sure you want to make the tagged units "Not Available"?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      !brw1.UpdateViewRecord()
                      if Missive('Should these units be added to the Return to Manufacturer location (RTM), or left in the current location?',|
                                  'ServiceBase 3g','mstop.jpg','/RTM|\Current')  = 1 then 
                          !return to manufacturer
                          LocalLocationFlag = true
      
                      ELSE
                          !leave in current
                          LocalLocationFlag = false
                      END
      
                      LocalReason = Clip(NotAvailableReason())
                      loop count = 1 to records(glo:Queue20)
                          Get(glo:Queue20,count)
                          if error() then break.
      
                          Access:Exchange.clearkey(xch:Ref_Number_Key)
                          xch:Ref_Number = GLO:Pointer20
                          if access:Exchange.fetch(xch:Ref_Number_Key)
                              !eh?
                          END
                          xch:Available = 'NOA'
                          xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                          if LocalLocationFlag then
                              xch:Location = 'RETURN TO MANUFACTURER'
                          END
                          Access:EXCHANGE.Update()
      
                          get(exchhist,0)
                          if access:exchhist.primerecord() = Level:Benign
                              exh:ref_number      = xch:ref_number
                              exh:date            = Today()
                              exh:time            = Clock()
                              access:users.clearkey(use:password_key)
                              use:password        = glo:password
                              access:users.fetch(use:password_key)
                              exh:user = use:user_code
                              exh:status          = clip(LocalReason)
                              access:exchhist.insert()
                          end!if access:exchhist.primerecord() = Level:Benign
                      END !Loop through the queue
                      FREE(glo:queue20)
                  Of 1 ! No Button
      
              End ! Case Missive
          END !if something was tagged
      End !SecurityCheck('EXCHANGE - NOT AVAILABLE')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NotAvailable, Accepted)
    OF ?MoveStockType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
      If SecurityCheck('EXCHANGE - MOVE STOCK TYPE')
        Case Missive('You do not have access to this option.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
      Else !SecurityCheck('EXCHANGE - MOVE STOCK TYPE')
      
      !TB03229 - J - 06/05/14 - process moved to another window shared with Loans
          !do not call if the location or type have not been set
          if tmp:AllLocations = false and clip(tmp:Location)   = '' then cycle.
          if tmp:allstock     = false and clip(Stock_Type_Temp)= '' then cycle.
      
          !some records exist - call the window (SentLoanOrExchange,SentLocation,SentStockType)
          if tmp:AllLocations then
              SendingLocation = 'ALL'
          ELSE
              SendingLocation =  tmp:Location
          END
          if tmp:allstock then
              SendingStockType = 'ALL'
          ELSE
              SendingStockType = Stock_Type_Temp
          END
          MoveStockType('X',SendingLocation,SendingStockType)
          DO DASBRW::32:DASUNTAGALL
      
      !TB03229 - removed existing code
      !  Case Missive('Do you wish to move the selected unit to a new Stock Type, or move ALL units to another Stock Type?','ServiceBase 3g',|
      !                 'mquest.jpg','\Cancel|Move All|Move One')
      !      Of 3 ! Move One Button
      !          Case Missive('You have selected to move the SELECTED unit to another Stock Type.'&|
      !            '<13,10>'&|
      !            '<13,10>Are your sure?','ServiceBase 3g',|
      !                         'mquest.jpg','\No|/Yes')
      !              Of 2 ! Yes Button
      !                    saverequest#      = globalrequest
      !                    globalresponse    = requestcancelled
      !                    globalrequest     = selectrecord
      !                    glo:select1       = 'EXC'
      !                    select_stock_type
      !                    if globalresponse = requestcompleted
      !                        access:exchange.clearkey(xch:ref_number_key)
      !                        xch:ref_number = brw1.q.xch:ref_number
      !                        if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
      !                            xch:stock_type  = stp:stock_type
      !                            access:exchange.update()
      !                            get(exchhist,0)
      !                            if access:exchhist.primerecord() = level:benign
      !                                exh:ref_number      = xch:ref_number
      !                                exh:date            = today()
      !                                exh:time            = clock()
      !                                access:users.clearkey(use:password_key)
      !                                use:password        = glo:password
      !                                access:users.fetch(use:password_key)
      !                                exh:user = use:user_code
      !                                exh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
      !                                                       ' TO ' & Clip(stp:stock_type)
      !                                access:exchhist.insert()
      !                            end!if access:exchhist.primerecord() = level:benign
      !
      !                        End!if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
      !                    end
      !                    globalrequest     = saverequest#
      !                    glo:select1       = ''
      !              Of 1 ! No Button
      !          End ! Case Missive
      !      Of 2 ! Move All Button
      !            Case Missive('You have selected to move ALL units, not matter what status, to a NEW Stock Type.'&|
      !              '<13,10>'&|
      !              '<13,10>Are you sure?','ServiceBase 3g',|
      !                           'mquest.jpg','\No|/Yes')
      !                Of 2 ! Yes Button
      !                    saverequest#      = globalrequest
      !                    globalresponse    = requestcancelled
      !                    globalrequest     = selectrecord
      !                    glo:select1       = 'EXC'
      !                    select_stock_type
      !                    if globalresponse = requestcompleted
      !                        setcursor(cursor:wait)
      !                        save_xch_id = access:exchange.savefile()
      !                        access:exchange.clearkey(xch:ref_number_stock_key)
      !                        xch:stock_type = stock_type_temp
      !                        set(xch:ref_number_stock_key,xch:ref_number_stock_key)
      !                        loop
      !                            if access:exchange.next()
      !                               break
      !                            end !if
      !                            if xch:stock_type <> stock_type_temp      |
      !                                then break.  ! end if
      !                            pos = Position(xch:ref_number_stock_key)
      !                            xch:stock_type  = stp:stock_type
      !                            access:exchange.update()
      !                            get(exchhist,0)
      !                            if access:exchhist.primerecord() = level:benign
      !                                exh:ref_number      = xch:ref_number
      !                                exh:date            = today()
      !                                exh:time            = clock()
      !                                access:users.clearkey(use:password_key)
      !                                use:password        = glo:password
      !                                access:users.fetch(use:password_key)
      !                                exh:user = use:user_code
      !                                exh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
      !                                                       ' TO ' & Clip(stp:stock_type)
      !                                access:exchhist.insert()
      !                            end!if access:exchhist.primerecord() = level:benign
      !
      !                            Reset(xch:ref_number_stock_key,pos)
      !                        end !loop
      !                        access:exchange.restorefile(save_xch_id)
      !                        setcursor()
      !                    end
      !                    globalrequest     = saverequest#
      !                    glo:select1       = ''
      !
      !                Of 1 ! No Button
      !            End ! Case Missive
      !      Of 1 ! Cancel Button
      !  End ! Case Missive
      !TB03229 - end of changes
      
      End !SecurityCheck('EXCHANGE - MOVE STOCK TYPE')
      BRW1.ResetQueue(Reset:queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
    OF ?UnitHistory
      ThisWindow.Update
      Browse_Exchange_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnitHistory, Accepted)
      glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UnitHistory, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020263'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020263'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020263'&'0')
      ***
    OF ?Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
      thiswindow.reset(1)
      error# = 0
      If xch:job_number <> 0
          Case Missive('This unit is already attached to this job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If xch:job_number > 0
      If error# = 0
          glo:select1 = xch:ref_number
          Post(Event:closewindow)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
    OF ?buttonAvailableQuantity
      ThisWindow.Update
      ExchangeAvailableQuantity
      ThisWindow.Reset
    OF ?Button:UnitsInTransit
      ThisWindow.Update
      UnitsInTransit
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
    ! Insert --- Disable "Make Available" button if in transit (DBH: 17/04/2009) #10473
    if (xch:Available = 'IT4' or xch:Available = 'ITS' or xch:Available = 'ITR' or xch:Available = 'ITP')
        ?ReturnUnitToStock{prop:Disable} = 1
    else ! if (xch:Status = '48H')
        ?ReturnUnitToStock{prop:Disable} = 0
    end ! if (xch:Status = '48H')
    ! end --- (DBH: 17/04/2009) #10473
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#68L(2)|M~I.M.E.I. Number~@s16@#3#38R(2)|M~Unit No~L@s8@#4#75L(2)|M~Model Number~@s30@#5#80L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s16@#7#35C|M~Cost Type~@s1@#8#130L(2)|M~Status~@s40@#9#34L(2)|M~Audit No~@s8@#10#'
          ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#38R(2)|M~Unit No~L@s8@#4#68L(2)|M~I.M.E.I. Number~@s16@#3#75L(2)|M~Model Number~@s30@#5#80L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s16@#7#35C|M~Cost Type~@s1@#8#130L(2)|M~Status~@s40@#9#34L(2)|M~Audit No~@s8@#10#'
          ?Tab:2{PROP:TEXT} = 'By Exchange Unit Number'
        OF 3
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#68L(2)|M~I.M.E.I. Number~@s16@#3#38R(2)|M~Unit No~L@s8@#4#75L(2)|M~Model Number~@s30@#5#80L(2)|M~Location~@s30@#6#50L(2)|M~M.S.N.~@s16@#7#35C|M~Cost Type~@s1@#8#130L(2)|M~Status~@s40@#9#34L(2)|M~Audit No~@s8@#10#'
          ?Tab:3{PROP:TEXT} = 'By Model Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='10L(2)|MI@s1@#1#50L(2)|M~M.S.N.~@s16@#7#68L(2)|M~I.M.E.I. Number~@s16@#3#38R(2)|M~Unit No~L@s8@#4#75L(2)|M~Model Number~@s30@#5#80L(2)|M~Location~@s30@#6#35C|M~Cost Type~@s1@#8#130L(2)|M~Status~@s40@#9#34L(2)|M~Audit No~@s8@#10#'
          ?Tab4{PROP:TEXT} = 'By M.S.N.'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?XCH:Ref_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XCH:Ref_Number:2, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XCH:Ref_Number:2, Selected)
    OF ?xch:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      glo:select1 = ''
      If thiswindow.request = SelectRecord
          UnHide(?Select)
          Hide(?ReturnUnitToStock)
      End
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(15,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(16,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(17,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(18,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(19,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(20,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(21,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(22,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(23,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(24,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(25,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(26,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(27,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'AVL' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(28,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(29,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 0 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(30,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 0
    RETURN SELF.SetSort(31,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(All_Temp) = 'YES' And Upper(tmp:AllStock) = 1 And Upper(tmp:AllLocations) = 1
    RETURN SELF.SetSort(32,Force)
  ELSE
    RETURN SELF.SetSort(33,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = xch:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  status_Temp = GetExchangeStatus(xch:Available,xch:Job_Number)
  
  
  IF (locMainStoreSelected = 1)
    locCostType = xch:FreeStockPurchased
  ELSE
    locCostType = ''
  END
  PARENT.SetQueueRecord
  IF (LocalTag = '*')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END
  SELF.Q.locCostType = locCostType                    !Assign formula result to display queue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  if instring('|'&clip(xch:Manufacturer)&'|',excludedMan,1,1) then RETURN(RECORD:FILTERED).
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = xch:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    EXECUTE DASBRW::32:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !LocalTag
  OF 3 !xch:ESN
  OF 4 !xch:Ref_Number
  OF 5 !xch:Model_Number
  OF 6 !xch:Location
  OF 7 !xch:MSN
  OF 8 !locCostType
  OF 9 !status_temp
  OF 10 !xch:Audit_Number
  OF 11 !xch:Job_Number
  OF 12 !xch:Stock_Type
  OF 13 !xch:Available
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step12,xch:Ref_Available_Key)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,xch:Ref_Available_Key)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?xch:ESN,LocalTag,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(?xch:ESN,xch:ESN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(?xch:Ref_Number,xch:Ref_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(?xch:ESN,xch:Model_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(?xch:ESN,xch:Location,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(?xch:MSN,xch:MSN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(?xch:ESN,locCostType,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(?xch:ESN,status_temp,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(?xch:ESN,xch:Audit_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(?xch:ESN,xch:Job_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(?xch:ESN,xch:Stock_Type,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(?xch:ESN,xch:Available,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(xch:Available)
  SELF.BC.AddResetField(all_temp)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(EXCHANGE)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('LocalTag')                   !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('LocalTag')                   !Header
                PSTRING('@S20')                       !Picture
                PSTRING('LocalTag')                   !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:ESN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s16')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Unit No')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Unit Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:MSN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('M.S.N.')                     !Header
                PSTRING('@s16')                       !Picture
                PSTRING('M.S.N.')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('locCostType')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('locCostType')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('locCostType')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('status_temp')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('status_temp')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('status_temp')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Audit_Number')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Audit No')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Audit No')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Job_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@p<<<<<<<<<<<<<<#pb')        !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Stock_Type')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Stock Type')                 !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Stock Type')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('xch:Available')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Available')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Available')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(12)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?all_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?XCH:Ref_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Model_Number_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:Ref_Number:2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:ESN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:MSN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

UnitsInTransit PROCEDURE                              !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Tag              STRING(1)
save_exchange_id     USHORT,AUTO
tmp:True             BYTE(1)
tmp:IMEINumber       STRING(30)
BRW8::View:Browse    VIEW(EXCHANGE)
                       PROJECT(xch:ESN)
                       PROJECT(xch:Ref_Number)
                       PROJECT(xch:InTransit)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
xch:ESN                LIKE(xch:ESN)                  !List box control field - type derived from field
xch:Ref_Number         LIKE(xch:Ref_Number)           !Primary key field - type derived from field
xch:InTransit          LIKE(xch:InTransit)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,310),USE(?Panel5),FILL(09A6A7CH)
                       ENTRY(@s30),AT(104,82,124,10),USE(xch:ESN),FONT(,,01010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       LIST,AT(104,96,196,234),USE(?List),IMM,FONT(,,01010101H,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~I.M.E.I. Number~@s30@'),FROM(Queue:Browse)
                       GROUP('Enter I.M.E.I. Number To Tag Unit'),AT(352,180,240,36),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('I.M.E.I. Number'),AT(359,196),USE(?tmp:IMEINumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(456,196,124,10),USE(tmp:IMEINumber),FONT(,,01010101H,FONT:bold),COLOR(COLOR:White),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                       END
                       BUTTON('&Tag'),AT(133,179,32,13),USE(?DASTAG),HIDE
                       BUTTON('tag &All'),AT(137,201,45,13),USE(?DASTAGAll),HIDE
                       BUTTON,AT(104,332),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(145,243,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(149,263,70,13),USE(?DASSHOWTAG),HIDE
                       GROUP('Finish && Receive Tagged Units'),AT(440,286,148,44),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(480,299),USE(?Button:ReceiveIntoStores),TRN,FLAT,ICON('retarcp.jpg')
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Units In Transit To ARC Stores'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = xch:Ref_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = xch:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = xch:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = xch:Ref_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = xch:Ref_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020729'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UnitsInTransit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:EXCHANGE,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UnitsInTransit')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,xch:InTransitKey)
  BRW8.AddRange(xch:InTransit,tmp:True)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?xch:ESN,xch:ESN,1,BRW8)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:Tag,BRW8.Q.tmp:Tag)
  BRW8.AddField(xch:ESN,BRW8.Q.xch:ESN)
  BRW8.AddField(xch:Ref_Number,BRW8.Q.xch:Ref_Number)
  BRW8.AddField(xch:InTransit,BRW8.Q.xch:InTransit)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UnitsInTransit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:IMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
          save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
          Access:EXCHANGE.Clearkey(xch:InTransitKey)
          xch:InTransit    = 1
          xch:ESN    = tmp:IMEINumber
          if (Access:EXCHANGE.TryFetch(xch:InTransitKey) = Level:Benign)
              ! Found
          else ! if (Access:EXCHANGE.TryFetch(xch:InTransitKey) = Level:Benign)
              ! Error
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('Cannot find the selected I.M.E.I. Number.'&|
                  '|','ServiceBase',|
                             'mexclam.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              select(?tmp:IMEINumber)
              cycle
          end ! if (Access:EXCHANGE.TryFetch(xch:InTransitKey) = Level:Benign)
      
          glo:Queue.Pointer = xch:Ref_Number
          get(glo:Queue,glo:Queue.Pointer)
          if (error())
              glo:Queue.Pointer = xch:Ref_Number
              add(glo:Queue,glo:Queue.Pointer)
          else ! if (error())
              ! Already Been Tagged
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('The selected I.M.E.I. Number has already been tagged.','ServiceBase',|
                             'mexclam.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
          end ! if (error())
          Access:EXCHANGE.RestoreFile(save_EXCHANGE_ID)
          BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:ReceiveIntoStores
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ReceiveIntoStores, Accepted)
      if (SecurityCheck('RECEIVE INTO ARC STORES'))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      end ! if (SecurityCheck('RECEIVE INTO ARC STORES'))
      
      if (Records(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any units.','ServiceBase',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      end ! if (Records(glo:Queue) = 0)
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to receive the tagged units?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes')
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              Cycle
      End!Case Message
      
      loop x# = 1 to records(glo:Queue)
          get(glo:Queue,x#)
          save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number    = glo:Pointer
          if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
              ! Found
              if (xch:InTransit = 1)
                  if (xch:Ref_Number > 0)
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number    = xch:Job_Number
                      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                          ! Found
                          if (JobInUse(job:Ref_Number,1))
                              cycle
                          end ! if (JobInUse(job:Ref_Number,1))
                          GetStatus(815,1,'JOB') ! Return To Exchange Stock
                          if (Access:JOBS.TryUpdate())
                              Cycle
                          else ! if (Access:JOBS.TryUpdate())
                          end ! if (Access:JOBS.TryUpdate())
                      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                      if (xch:Available = 'IT4')
                          ! If 48 Hr. Mark the original job as returned to exchange stock too (DBH: 24/03/2009) #10473
                          Access:JOBS.Clearkey(job:ESN_Key)
                          job:ESN    = xch:ESN
                          set(job:ESN_Key,job:ESN_Key)
                          loop
                              if (Access:JOBS.Next())
                                  Break
                              end ! if (Access:JOBS.Next())
                              if (job:ESN    <> xch:ESN)
                                  Break
                              end ! if (job:ESN    <> xch:ESN)
                              if (sub(job:Current_Status,1,3) = '734')
                                  if (JobInUse(job:Ref_Number,1))
                                      cycle
                                  end ! if (JobInUse(job:Ref_Number,1))
                                  GetStatus(815,1,'JOB') ! Return To Exchange Stock
                                  if (Access:JOBS.TryUpdate())
                                      Cycle
                                  end ! if (Access:JOBS.TryUpdate())
                              end ! if (sub(job:Current_Status,1,3) = '734')
                          end ! loop
                      end ! if (f:Available = 'IT4')
                  else ! end ! if (xch:Ref_Number > 0)
                      Beep(Beep:SystemExclamation)  ;  Yield()
                      Case Missive('Unable to find the job number (' & Clip(xch:Ref_Number) & ') associated with unit ' & Clip(xch:ESN) & '.'&|
                          '|'&|
                          '|Do you wish to mark the unit as available anyway?','ServiceBase',|
                                     'mexclam.jpg','&No|&Yes')
                          Of 2 ! &Yes Button
                          Of 1 ! &No Button
                              cycle
                      End!Case Message
                  end ! if (xch:Ref_Number > 0)
                  xch:Job_Number = ''
                  xch:InTransit = 0
                  xch:Available = 'AVL'
                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                  if (Access:EXCHANGE.TryUpdate())
                  else ! if (Access:EXCHANGE.TryUpdate())
                      if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                          exh:Ref_Number = xch:Ref_Number
                          exh:Date = Today()
                          exh:Time = Clock()
                          Access:USERS.Clearkey(use:Password_Key)
                          use:Password    = glo:Password
                          if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                              ! Found
                          else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                              ! Error
                          end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                          exh:User = use:User_Code
                          exh:Status = 'UNIT AVAILABLE: RESTOCKED'
                          if (Access:EXCHHIST.TryInsert() = Level:Benign)
                              ! Inserted
                          else ! if Access:EXCHHIST.TryInsert() = Level:Benign
                              ! Error
                              Access:EXCHHIST.CancelAutoInc()
                          end ! if Access:EXCHHIST.TryInsert() = Level:Benign
                      end ! if Access:EXCHHIST.PrimeRecord() = Level:Benign
      
                  end ! if (Access:EXCHANGE.TryUpdate())
      
              end ! if (xch:InTrasit = 1)
          else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          Access:EXCHANGE.RestoreFile(save_EXCHANGE_ID)
      end ! loop x# = 1 to records(glo:Queue)
      Post(event:Accepted,?DasUnTagAll)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ReceiveIntoStores, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020729'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020729'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020729'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = xch:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = xch:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

Update_Exchange_By_Audit PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
stock_unit_status    STRING(30)
replacement_unit_status STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::exa:Record  LIKE(exa:RECORD),STATIC
QuickWindow          WINDOW('Update the EXCHANGE File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PANEL,AT(64,54,552,20),USE(?Panel1:2),FILL(09A6A7CH)
                       PROMPT('Audit Number'),AT(68,58),USE(?EXA:Audit_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(148,58,64,10),USE(exa:Audit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                       SHEET,AT(64,76,276,288),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Unit'),USE(?Tab1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Exchange Unit Number'),AT(68,92),USE(?XCH:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(164,92,64,10),USE(exa:Stock_Unit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(68,108),USE(?stock_unit_status:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,108,124,10),USE(stock_unit_status),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(68,124),USE(?XCH:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,124,64,10),USE(xch:ESN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('M.S.N.'),AT(68,137),USE(?XCH:MSN:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,137,64,10),USE(xch:MSN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(68,148),USE(?XCH:Model_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,148,124,10),USE(xch:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Manufacturer'),AT(68,164),USE(?XCH:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,161,124,10),USE(xch:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(68,180),USE(?XCH:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,180,124,10),USE(xch:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(68,196),USE(?XCH:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,196,124,10),USE(xch:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Stock Type'),AT(68,212),USE(?XCH:Stock_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(164,212,124,10),USE(xch:Stock_Type),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchanged On Job'),AT(68,228),USE(?XCH:Job_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@p<<<<<<<#pb),AT(164,228,64,10),USE(xch:Job_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Job Number')
                         END
                       END
                       SHEET,AT(344,76,272,288),USE(?Sheet1:2),COLOR(0D6E7EFH),SPREAD
                         TAB('Replacement Unit'),USE(?Tab2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Exchange Unit Number'),AT(348,92),USE(?XCH:Ref_Number:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(444,92,64,10),USE(exa:Replacement_Unit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(348,108),USE(?replacement_unit_status:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,108,124,10),USE(replacement_unit_status),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(348,124),USE(?XCH:ESN:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,124,64,10),USE(xch_ali:ESN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('M.S.N.'),AT(348,137),USE(?XCH:MSN:Prompt:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,137,64,10),USE(xch_ali:MSN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(348,148),USE(?XCH:Model_Number:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,148,124,10),USE(xch_ali:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Manufacturer'),AT(348,161),USE(?XCH:Manufacturer:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,161,124,10),USE(xch_ali:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(348,180),USE(?XCH:Location:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,180,124,10),USE(xch_ali:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(348,196),USE(?XCH:Shelf_Location:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,196,124,10),USE(xch_ali:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Stock Type'),AT(348,212),USE(?XCH:Stock_Type:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(444,212,124,10),USE(xch_ali:Stock_Type),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchanged On Job'),AT(348,228),USE(?XCH:Job_Number:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@p<<<<<<<#pb),AT(444,228,64,10),USE(xch_ali:Job_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY,MSG('Job Number')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Compare Exchange Units'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020275'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Exchange_By_Audit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OK
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(exa:Record,History::exa:Record)
  SELF.AddHistoryField(?exa:Audit_Number,3)
  SELF.AddHistoryField(?exa:Stock_Unit_Number,4)
  SELF.AddHistoryField(?exa:Replacement_Unit_Number,5)
  SELF.AddUpdateFile(Access:EXCAUDIT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCAUDIT.Open
  Access:EXCHANGE.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCAUDIT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Exchange_By_Audit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  access:exchange.clearkey(xch:ref_number_key)
  xch:ref_number = EXA:Stock_Unit_Number
  access:exchange.fetch(xch:ref_number_key)
  
  access:exchange_alias.clearkey(xch_ali:ref_number_key)
  xch_ali:ref_number = EXA:Replacement_Unit_Number
  If access:exchange_alias.fetch(xch_ali:ref_number_key)
      hide(?tab2)
  End!If access:exchange_alias.fetch(xch_ali:ref_number_key)
  
  Case XCH:Available
      Of 'AVL'
          stock_unit_status = 'AVAILABLE'
      Of 'EXC'
          stock_unit_status = 'EXCHANGED'
      Of 'INC'
          stock_unit_status = 'INCOMING TRANSIT'
      Of 'REP'
          stock_unit_status = 'IN REPAIR'
      Of 'DES'
          stock_unit_status = 'DESPATCHED'
      OF 'SUS'
          stock_unit_status = 'SUSPENDED'
  End!Case XCH:Available
  Case XCH_ALI:Available
      Of 'AVL'
          replacement_unit_status = 'AVAILABLE'
      Of 'EXC'
          replacement_unit_status = 'EXCHANGED'
      Of 'INC'
          replacement_unit_status = 'INCOMING TRANSIT'
      Of 'REP'
          replacement_unit_status = 'IN REPAIR'
      Of 'DES'
          replacement_unit_status = 'DESPATCHED'
      OF 'SUS'
          replacement_unit_status = 'SUSPENDED'
  End!Case XCH:Available
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Exchange_By_Audit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020275'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020275'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020275'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Exchange_By_Audit PROCEDURE (f_stock_type)     !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(30)
Stock_Type_Temp      STRING(30)
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type_Temp
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                       PROJECT(exa:Stock_Unit_Number)
                       PROJECT(exa:Replacement_Unit_Number)
                       PROJECT(exa:Record_Number)
                       PROJECT(exa:Stock_Type)
                       JOIN(xch_ali:Ref_Number_Key,exa:Replacement_Unit_Number)
                         PROJECT(xch_ali:ESN)
                         PROJECT(xch_ali:Model_Number)
                         PROJECT(xch_ali:Ref_Number)
                       END
                       JOIN(xch:Ref_Number_Key,exa:Stock_Unit_Number)
                         PROJECT(xch:ESN)
                         PROJECT(xch:Model_Number)
                         PROJECT(xch:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
exa:Audit_Number       LIKE(exa:Audit_Number)         !List box control field - type derived from field
exa:Stock_Unit_Number  LIKE(exa:Stock_Unit_Number)    !List box control field - type derived from field
xch:ESN                LIKE(xch:ESN)                  !List box control field - type derived from field
xch:Model_Number       LIKE(xch:Model_Number)         !List box control field - type derived from field
exa:Replacement_Unit_Number LIKE(exa:Replacement_Unit_Number) !List box control field - type derived from field
xch_ali:ESN            LIKE(xch_ali:ESN)              !List box control field - type derived from field
xch_ali:Model_Number   LIKE(xch_ali:Model_Number)     !List box control field - type derived from field
exa:Record_Number      LIKE(exa:Record_Number)        !Primary key field - type derived from field
exa:Stock_Type         LIKE(exa:Stock_Type)           !Browse key field - type derived from field
xch_ali:Ref_Number     LIKE(xch_ali:Ref_Number)       !Related join file key field - type derived from field
xch:Ref_Number         LIKE(xch:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB12::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
QuickWindow          WINDOW('Browse The Exchange Units File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(68,110,448,250),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('52R(2)|M~Audit Number~L@s8b@53R(2)|M~Stock Unit No~L@s8b@69L(2)|M~I.M.E.I. Numbe' &|
   'r~@s16@84L(2)|M~Model Number~@s30@75R(2)|M~Replacement Unit No~L@s8b@69L(2)|M~I.' &|
   'M.E.I. Number~@s16@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Exchange Unit File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(548,160),USE(?Make_Available),TRN,FLAT,LEFT,ICON('uniavp.jpg')
                       BUTTON,AT(548,304),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,332),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       PANEL,AT(64,54,552,24),USE(?Panel1),FILL(09A6A7CH)
                       COMBO(@s30),AT(140,62,124,10),USE(Stock_Type_Temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       PROMPT('Stock Type'),AT(68,62),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(64,80,552,284),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Audit Number'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(68,96,64,10),USE(exa:Stock_Unit_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020265'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Exchange_By_Audit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:EXCAUDIT.Open
  Relate:JOBS.Open
  Relate:USERS_ALIAS.Open
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCAUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Exchange_By_Audit')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,exa:Audit_Number_Key)
  BRW1.AddRange(exa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,exa:Audit_Number,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  BRW1.AddField(exa:Audit_Number,BRW1.Q.exa:Audit_Number)
  BRW1.AddField(exa:Stock_Unit_Number,BRW1.Q.exa:Stock_Unit_Number)
  BRW1.AddField(xch:ESN,BRW1.Q.xch:ESN)
  BRW1.AddField(xch:Model_Number,BRW1.Q.xch:Model_Number)
  BRW1.AddField(exa:Replacement_Unit_Number,BRW1.Q.exa:Replacement_Unit_Number)
  BRW1.AddField(xch_ali:ESN,BRW1.Q.xch_ali:ESN)
  BRW1.AddField(xch_ali:Model_Number,BRW1.Q.xch_ali:Model_Number)
  BRW1.AddField(exa:Record_Number,BRW1.Q.exa:Record_Number)
  BRW1.AddField(exa:Stock_Type,BRW1.Q.exa:Stock_Type)
  BRW1.AddField(xch_ali:Ref_Number,BRW1.Q.xch_ali:Ref_Number)
  BRW1.AddField(xch:Ref_Number,BRW1.Q.xch:Ref_Number)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB12.Init(Stock_Type_Temp,?Stock_Type_Temp,Queue:FileDropCombo.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo
  FDCB12.AddSortOrder(stp:Use_Loan_Key)
  FDCB12.AddField(stp:Stock_Type,FDCB12.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:EXCAUDIT.Close
    Relate:JOBS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Exchange_By_Audit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            if securitycheck('AUDIT NUMBERS - INSERT')
                Case Missive('You do not have access to his option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                access:excaudit.cancelautoinc()
                do_update# = false
            end!if securitycheck('AUDIT NUMBERS - INSERT',x")
        of changerecord
            if securitycheck('AUDIT NUMBERS - CHANGE')
                Case Missive('You do not have access to his option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!if securitycheck('AUDIT NUMBERS - CHANGE',x")
        of deleterecord
            if securitycheck('AUDIT NUMBERS - DELETE')
                Case Missive('You do not have access to his option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            Else
              If exa:stock_unit_number <> '' Or exa:replacement_unit_number <> ''
                  Case Missive('There are Exchange Units attached to this audit number.'&|
                    '<13,10>'&|
                    '<13,10>You must make this Audit Number available before you can delete it.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  do_update# = False
              End!If exc:stock_unit_number <> '' Or exc:replacement_unit_number <> ''
            end!if securitycheck('AUDIT NUMBERS - DELETE',x")
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Exchange_By_Audit
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020265'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020265'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020265'&'0')
      ***
    OF ?Make_Available
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Make_Available, Accepted)
      If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE')
          Case Missive('You do not have access to his option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE)
          Case Missive('Warning! Any Exchange Units attached to this audit number will have it removed.'&|
            '<13,10>'&|
            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  thiswindow.reset(1)
                  access:excaudit.clearkey(exa:audit_number_key)
                  exa:stock_type   = Stock_Type_Temp
                  exa:audit_number = brw1.q.exa:audit_number
                  if access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                      If exa:Stock_Unit_Number <> ''
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = exa:stock_unit_number
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                              xch:audit_number = ''
                              access:exchange.update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = level:benign
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'MANUALLY REMOVED FROM AUDIT NO: ' & Clip(exa:stock_unit_number)
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = level:benign
                          End!if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                      End!If EXA:Stock_Unit_Number <> ''
                      If exa:Replacement_Unit_Number <> ''
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = exa:replacement_unit_number
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                              xch:audit_number = ''
                              access:exchange.update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = level:benign
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'MANUALLY REMOVED FROM AUDIT NO: ' & Clip(exa:replacement_unit_number)
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = level:benign
                          End!if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                      End!If exa:Replacement_Unit_Number <> ''
                      exa:stock_unit_number = ''
                      exa:replacement_unit_number = ''
                      access:excaudit.update()
                  End!if access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
              Of 1 ! No Button
          End ! Case Missive
      End!If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Make_Available, Accepted)
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?exa:Stock_Unit_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?exa:Stock_Unit_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?exa:Stock_Unit_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  CASE (xch:Available)
  OF 'AVL'
    status_temp = 'AVAILABLE'
  OF 'LOA'
    status_temp = 'EXCHANGED - JOB NO: ' & FORMAT(xch:Job_Number,@p<<<<<<<#p)
  ELSE
    status_temp = 'IN REPAIR - JOB NO: ' & FORMAT(xch:Job_Number,@p<<<<<<<#p)
  END
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

UpdateTRADEACC PROCEDURE                              !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::49:TAGFLAG         BYTE(0)
DASBRW::49:TAGMOUSE        BYTE(0)
DASBRW::49:TAGDISPSTATUS   BYTE(0)
DASBRW::49:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::52:TAGFLAG         BYTE(0)
DASBRW::52:TAGMOUSE        BYTE(0)
DASBRW::52:TAGDISPSTATUS   BYTE(0)
DASBRW::52:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
WinName              STRING(255),STATIC
WinPrint             CLASS(CWin32File)
                     END
CurrentTab           STRING(80)
save_subtracc_id     USHORT,AUTO
save_tra_ali_id      USHORT,AUTO
pos                  STRING(255)
ok_pressed_temp      BYTE(0)
Account_Number_temp  STRING(15)
Company_Name_temp    STRING(30)
use_sub_account_temp STRING(3)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
discount_labour_temp REAL
discount_parts_temp  REAL
vat_labour_temp      REAL
vat_parts_temp       REAL
Discount_Retail_Temp REAL
vat_retail_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:True             BYTE(1)
tmp:Yes              STRING('YES')
tmp:TradeState       LONG
tmp:SaveState        LONG
tmp:SaveState2       LONG
SaveDataGroup        GROUP,PRE(sav)
HeadRecordNumber     LONG
RecordNumber         LONG
MainAccountNumber    STRING(30)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Branch               STRING(30)
Postcode             STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
ContactName          STRING(30)
OutgoingCourier      STRING(30)
WebPassword1         STRING(30)
WebPassword2         STRING(30)
SiteLocation         STRING(30)
BranchIdentification STRING(2)
RemoteRepairCentre   BYTE(0)
StoresAccount        STRING(30)
                     END
AccountQueue         QUEUE,PRE(accque)
RecordNumber         LONG
                     END
tmp:DayOff           STRING(1)
GenericAccountFlag   BYTE(1)
GenericTag           STRING(1)
BranchTag            STRING(1)
CountedRRC           LONG
LicensedRRC          LONG
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tra:Labour_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?tra:Parts_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?tra:Loan_Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?tra:Exchange_Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?tra:Turnaround_Time
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:10 QUEUE                          !Queue declaration for browse/combo box using ?tra:Retail_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?tra:ChargeType
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?tra:WarChargeType
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tra:DespatchedJobStatus
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tra:InvoicedJobStatus
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Branch)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Branch             LIKE(sub:Branch)               !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(TRABUSHR)
                       PROJECT(tbh:TheDate)
                       PROJECT(tbh:StartTime)
                       PROJECT(tbh:EndTime)
                       PROJECT(tbh:RecordNumber)
                       PROJECT(tbh:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tbh:TheDate            LIKE(tbh:TheDate)              !List box control field - type derived from field
tbh:StartTime          LIKE(tbh:StartTime)            !List box control field - type derived from field
tbh:EndTime            LIKE(tbh:EndTime)              !List box control field - type derived from field
tmp:DayOff             LIKE(tmp:DayOff)               !List box control field - type derived from local data
tbh:RecordNumber       LIKE(tbh:RecordNumber)         !Primary key field - type derived from field
tbh:RefNumber          LIKE(tbh:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW34::View:Browse   VIEW(TRAHUBS)
                       PROJECT(trh:Hub)
                       PROJECT(trh:RecordNumber)
                       PROJECT(trh:TRADEACCAccountNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
trh:Hub                LIKE(trh:Hub)                  !List box control field - type derived from field
trh:RecordNumber       LIKE(trh:RecordNumber)         !Primary key field - type derived from field
trh:TRADEACCAccountNumber LIKE(trh:TRADEACCAccountNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW45::View:Browse   VIEW(ASVACC)
                       PROJECT(ASV:RecordNo)
                       PROJECT(ASV:TradeAccNo)
                       PROJECT(ASV:Manufacturer)
                       PROJECT(ASV:ASVCode)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
ASV:RecordNo           LIKE(ASV:RecordNo)             !List box control field - type derived from field
ASV:TradeAccNo         LIKE(ASV:TradeAccNo)           !List box control field - type derived from field
ASV:Manufacturer       LIKE(ASV:Manufacturer)         !List box control field - type derived from field
ASV:ASVCode            LIKE(ASV:ASVCode)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW47::View:Browse   VIEW(SUBTRACC_ALIAS)
                       PROJECT(sub_ali:RecordNumber)
                       PROJECT(sub_ali:Main_Account_Number)
                       PROJECT(sub_ali:Account_Number)
                       PROJECT(sub_ali:Company_Name)
                       PROJECT(sub_ali:Generic_Account)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
GenericTag             LIKE(GenericTag)               !List box control field - type derived from local data
GenericTag_Icon        LONG                           !Entry's icon ID
sub_ali:RecordNumber   LIKE(sub_ali:RecordNumber)     !List box control field - type derived from field
sub_ali:Main_Account_Number LIKE(sub_ali:Main_Account_Number) !List box control field - type derived from field
sub_ali:Account_Number LIKE(sub_ali:Account_Number)   !List box control field - type derived from field
sub_ali:Company_Name   LIKE(sub_ali:Company_Name)     !List box control field - type derived from field
sub_ali:Generic_Account LIKE(sub_ali:Generic_Account) !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW48::View:Browse   VIEW(TRAHUBAC)
                       PROJECT(TRA1:RecordNo)
                       PROJECT(TRA1:HeadAcc)
                       PROJECT(TRA1:SubAcc)
                       PROJECT(TRA1:SubAccName)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
BranchTag              LIKE(BranchTag)                !List box control field - type derived from local data
BranchTag_Icon         LONG                           !Entry's icon ID
TRA1:RecordNo          LIKE(TRA1:RecordNo)            !List box control field - type derived from field
TRA1:HeadAcc           LIKE(TRA1:HeadAcc)             !List box control field - type derived from field
TRA1:SubAcc            LIKE(TRA1:SubAcc)              !List box control field - type derived from field
TRA1:SubAccName        LIKE(TRA1:SubAccName)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB15::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB24::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB32::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB33::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB36::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB5::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB16::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB30::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB31::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
History::tra:Record  LIKE(tra:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update Trade Account'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Insert / Amend Trade Account'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(4,28,304,366),USE(?Sheet3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Main Account Details'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Account Number'),AT(8,50),USE(?TRA:Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(104,50,64,10),USE(tra:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(172,44),USE(?ReplicateFrom),TRN,FLAT,HIDE,LEFT,ICON('repfromp.jpg')
                           PROMPT('Company Name'),AT(8,70),USE(?TRA:Company_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,70,136,10),USE(tra:Company_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s2),AT(104,84,64,10),USE(tra:BranchIdentification),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Branch ID No'),AT(8,84),USE(?Prompt45),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Postcode'),AT(8,142),USE(?TRA:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(104,142,64,10),USE(tra:Postcode),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Hub'),AT(176,142),USE(?tra:Hub:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(228,142,64,10),USE(tra:Hub),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),UPR,READONLY
                           PROMPT('Address'),AT(8,98),USE(?TRA:Address_Line1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,98,136,10),USE(tra:Address_Line1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(104,110,136,10),USE(tra:Address_Line2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Suburb'),AT(8,126),USE(?TRA:Address_Line1:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,126,124,10),USE(tra:Address_Line3),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(232,122),USE(?LookupSuburb),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Telephone Number'),AT(8,158),USE(?TRA:Telephone_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(104,158,64,10),USE(tra:Telephone_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           STRING('Fax Number'),AT(176,158),USE(?fax_number),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(228,158,64,10),USE(tra:Fax_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Email Address'),AT(8,188),USE(?tra:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s255),AT(104,188,136,10),USE(tra:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                           PROMPT('V.A.T. Number'),AT(8,234),USE(?tra:VAT_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,234,136,10),USE(tra:VAT_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name'),AT(8,172),USE(?TRA:Contact_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,172,136,10),USE(tra:Contact_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier - Incoming'),AT(8,248),USE(?tra:Courier_Incoming:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,248,124,10),USE(tra:Courier_Incoming),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(232,244),USE(?LookupIncomingCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Courier - Outgoing'),AT(8,264),USE(?tra:Courier_Outgoing:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(104,264,124,10),USE(tra:Courier_Outgoing),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(232,260),USE(?LookupOutgoingCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Default Courier Cost'),AT(8,278),USE(?TRA:Courier_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(104,278,64,10),USE(tra:Courier_Cost),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Loan Stock Type'),AT(8,292),USE(?tra:loan_stock_type:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(104,292,136,10),USE(tra:Loan_Stock_Type),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:6)
                           PROMPT('Exchange Stock Type'),AT(8,306),USE(?tra:exchange_stock_type:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(104,306,136,10),USE(tra:Exchange_Stock_Type),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:7)
                           PROMPT('Turnaround Time'),AT(8,322),USE(?turnaround_time),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(104,322,136,10),USE(tra:Turnaround_Time),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:8)
                           CHECK('Stop Account'),AT(104,334),USE(tra:Stop_Account),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Allow Cash Sales'),AT(180,334),USE(tra:Allow_Cash_Sales),DISABLE,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           PROMPT('Site Location'),AT(104,344),USE(?tra:SiteLocation:Prompt),TRN,LEFT,FONT('Tahoma',7,COLOR:White,,CHARSET:ANSI)
                           CHECK('Remote Repr Cntr'),AT(8,352),USE(tra:RemoteRepairCentre),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Remote Repair Centre'),TIP('Remote Repair Centre'),VALUE('1','0')
                           ENTRY(@s30),AT(104,352,124,10),USE(tra:SiteLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Site Location'),TIP('Site Location'),UPR
                           BUTTON,AT(232,348),USE(?LookupSiteLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Return Exchange Booking Account'),AT(8,370,88,18),USE(?tra:RtnExchangeAccount:Prompt),FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(104,370,124,10),USE(tra:RtnExchangeAccount),FONT(,8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Returned Exchange Account'),TIP('Returned Exchange Account'),UPR
                           BUTTON,AT(232,366),USE(?buttonReturnExchange),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Registered CO Details'),USE(?Tab22),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Company Trading Name'),AT(12,48),USE(?tra:coTradingName:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,48,124,10),USE(tra:coTradingName),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Trading Name 2'),AT(12,68),USE(?tra:coTradingName2:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,68,124,10),USE(tra:coTradingName2),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Location'),AT(12,88),USE(?tra:coLocation:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,88,124,10),USE(tra:coLocation),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Registration No'),AT(12,108),USE(?tra:coRegistrationNo:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(132,108,124,10),USE(tra:coRegistrationNo),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company VAT Number'),AT(12,128),USE(?tra:coVATNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(132,128,124,10),USE(tra:coVATNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Address Line 1'),AT(12,148),USE(?tra:coAddressLine1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,148,124,10),USE(tra:coAddressLine1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Address Line 2'),AT(12,168),USE(?tra:coAddressLine2:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,168,124,10),USE(tra:coAddressLine2),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Address Line 3'),AT(12,188),USE(?tra:coAddressLine3:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,188,124,10),USE(tra:coAddressLine3),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Address Line 4'),AT(12,208),USE(?tra:coAddressLine4:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,208,124,10),USE(tra:coAddressLine4),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Telephone Number'),AT(12,228),USE(?tra:coTelephoneNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(132,228,124,10),USE(tra:coTelephoneNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Fax Number'),AT(12,248),USE(?tra:coFaxNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(132,248,124,10),USE(tra:coFaxNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Company Email Address'),AT(12,268),USE(?tra:coEmailAddress:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(132,268,124,10),USE(tra:coEmailAddress),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Company SMS name'),AT(12,288),USE(?tra:coSMSname:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(132,288,124,10),USE(TRA2:coSMSname),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Email Details'),USE(?Tab23),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('SMS Received Email 2'),AT(20,80),USE(?Prompt71),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s100),AT(112,80,176,10),USE(TRA2:SMS_Email2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('SMS Received Email 3'),AT(20,98),USE(?Prompt72),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s100),AT(112,98,176,10),USE(TRA2:SMS_Email3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('SMS Report Emails'),AT(20,159),USE(?Prompt73),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(112,159,176,43),USE(TRA2:SMS_ReportEmail),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('(There can be up to five emails, start each on a new lilne)'),AT(73,206),USE(?String12),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(312,28,360,16),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Replicatedfrom'),AT(428,30,240,12),USE(?ReplicatedFrom),HIDE,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(312,46,364,348),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('General'),USE(?Tab8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use Invoice As Delivery Address'),AT(320,62,136,10),USE(tra:Use_Delivery_Address),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Use Spares Request Process'),AT(512,62),USE(TRA2:UseSparesRequest),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO'),MSG('YES, NO or blank')
                           CHECK('Use Invoice As Collection Address'),AT(320,75),USE(tra:Use_Collection_Address),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Record End User Address'),AT(320,86),USE(tra:Use_Customer_Address),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Do not use the Trade Account''s Address<13,10>as the Job''s ''Customer Address'''),VALUE('YES','NO')
                           CHECK('Invoice End User Address'),AT(320,99),USE(tra:Invoice_Customer_Address),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Do not use the Trade Account''s Address as the<13,10>Invoice Address. Use the Job''s "C' &|
   'ustomer Address"'),VALUE('YES','NO')
                           CHECK('Auto Send Status Emails'),AT(320,110),USE(tra:AutoSendStatusEmails),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Auto Send Status Emails'),TIP('Auto Send Status Emails'),VALUE('1','0')
                           CHECK('Do MSISDN Check'),AT(320,122),USE(tra:DoMSISDNCheck),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Do MSISDN Check'),TIP('Do MSISDN Check'),VALUE('1','0')
                           BUTTON,AT(340,321),USE(?ReplicateSubAccount),TRN,FLAT,HIDE,LEFT,ICON('repaccp.jpg')
                           CHECK('Activate 48 Hour Exchange Process'),AT(320,139),USE(tra:Activate48Hour),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Activate 48 Hour Exchange Process'),TIP('Activate 48 Hour Exchange Process'),VALUE('1','0')
                           CHECK('Ignore Replenishment Process'),AT(512,139),USE(tra:IgnoreReplenishmentProcess),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Replenishment Process'),TIP('Ignore Replenishment Process'),VALUE('1','0')
                           BUTTON,AT(444,321),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(512,321),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(580,321),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           PROMPT('Sub Accounts'),AT(564,198),USE(?Prompt28),TRN,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(336,178,312,172),USE(?CurrentTab),SPREAD
                             TAB('By Account Number'),USE(?Tab3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s15),AT(340,200,64,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             END
                             TAB('By Branch'),USE(?Tab5),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(340,197,100,10),USE(sub:Branch),FONT('Tahoma',8,,FONT:bold),UPR
                             END
                             TAB('By Company Name'),USE(?Tab4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(340,197,136,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             END
                           END
                           PANEL,AT(336,158,312,16),USE(?Panel2),FILL(09A6A7CH)
                           CHECK('Use Sub Accounts'),AT(340,162),USE(tra:Use_Sub_Accounts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Invoice Sub Accounts'),AT(548,162),USE(tra:Invoice_Sub_Accounts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           LIST,AT(340,214,304,102),USE(?List),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('65L(2)|M~Account Number~@s15@100L(2)|M~Branch~@s30@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Service'),USE(?Tab9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(316,62,352,320),USE(?Sheet5),LEFT,SPREAD,DOWN
                             TAB('General'),USE(?Tab12),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               CHECK('Exchange Unit Account'),AT(336,67),USE(tra:ExchangeAcc),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Account used for repairing exchange units'),TIP('Account used for repairing exchange units'),VALUE('YES','NO')
                               CHECK('Second Year Warranty Account'),AT(504,67),USE(tra:SecondYearAccount),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Second Year Warranty Account (True/False)'),TIP('Second Year Warranty Account (True/False)'),VALUE('1','0')
                               CHECK('Exclude From Bouncer Table'),AT(336,78),USE(tra:ExcludeBouncer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Exclude From Bouncer'),TIP('Exclude From Bouncer'),VALUE('1','0')
                               CHECK('Show Repair Types With Trade Price Structure'),AT(336,91),USE(tra:ShowRepairTypes),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Repair Type Display'),TIP('Repair Type Display'),VALUE('1','0')
                               GROUP('Default Charge Types'),AT(336,102,200,40),USE(?Charge_Type_Group),DISABLE,BOXED,TRN
                                 COMBO(@s30),AT(408,115,124,10),USE(tra:ChargeType),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:11)
                                 PROMPT('Chargeable'),AT(340,115),USE(?Prompt26),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                                 PROMPT('Warranty'),AT(340,126),USE(?Prompt26:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                                 COMBO(@s30),AT(408,126,124,10),USE(tra:WarChargeType),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:12)
                               END
                               CHECK('Stop Units From Being Sent To Third Party'),AT(336,163),USE(tra:StopThirdParty),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Stop Unit For Going To Third Party'),TIP('Stop Unit For Going To Third Party'),VALUE('1','0')
                               GROUP('Allow Address Manipulation'),AT(336,180,116,39),USE(?Group6),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                                 CHECK('Collection Address'),AT(344,191),USE(tra:ChangeCollAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Collection Address'),TIP('Collection Address'),VALUE('1','0')
                                 CHECK('Delivery Address'),AT(344,201),USE(tra:ChangeDelAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Delivery Address'),TIP('Delivery Address'),VALUE('1','0')
                               END
                               CHECK('Allow Loan'),AT(336,235),USE(tra:Allow_Loan),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                               CHECK('Allow Exchange'),AT(336,246),USE(tra:Allow_Exchange),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                               PROMPT('Estimate If Over'),AT(412,254),USE(?TRA:Estimate_If_Over:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold)
                               CHECK('Force Estimate'),AT(336,262),USE(tra:Force_Estimate),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                               ENTRY(@n14.2),AT(412,262,64,10),USE(tra:Estimate_If_Over),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               CHECK('Force Common Fault'),AT(336,278),USE(tra:ForceCommonFault),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Common Fault'),TIP('Force Common Fault'),VALUE('1','0')
                               CHECK('Force Order Number'),AT(336,291),USE(tra:ForceOrderNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Order Number'),TIP('Force Order Number'),VALUE('1','0')
                               CHECK('Allow End User Name'),AT(336,302),USE(tra:Use_Contact_Name),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                               CHECK('Repair Engineer To QA Job'),AT(440,302),USE(tra:RepairEngineerQA),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Re'),TIP('Re'),VALUE('1','0')
                               CHECK('Allocate QA Engineer at Completion'),AT(440,315),USE(tra:AllocateQAEng),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                               CHECK('Override Mandatory Mobile Number'),AT(336,328),USE(tra:OverrideMobileDefault),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Override Mandatory Mobile Number'),TIP('Override Mandatory Mobile Number'),VALUE('1','0')
                               PROMPT('Pre Book Region:'),AT(338,353),USE(?TRA2:Pre_Book_Region:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(424,353,104,11),USE(TRA2:Pre_Book_Region),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Used to allow use of prebooking'),TIP('Used to allow use of prebooking')
                               BUTTON,AT(536,348),USE(?CallLookup:2),FLAT,ICON('lookupp.jpg')
                             END
                             TAB('Financial'),USE(?Tab13),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               CHECK('Zero Value Chargeable Parts'),AT(336,67),USE(tra:ZeroChargeable),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Don''t show Chargeable Costs'),TIP('Don''t show Chargeable Costs'),VALUE('YES','NO')
                               CHECK('Refurbishment Charge'),AT(336,78),USE(tra:RefurbCharge),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                               OPTION('Invoice Print Type'),AT(464,158,112,20),USE(tra:InvoiceType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type')
                                 RADIO('Manual'),AT(472,166),USE(?Option4:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                                 RADIO('Automatic'),AT(520,166),USE(?Option4:Radio1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                               END
                               OPTION('Invoice Print Type'),AT(464,179,112,20),USE(tra:InvoiceTypeComplete),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type')
                                 RADIO('Manual'),AT(472,187),USE(?Option4:Radio1:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                                 RADIO('Automatic'),AT(520,187),USE(?Option4:Radio1:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                               END
                               OPTION('Invoice Type'),AT(336,91,144,64),USE(tra:MultiInvoice),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)')
                                 RADIO('Single Invoice (No Summary)'),AT(344,102),USE(?TRA:MultiIn:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Will produce one invoice per job.'),VALUE('SIN')
                                 RADIO('Single Invoice (Summary)'),AT(344,115),USE(?TRA:MultiIn:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Will produce one invoice per job. <13,10>During Batch Invoicing a summary <13,10>of all in' &|
   'voices created will be produced.'),VALUE('SIM')
                                 RADIO('Multiple Invoice'),AT(344,126),USE(?TRA:MultiIn:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Many jobs will appear on the same invoice.'),VALUE('MUL')
                                 RADIO('Batch Invoice'),AT(344,139),USE(?TRA:MultiIn:Radio4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('All jobs in a batch will appear on the same invoice.'),VALUE('BAT')
                               END
                               CHECK('Print Invoice At Despatch'),AT(336,166),USE(tra:InvoiceAtDespatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Invoice At Despatch'),TIP('Print Invoice At Despatch'),VALUE('1','0')
                               CHECK('Print Invoice At Completion'),AT(336,187),USE(tra:InvoiceAtCompletion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Invoice At Completion'),TIP('Invoice At Completion'),VALUE('1','0')
                               GROUP('V.A.T. Codes'),AT(336,203,92,52),USE(?Group11),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                                 PROMPT('Labour'),AT(340,214),USE(?labour:2),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                                 COMBO(@s2),AT(380,214,40,10),USE(tra:Labour_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,FORMAT('14L(2)|M@s2@24L(2)@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:1)
                                 PROMPT('Parts'),AT(340,227),USE(?parts:2),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                                 COMBO(@s2),AT(380,227,40,10),USE(tra:Parts_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,FORMAT('13L(2)|M@s2@24L(2)@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:5)
                                 PROMPT('Retail'),AT(340,240),USE(?retail:2),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                                 COMBO(@s2),AT(380,240,40,10),USE(tra:Retail_VAT_Code),IMM,VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ,FORMAT('15L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:10)
                               END
                               CHECK('Company Owned Franchise'),AT(336,263),USE(tra:CompanyOwned),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Company Owned Franchise'),TIP('Company Owned Franchise'),VALUE('1','0')
                               CHECK('Allow Credit Notes'),AT(336,278),USE(tra:AllowCreditNotes),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Allow Credit Notes'),TIP('Allow Credit Notes'),VALUE('1','0')
                             END
                           END
                         END
                         TAB('Retail'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Payment Method'),AT(316,62,124,28),USE(tra:Retail_Payment_Type),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Account'),AT(320,74),USE(?TRA:Retail_Payment_Type:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ACC')
                             RADIO('Cash'),AT(384,74),USE(?TRA:Retail_Payment_Type:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('CAS')
                           END
                           OPTION('Pricing Structure'),AT(316,92,124,28),USE(tra:Retail_Price_Structure),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Trade'),AT(320,104),USE(?TRA:Retail_Price_Structure:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('TRA')
                             RADIO('Purchase'),AT(384,104),USE(?tra:Retail_Price_Structure:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('PUR')
                           END
                           CHECK('Print Retail Picking Note'),AT(316,126),USE(tra:Print_Retail_Picking_Note),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Price Retail Despatch Notes'),AT(316,142),USE(tra:Price_Retail_Despatch),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Zero Value Parts'),AT(316,158),USE(tra:RetailZeroParts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Zero Value Parts'),TIP('Zero Value Parts'),VALUE('1','0')
                           CHECK('Charge In Euros'),AT(316,174),USE(tra:EuroApplies),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Apply Euro')
                           PROMPT('Stores Account'),AT(316,194),USE(?tra:StoresAccount:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(420,194,124,10),USE(tra:StoresAccount),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Stores Account'),TIP('Stores Account'),UPR
                           BUTTON,AT(548,190),USE(?LookupStoresAccount),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Line 500 Account Number'),AT(316,212),USE(?tra:Line500AccountNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(420,212,124,10),USE(tra:Line500AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Line 500 Account Number'),TIP('Line 500 Account Number'),UPR
                         END
                         TAB('Web'),USE(?Tab10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('General Password'),AT(316,62),USE(?TRA:Password:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(392,62,124,10),USE(tra:Password),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,PASSWORD
                           PROMPT('Order Password'),AT(316,78),USE(?tra:WebPassword1:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(392,78,124,10),USE(tra:WebPassword1),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Web Password1'),TIP('Web Password1'),UPR,PASSWORD
                           PROMPT('Web Notes'),AT(316,94),USE(?TRA:Password:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(392,94,208,56),USE(tra:WebMemo),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Web Memo')
                           GROUP('Stock Access Levels'),AT(392,155,124,32),USE(?StockAccessLevels),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('E1'),AT(400,166),USE(tra:E1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('E1'),TIP('E1'),VALUE('1','0')
                             CHECK('E2'),AT(444,166),USE(tra:E2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('E2'),TIP('E2'),VALUE('1','0')
                             CHECK('E3'),AT(488,166),USE(tra:E3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('E3'),TIP('E3'),VALUE('1','0')
                           END
                           GROUP('SB Online Defaults'),AT(320,196,344,194),USE(?groupSBOnline),BOXED,TRN
                             STRING('Off'),AT(320,208,16,22),USE(?String15),TRN,ANGLE(900)
                             STRING('On'),AT(332,208,16,22),USE(?String16),TRN,ANGLE(900)
                             STRING('Off'),AT(492,210,16,22),USE(?String18),TRN,ANGLE(900)
                             STRING('On'),AT(504,210,16,22),USE(?String19),TRN,ANGLE(900)
                             STRING('Force'),AT(520,204,16,22),USE(?String20),TRN,ANGLE(900)
                             STRING('Force'),AT(348,204,16,22),USE(?String17),TRN,LEFT(2),ANGLE(900)
                             STRING('New Job Booking'),AT(380,236),USE(?String13)
                             STRING('Trade Accounts'),AT(548,236),USE(?String14)
                             OPTION,AT(324,248,58,18),USE(tra:SBOnlineJobProgress),MSG('Job Progress')
                               RADIO,AT(328,253),USE(?tra:SBOnlineJobProgress:Radio7),VALUE('0')
                               RADIO,AT(344,253),USE(?tra:SBOnlineJobProgress:Radio8),VALUE('1')
                               RADIO,AT(360,253),USE(?tra:SBOnlineJobProgress:Radio9),VALUE('2')
                             END
                             STRING('Engineer Update'),AT(548,253),USE(?String22)
                             OPTION,AT(324,264,58,18),USE(TRA2:SBOnlineDespatch),TRN
                               RADIO,AT(328,270),USE(?TRA2:SBOnlineDespatch:Radio10),VALUE('0')
                               RADIO,AT(344,270),USE(?TRA2:SBOnlineDespatch:Radio11),VALUE('1')
                               RADIO,AT(360,270),USE(?TRA2:SBOnlineDespatch:Radio12),VALUE('2')
                             END
                             OPTION,AT(492,266,58,18),USE(TRA2:SBOnlineAudits),TRN
                               RADIO,AT(498,270),USE(?TRA2:SBOnlineAudits:Radio10),VALUE('0')
                               RADIO,AT(514,270),USE(?TRA2:SBOnlineAudits:Radio11),VALUE('1')
                               RADIO,AT(530,270),USE(?TRA2:SBOnlineAudits:Radio12),VALUE('2')
                             END
                             STRING('Despatch'),AT(380,270),USE(?String23)
                             STRING('Audits'),AT(548,270),USE(?String24)
                             OPTION,AT(324,282,58,18),USE(TRA2:SBOnlineStock),TRN
                               RADIO,AT(328,287),USE(?TRA2:SBOnlineStock:Radio10),VALUE('0')
                               RADIO,AT(344,287),USE(?TRA2:SBOnlineStock:Radio11),VALUE('1')
                               RADIO,AT(360,287),USE(?TRA2:SBOnlineStock:Radio12),VALUE('2')
                             END
                             STRING('Stock'),AT(380,287),USE(?String25)
                             OPTION,AT(492,282,58,18),USE(TRA2:SBOnlineUsers),TRN
                               RADIO,AT(498,287),USE(?TRA2:SBOnlineUsers:Radio10),VALUE('0')
                               RADIO,AT(514,287),USE(?TRA2:SBOnlineUsers:Radio11),VALUE('1')
                               RADIO,AT(530,287),USE(?TRA2:SBOnlineUsers:Radio12),VALUE('2')
                             END
                             STRING('Users'),AT(548,287),USE(?String26)
                             OPTION,AT(324,300,58,18),USE(TRA2:SBOnlineQAEtc),TRN
                               RADIO,AT(328,304),USE(?TRA2:SBOnlineQAEtc:Radio10),VALUE('0')
                               RADIO,AT(344,304),USE(?TRA2:SBOnlineQAEtc:Radio11),VALUE('1')
                               RADIO,AT(360,304),USE(?TRA2:SBOnlineQAEtc:Radio12),VALUE('2')
                             END
                             STRING('QA, Allocations and Handover'),AT(380,304),USE(?String27),TRN
                             OPTION,AT(492,300,58,18),USE(TRA2:SBOnlineLoansExch),TRN
                               RADIO,AT(498,304),USE(?TRA2:SBOnlineLoansExch:Radio1),VALUE('0')
                               RADIO,AT(514,304),USE(?TRA2:SBOnlineLoansExch:Radio2),VALUE('1')
                               RADIO,AT(530,304),USE(?TRA2:SBOnlineLoansExch:Radio3),VALUE('2')
                             END
                             STRING('Loans and Exchanges'),AT(548,304),USE(?String28)
                             OPTION,AT(324,318,58,18),USE(TRA2:SBOnlineWarrClaims),TRN
                               RADIO,AT(328,321),USE(?TRA2:SBOnlineWarrClaims:Radio10),VALUE('0')
                               RADIO,AT(344,321),USE(?TRA2:SBOnlineWarrClaims:Radio11),VALUE('1')
                               RADIO,AT(360,321),USE(?TRA2:SBOnlineWarrClaims:Radio12),VALUE('2')
                             END
                             STRING('Warranty Claims'),AT(380,321),USE(?String29)
                             OPTION,AT(492,318,58,18),USE(TRA2:SBOnlineBouncers),TRN
                               RADIO,AT(498,321),USE(?TRA2:SBOnlineBouncers:Radio10),VALUE('0')
                               RADIO,AT(514,321),USE(?TRA2:SBOnlineBouncers:Radio11),VALUE('1')
                               RADIO,AT(530,321),USE(?TRA2:SBOnlineBouncers:Radio12),VALUE('2')
                             END
                             STRING('Bouncers'),AT(548,321),USE(?String30),TRN
                             OPTION,AT(324,336,58,18),USE(TRA2:SBOnlineWaybills),TRN
                               RADIO,AT(328,338),USE(?TRA2:SBOnlineWaybills:Radio10),VALUE('0')
                               RADIO,AT(344,338),USE(?TRA2:SBOnlineWaybills:Radio11),VALUE('1')
                               RADIO,AT(360,338),USE(?TRA2:SBOnlineWaybills:Radio12),VALUE('2')
                             END
                             STRING('Waybill Procedure'),AT(380,338),USE(?String31)
                             OPTION,AT(492,336,58,18),USE(TRA2:SBOnlineReports),TRN
                               RADIO,AT(498,338),USE(?TRA2:SBOnlineReports:Radio10),VALUE('0')
                               RADIO,AT(514,338),USE(?TRA2:SBOnlineReports:Radio11),VALUE('1')
                               RADIO,AT(530,338),USE(?TRA2:SBOnlineReports:Radio12),VALUE('2')
                             END
                             STRING('Reports'),AT(548,338),USE(?String32)
                             STRING('Job Progress'),AT(380,253),USE(?String21)
                             OPTION,AT(492,248,58,18),USE(TRA2:SBOnlineEngineer)
                               RADIO,AT(498,253),USE(?TRA2:SBOnlineEngineer:Radio7),VALUE('0')
                               RADIO,AT(514,253),USE(?TRA2:SBOnlineEngineer:Radio8),VALUE('1')
                               RADIO,AT(530,253),USE(?TRA2:SBOnlineEngineer:Radio9),VALUE('2')
                             END
                             OPTION,AT(492,230,58,18),USE(TRA2:SBOnlineTradeAccs)
                               RADIO,AT(498,236),USE(?TRA2:SBOnlineTradeAccs:Radio4),VALUE('0')
                               RADIO,AT(514,236),USE(?TRA2:SBOnlineTradeAccs:Radio5),VALUE('1')
                               RADIO,AT(530,236),USE(?TRA2:SBOnlineTradeAccs:Radio6),VALUE('2')
                             END
                             OPTION,AT(324,230,58,18),USE(tra:UseSBOnline),MSG('Use SB Online')
                               RADIO,AT(328,236),USE(?tra:UseSBOnline:Radio1),VALUE('0')
                               RADIO,AT(344,236),USE(?tra:UseSBOnline:Radio2),VALUE('1')
                               RADIO,AT(360,236),USE(?tra:UseSBOnline:Radio3),VALUE('2')
                             END
                           END
                         END
                         TAB('Despatch'),USE(?Tab11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(316,62,352,286),USE(?Sheet6),LEFT,SPREAD,DOWN
                             TAB('General'),USE(?Tab18),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               CHECK('Print Service Despatch Notes'),AT(336,64),USE(tra:Print_Despatch_Notes),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                               CHECK('Skip Despatch'),AT(548,64),USE(tra:IgnoreDespatch),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Do not use Despatch Table')
                               CHECK('Print Despatch Note On Completion'),AT(348,75),USE(tra:Print_Despatch_Complete),DISABLE,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Print Despatch Note At Completion'),TIP('Print Despatch Note At Completion'),VALUE('YES','NO')
                               CHECK('Print Despatch Note On Despatch'),AT(348,83),USE(tra:Print_Despatch_Despatch),DISABLE,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Print Despatch Note At Completion'),TIP('Print Despatch Note At Completion'),VALUE('YES','NO')
                               CHECK('Hide Address On Despatch Note'),AT(348,91),USE(tra:HideDespAdd),DISABLE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide Address On Despatch Note'),TIP('Hide Address On Despatch Note'),VALUE('1','0')
                               GROUP('Despatch Note Type'),AT(336,99,196,49),USE(?Despatch_note_type),DISABLE,BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                                 CHECK('Despatch Note Per Item'),AT(344,110),USE(tra:Despatch_Note_Per_Item),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                                 CHECK('Despatch Note Summary'),AT(344,123),USE(tra:Summary_Despatch_Notes,,?TRA:Summary_Despatch_Notes:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                                 CHECK('Print Summary Note For Individual Despatch'),AT(344,134),USE(tra:IndividualSummary),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Individual Summary Report At Despatch'),TIP('Print Individual Summary Report At Despatch'),VALUE('1','0')
                               END
                               CHECK('Price Service Despatch Notes'),AT(336,150),USE(tra:Price_Despatch),DISABLE,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                               CHECK('First Copy Un-priced'),AT(464,150),USE(tra:Price_First_Copy_Only),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Display pricing details on first copy of despatch note')
                               PROMPT('Number Of Despatch Note Copies'),AT(336,168),USE(?tra:Num_Despatch_Note_Copies:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s8),AT(464,168,56,10),USE(tra:Num_Despatch_Note_Copies),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Number of despatch notes to print'),TIP('Number of despatch notes to print'),UPR
                               PROMPT('Completed Status'),AT(564,187),USE(?CompletedStatus),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                               CHECK('Despatch Paid Jobs Only'),AT(336,195),USE(tra:Despatch_Paid_Jobs),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                               CHECK('Set Completed Status'),AT(464,195),USE(tra:SetDespatchJobStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Completed Status'),TIP('Set Completed Status'),VALUE('1','0')
                               COMBO(@s30),AT(564,195,100,10),USE(tra:DespatchedJobStatus),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo),MSG('Completed Status')
                               PROMPT('Completed Status'),AT(564,206),USE(?CompletedStatus:2),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                               CHECK('Despatch Invoiced Jobs Only'),AT(336,214),USE(tra:Despatch_Invoiced_Jobs),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                               CHECK('Set Completed Status'),AT(464,214),USE(tra:SetInvoicedJobStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Completed Status'),TIP('Set Completed Status'),VALUE('1','0')
                               COMBO(@s30),AT(564,214,100,10),USE(tra:InvoicedJobStatus),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2),MSG('Completed Status')
                               CHECK('Batch Despatch'),AT(336,228),USE(tra:Skip_Despatch),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Use Batch Despatch')
                               CHECK('Use Alternative Contact Nos On Despatch Note'),AT(336,243),USE(tra:UseDespatchDetails),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Alternative Contact Nos On Despatch Note#'),TIP('Use Alternative Contact Nos On Despatch Note#'),VALUE('1','0')
                               GROUP('Alternative Contact Numbers'),AT(336,254,292,60),USE(?AlternativeContactNumbers),DISABLE,BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                                 PROMPT('Telephone Number'),AT(344,265),USE(?tra:AltTelephoneNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                                 ENTRY(@s30),AT(420,265,64,10),USE(tra:AltTelephoneNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                                 PROMPT('Fax Number'),AT(344,281),USE(?tra:AltFaxNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                                 ENTRY(@s30),AT(420,281,64,10),USE(tra:AltFaxNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fax Number'),TIP('Fax Number'),UPR
                                 PROMPT('Email Address'),AT(344,297),USE(?tra:AltEmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                                 ENTRY(@s255),AT(420,297,200,10),USE(tra:AltEmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                               END
                               CHECK('Use End User For Despatch (Disable Multiple Despatch)'),AT(336,316),USE(tra:UseCustDespAdd),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Customer Address As Despatch Address'),TIP('Do not use the Trade Account''s Address as the<13,10>Despatch Address for a Job. Use t' &|
   'he "Customer Address"'),VALUE('YES','NO')
                               PROMPT('RRC Waybill Prefix'),AT(336,330),USE(?tra:RRCWaybillPrefix:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(416,330,124,10),USE(tra:RRCWaybillPrefix),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('RRC Waybill Prefix'),TIP('RRC Waybill Prefix'),UPR
                               CHECK('Use Trade Contact No On Despatch Note'),AT(336,180),USE(tra:UseTradeContactNo),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Use Trade Contact Name'),TIP('Use Trade Contact Name'),VALUE('1','0')
                             END
                             TAB('OBF Address'),USE(?Tab19),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               STRING('OBF Delivery Address'),AT(350,76),USE(?String9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               PROMPT('Company Name'),AT(350,92),USE(?tra:OBFCompanyName:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(440,92,136,10),USE(tra:OBFCompanyName),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               PROMPT('Address'),AT(350,114),USE(?tra:OBFAddress1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(440,114,136,10),USE(tra:OBFAddress1),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               ENTRY(@s30),AT(440,129,136,10),USE(tra:OBFAddress2),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               PROMPT('Suburb'),AT(350,144),USE(?tra:OBFSuburb:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(440,144,124,10),USE(tra:OBFSuburb),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               BUTTON,AT(568,139),USE(?LookupOBFSuburb),TRN,FLAT,ICON('lookupp.jpg')
                               PROMPT('Contact Name'),AT(350,159),USE(?tra:OBFContactName:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(440,159,136,10),USE(tra:OBFContactName),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               PROMPT('Contact Number'),AT(350,174),USE(?tra:OBFContactNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s15),AT(440,174,64,10),USE(tra:OBFContactNumber),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               PROMPT('Email Address'),AT(350,189),USE(?tra:OBFEmailAddress:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s255),AT(440,189,136,10),USE(tra:OBFEmailAddress),FONT('Tahoma',8,010101H,,CHARSET:ANSI),COLOR(COLOR:White)
                             END
                           END
                         END
                         TAB('Business Hrs'),USE(?Tab14),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Monday - Friday Hours Of Business'),AT(316,62,152,48),USE(?Group9),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Start Time'),AT(320,75),USE(?tra:StartWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1),AT(364,75,56,10),USE(tra:StartWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Work Hours'),TIP('Start Work Hours'),UPR
                             STRING('(HH:MM)'),AT(428,75),USE(?String3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('End Time'),AT(320,91),USE(?tra:EndWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1),AT(364,91,56,10),USE(tra:EndWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Work Hours'),TIP('End Work Hours'),UPR
                             STRING('(HH:MM)'),AT(428,91),USE(?String4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('Include Saturday'),AT(316,118),USE(tra:IncludeSaturday),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Include Saturday')
                           GROUP('Saturday Hours Of Business'),AT(316,131,152,48),USE(?SaturdayHoursOfBusiness),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Start Time'),AT(320,147),USE(?tra:SatStartWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@t1b),AT(364,147,56,10),USE(tra:SatStartWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Time'),TIP('Start Time'),UPR
                             STRING('(HH:MM)'),AT(428,147),USE(?String3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('End Time'),AT(320,163),USE(?tra:SatEndWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@t1b),AT(364,163,56,10),USE(tra:SatEndWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Time'),TIP('End Time'),UPR
                             STRING('(HH:MM)'),AT(428,163),USE(?String4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           GROUP('Sunday Hours Of Business'),AT(472,131,152,48),USE(?SundayHoursOfBusiness),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('(HH:MM)'),AT(584,163),USE(?String4:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Start Time'),AT(476,147),USE(?tra:SunStartWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@t1b),AT(520,147,56,10),USE(tra:SunStartWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Time'),TIP('Start Time'),UPR
                             STRING('(HH:MM)'),AT(584,147),USE(?String3:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('End Time'),AT(476,163),USE(?tra:SunEndWorkHours:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@t1b),AT(520,163,56,10),USE(tra:SunEndWorkHours),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Time'),TIP('End Time'),UPR
                           END
                           PROMPT('Hours Of Business Exceptions'),AT(316,182),USE(?Prompt44),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(316,195,180,136),USE(?List:3),IMM,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('52R(2)|M~Date~@d6@40R(2)|M~Start Time~@t1b@40R(2)|M~End Time~@t1b@4L(2)|M~Day Of' &|
   'f~@s1@'),FROM(Queue:Browse:2)
                           BUTTON('template insert'),AT(500,195,56,16),USE(?Insert:3),HIDE
                           BUTTON,AT(500,246),USE(?INsert:4),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(500,275),USE(?Change:3),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(500,302),USE(?Delete:3),TRN,FLAT,ICON('deletep.jpg')
                           CHECK('Include Sunday'),AT(472,118),USE(tra:IncludeSunday),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Include Sunday')
                         END
                         TAB('VCP Defaults'),USE(?Tab15),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Region'),AT(316,66),USE(?tra:Region:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(392,66,124,10),USE(tra:Region),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Regiong'),TIP('Regiong'),UPR
                           BUTTON,AT(520,62),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('VCP Waybill Prefix'),AT(316,84),USE(?tra:VCPWaybillPrefix:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(392,84,124,10),USE(tra:VCPWaybillPrefix),COLOR(COLOR:White),MSG('VCP Waybill Prefix'),TIP('VCP Waybill Prefix'),UPR
                         END
                         TAB('Hubs'),USE(?Tab16),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(316,62,352,288),USE(?Sheet7),LEFT,SPREAD,UP
                             TAB('Hubs'),USE(?Tab20),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI)
                               PROMPT('Hubs Assigned To Account'),AT(340,66),USE(?Prompt47),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               LIST,AT(340,78,144,148),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Hub~@s30@'),FROM(Queue:Browse:1)
                               BUTTON,AT(488,158),USE(?Button:InsertHub),TRN,FLAT,ICON('insertp.jpg')
                               BUTTON,AT(488,200),USE(?Delete:2),TRN,FLAT,ICON('deletep.jpg')
                             END
                             TAB('Generic Accounts'),USE(?Tab21),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI)
                               BUTTON,AT(467,70),USE(?AddAccountButton),TRN,FLAT,ICON('AddGAccP.jpg')
                               STRING('Generic Accounts'),AT(380,85),USE(?String10),FONT(,,,FONT:bold)
                               STRING('Selected Accounts'),AT(555,85),USE(?String11),FONT(,,,FONT:bold)
                               LIST,AT(336,98,152,200),USE(?List:5),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('10L(2)|I@s1@0L(2)|M~Entry Long~@s8@0L(2)|M~Main Account Number~@s15@53L(2)|M~Acc' &|
   'ount No~@s15@120L(2)|M~Company Name~@s30@0L(2)|M~Check 01~@n1@'),FROM(Queue:Browse:4)
                               LIST,AT(513,98,152,200),USE(?List:6),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('10R|I@s1@0R|M~Record No~L(2)@n-14@0R|M~Head Acc~L(2)@s15@53L|M~Sub Acc~L(2)@s15@' &|
   '120L|M~Sub Acc Name~L(2)@s30@'),FROM(Queue:Browse:5)
                               BUTTON,AT(467,298),USE(?RemoveAccountButton),TRN,FLAT,ICON('RemGAccP.jpg')
                               BUTTON,AT(512,324),USE(?DASTAG:2),TRN,FLAT,ICON('STgItemP.jpg')
                               BUTTON,AT(569,324),USE(?DASTAGAll:2),TRN,FLAT,ICON('STagAllP.jpg')
                               BUTTON,AT(626,324),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('SUntagAP.jpg')
                               BUTTON,AT(336,324),USE(?DASTAG),TRN,FLAT,ICON('STgItemP.jpg')
                               BUTTON,AT(393,324),USE(?DASTAGAll),TRN,FLAT,ICON('STagAllP.jpg')
                               BUTTON,AT(450,324),USE(?DASUNTAGALL),TRN,FLAT,ICON('SUntagAP.jpg')
                               BUTTON('&Rev tags'),AT(540,280,14,13),USE(?DASREVTAG:2),HIDE
                               BUTTON('sho&W tags'),AT(612,278,12,14),USE(?DASSHOWTAG:2),HIDE
                               BUTTON('&Rev tags'),AT(368,276,14,14),USE(?DASREVTAG),HIDE
                               BUTTON('sho&W tags'),AT(436,278,14,14),USE(?DASSHOWTAG),HIDE
                             END
                           END
                         END
                         TAB('MRRCCodes'),USE(?Tab17),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(320,68,200,160),USE(?List:4),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('0L(2)|M~Entry Long~@s8@0L(2)|M~Entry 30 Small~@s20@120L(2)|M~Manufacturer~@s30@1' &|
   '20L(2)|M~ASV Code~@s30@'),FROM(Queue:Browse:3)
                           BUTTON,AT(528,146),USE(?Insert:2),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(528,174),USE(?Change:2),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(528,202),USE(?Delete:4),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(8,396),USE(?Button6),TRN,FLAT,LEFT,ICON('stanchap.jpg')
                       BUTTON,AT(540,396),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,396),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(76,396),USE(?FaultCodes),TRN,FLAT,HIDE,LEFT,ICON('amdfaup.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW2::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW2::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3
BRW14                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW34                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW34::Sort0:Locator StepLocatorClass                 !Default Locator
BRW45                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW45::Sort0:Locator StepLocatorClass                 !Default Locator
BRW47                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW47::Sort0:Locator StepLocatorClass                 !Default Locator
BRW48                CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW48::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB24               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

FDCB32               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
                     END

FDCB33               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB36               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:10        !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

FDCB30               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB31               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_sub_id   ushort,auto
save_tch_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:tra:Courier_Incoming                Like(tra:Courier_Incoming)
look:tra:Courier_Outgoing                Like(tra:Courier_Outgoing)
look:tra:SiteLocation                Like(tra:SiteLocation)
look:TRA2:Pre_Book_Region                Like(TRA2:Pre_Book_Region)
look:tra:StoresAccount                Like(tra:StoresAccount)
look:tra:Region                Like(tra:Region)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::49:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW47.UpdateBuffer
   glo:Queue.Pointer = sub_ali:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub_ali:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    GenericTag = '*'
  ELSE
    DELETE(glo:Queue)
    GenericTag = ''
  END
    Queue:Browse:4.GenericTag = GenericTag
  IF (GenericTag = '*')
    Queue:Browse:4.GenericTag_Icon = 2
  ELSE
    Queue:Browse:4.GenericTag_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::49:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW47.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW47::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub_ali:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW47.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::49:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW47.Reset
  SETCURSOR
  BRW47.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::49:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::49:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::49:QUEUE = glo:Queue
    ADD(DASBRW::49:QUEUE)
  END
  FREE(glo:Queue)
  BRW47.Reset
  LOOP
    NEXT(BRW47::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::49:QUEUE.Pointer = sub_ali:Account_Number
     GET(DASBRW::49:QUEUE,DASBRW::49:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub_ali:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW47.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::49:DASSHOWTAG Routine
   CASE DASBRW::49:TAGDISPSTATUS
   OF 0
      DASBRW::49:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::49:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::49:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW47.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::52:DASTAGONOFF Routine
  GET(Queue:Browse:5,CHOICE(?List:6))
  BRW48.UpdateBuffer
   glo:Queue2.Pointer2 = TRA1:RecordNo
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = TRA1:RecordNo
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    BranchTag = '*'
  ELSE
    DELETE(glo:Queue2)
    BranchTag = ''
  END
    Queue:Browse:5.BranchTag = BranchTag
  IF (Branchtag = '*')
    Queue:Browse:5.BranchTag_Icon = 2
  ELSE
    Queue:Browse:5.BranchTag_Icon = 1
  END
  PUT(Queue:Browse:5)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::52:DASTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW48.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW48::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = TRA1:RecordNo
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW48.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::52:DASUNTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW48.Reset
  SETCURSOR
  BRW48.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::52:DASREVTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::52:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::52:QUEUE = glo:Queue2
    ADD(DASBRW::52:QUEUE)
  END
  FREE(glo:Queue2)
  BRW48.Reset
  LOOP
    NEXT(BRW48::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::52:QUEUE.Pointer2 = TRA1:RecordNo
     GET(DASBRW::52:QUEUE,DASBRW::52:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = TRA1:RecordNo
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW48.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::52:DASSHOWTAG Routine
   CASE DASBRW::52:TAGDISPSTATUS
   OF 0
      DASBRW::52:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::52:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::52:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW48.ResetSort(1)
   SELECT(?List:6,CHOICE(?List:6))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
SubAccountAccess        Routine
    If SecurityCheck('USE SUB ACCOUNTS')
        ?tra:Use_Sub_Accounts{prop:Disable} = 1
    Else !SecurityCheck('USE SUB ACCOUNTS')
        !Check to see if user can untick sub accounts? - 4346  (DBH: 21-06-2004)
        If tra:Use_Sub_Accounts = 'YES'
            If SecurityCheck('USE SUB ACCOUNTS - UNTICK')
                ?tra:Use_Sub_Accounts{prop:Disable} = True
            Else !If SecurityCheck('USE SUB ACCOUNTS - UNTICK')
                ?tra:Use_Sub_Accounts{prop:Disable} = False
            End !If SecurityCheck('USE SUB ACCOUNTS - UNTICK')
        Else !If tra:Use_Sub_Accounts = 'YES'
            ?tra:Use_Sub_Accounts{prop:Disable} = False
        End !If tra:Use_Sub_Accounts = 'YES'
    End !If SecurityCheck('USE SUB ACCOUNTS')
ReplicatedFrom      Routine
    If tra:ReplicateAccount <> ''
        ?ReplicatedFrom{prop:Text} = 'Account Replicated From: ' & Clip(tra:ReplicateAccount)
        ?ReplicatedFrom{prop:Hide} = 0
    Else !If tra:ReplicateAccount <> ''
        ?ReplicatedFrom{prop:Hide} = 1
    End !If tra:ReplicateAccount <> ''
    Display()
Hide_Fields     Routine
    If tra:account_type <> 'CREDIT'
!        Disable(?tra:credit_limit)
!        Disable(?tra:credit_limit:prompt)
    Else
!        Enable(?tra:credit_limit)
!        Enable(?tra:credit_limit:prompt)
    End
    If tra:use_sub_accounts <> 'YES'
        Hide(?Insert)
        Hide(?Change)
        Hide(?Delete)
    Else
        Unhide(?Insert)
        Unhide(?Change)
        Unhide(?Delete)
    End
    Display()
Copy_Main_Account           Routine

    setcursor(cursor:wait)
    save_sub_id = access:subtracc.savefile()
    access:subtracc.clearkey(sub:main_account_key)
    sub:main_account_number = tra:account_number
    set(sub:main_account_key,sub:main_account_key)
    loop
        if access:subtracc.next()
           break
        end !if
        if sub:main_account_number <> tra:account_number      |
            then break.  ! end if
        Delete(subtracc)
    end !loop
    access:subtracc.restorefile(save_sub_id)
    setcursor()

    Clear(sub:record)
    sub:main_account_number = tra:account_number
    sub:account_number      = tra:account_number
    SUB:Company_Name        = tra:company_Name
    sub:branch              = 'MAIN ACCOUNT'
    TRA:Postcode            =TRA:Postcode
    TRA:Company_Name        =TRA:Company_Name
    TRA:Address_Line1       =TRA:Address_Line1
    TRA:Address_Line2       =TRA:Address_Line2
    TRA:Address_Line3       =TRA:Address_Line3
    TRA:Telephone_Number    =TRA:Telephone_Number
    TRA:Fax_Number          =TRA:Fax_Number
    Access:subtracc.Insert()

Fill_Rates      Routine
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:labour_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_labour_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:parts_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_parts_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:retail_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_retail_temp  = vat:vat_rate
    end

    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Trade Account'
  OF ChangeRecord
    ActionMessage = 'Changing A Trade Account'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020297'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTRADEACC')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tra:Record,History::tra:Record)
  SELF.AddHistoryField(?tra:Account_Number,2)
  SELF.AddHistoryField(?tra:Company_Name,4)
  SELF.AddHistoryField(?tra:BranchIdentification,118)
  SELF.AddHistoryField(?tra:Postcode,3)
  SELF.AddHistoryField(?tra:Hub,141)
  SELF.AddHistoryField(?tra:Address_Line1,5)
  SELF.AddHistoryField(?tra:Address_Line2,6)
  SELF.AddHistoryField(?tra:Address_Line3,7)
  SELF.AddHistoryField(?tra:Telephone_Number,8)
  SELF.AddHistoryField(?tra:Fax_Number,9)
  SELF.AddHistoryField(?tra:EmailAddress,10)
  SELF.AddHistoryField(?tra:VAT_Number,35)
  SELF.AddHistoryField(?tra:Contact_Name,11)
  SELF.AddHistoryField(?tra:Courier_Incoming,32)
  SELF.AddHistoryField(?tra:Courier_Outgoing,33)
  SELF.AddHistoryField(?tra:Courier_Cost,53)
  SELF.AddHistoryField(?tra:Loan_Stock_Type,52)
  SELF.AddHistoryField(?tra:Exchange_Stock_Type,51)
  SELF.AddHistoryField(?tra:Turnaround_Time,56)
  SELF.AddHistoryField(?tra:Stop_Account,23)
  SELF.AddHistoryField(?tra:Allow_Cash_Sales,24)
  SELF.AddHistoryField(?tra:RemoteRepairCentre,112)
  SELF.AddHistoryField(?tra:SiteLocation,113)
  SELF.AddHistoryField(?tra:RtnExchangeAccount,166)
  SELF.AddHistoryField(?tra:coTradingName,154)
  SELF.AddHistoryField(?tra:coTradingName2,155)
  SELF.AddHistoryField(?tra:coLocation,156)
  SELF.AddHistoryField(?tra:coRegistrationNo,157)
  SELF.AddHistoryField(?tra:coVATNumber,158)
  SELF.AddHistoryField(?tra:coAddressLine1,159)
  SELF.AddHistoryField(?tra:coAddressLine2,160)
  SELF.AddHistoryField(?tra:coAddressLine3,161)
  SELF.AddHistoryField(?tra:coAddressLine4,162)
  SELF.AddHistoryField(?tra:coTelephoneNumber,163)
  SELF.AddHistoryField(?tra:coFaxNumber,164)
  SELF.AddHistoryField(?tra:coEmailAddress,165)
  SELF.AddHistoryField(?tra:Use_Delivery_Address,36)
  SELF.AddHistoryField(?tra:Use_Collection_Address,37)
  SELF.AddHistoryField(?tra:Use_Customer_Address,60)
  SELF.AddHistoryField(?tra:Invoice_Customer_Address,61)
  SELF.AddHistoryField(?tra:AutoSendStatusEmails,94)
  SELF.AddHistoryField(?tra:DoMSISDNCheck,140)
  SELF.AddHistoryField(?tra:Activate48Hour,130)
  SELF.AddHistoryField(?tra:IgnoreReplenishmentProcess,143)
  SELF.AddHistoryField(?tra:Use_Sub_Accounts,21)
  SELF.AddHistoryField(?tra:Invoice_Sub_Accounts,22)
  SELF.AddHistoryField(?tra:ExchangeAcc,66)
  SELF.AddHistoryField(?tra:SecondYearAccount,129)
  SELF.AddHistoryField(?tra:ExcludeBouncer,72)
  SELF.AddHistoryField(?tra:ShowRepairTypes,78)
  SELF.AddHistoryField(?tra:ChargeType,64)
  SELF.AddHistoryField(?tra:WarChargeType,65)
  SELF.AddHistoryField(?tra:StopThirdParty,86)
  SELF.AddHistoryField(?tra:ChangeCollAddress,99)
  SELF.AddHistoryField(?tra:ChangeDelAddress,100)
  SELF.AddHistoryField(?tra:Allow_Loan,39)
  SELF.AddHistoryField(?tra:Allow_Exchange,40)
  SELF.AddHistoryField(?tra:Force_Estimate,54)
  SELF.AddHistoryField(?tra:Estimate_If_Over,55)
  SELF.AddHistoryField(?tra:ForceCommonFault,76)
  SELF.AddHistoryField(?tra:ForceOrderNumber,77)
  SELF.AddHistoryField(?tra:Use_Contact_Name,34)
  SELF.AddHistoryField(?tra:RepairEngineerQA,128)
  SELF.AddHistoryField(?tra:AllocateQAEng,119)
  SELF.AddHistoryField(?tra:OverrideMobileDefault,137)
  SELF.AddHistoryField(?tra:ZeroChargeable,62)
  SELF.AddHistoryField(?tra:RefurbCharge,63)
  SELF.AddHistoryField(?tra:InvoiceType,83)
  SELF.AddHistoryField(?tra:InvoiceTypeComplete,121)
  SELF.AddHistoryField(?tra:MultiInvoice,67)
  SELF.AddHistoryField(?tra:InvoiceAtDespatch,82)
  SELF.AddHistoryField(?tra:InvoiceAtCompletion,120)
  SELF.AddHistoryField(?tra:Labour_VAT_Code,15)
  SELF.AddHistoryField(?tra:Parts_VAT_Code,16)
  SELF.AddHistoryField(?tra:Retail_VAT_Code,17)
  SELF.AddHistoryField(?tra:CompanyOwned,136)
  SELF.AddHistoryField(?tra:AllowCreditNotes,139)
  SELF.AddHistoryField(?tra:Retail_Payment_Type,58)
  SELF.AddHistoryField(?tra:Retail_Price_Structure,59)
  SELF.AddHistoryField(?tra:Print_Retail_Picking_Note,46)
  SELF.AddHistoryField(?tra:Price_Retail_Despatch,28)
  SELF.AddHistoryField(?tra:RetailZeroParts,73)
  SELF.AddHistoryField(?tra:EuroApplies,75)
  SELF.AddHistoryField(?tra:StoresAccount,127)
  SELF.AddHistoryField(?tra:Line500AccountNumber,135)
  SELF.AddHistoryField(?tra:Password,57)
  SELF.AddHistoryField(?tra:WebPassword1,80)
  SELF.AddHistoryField(?tra:WebMemo,79)
  SELF.AddHistoryField(?tra:E1,87)
  SELF.AddHistoryField(?tra:E2,88)
  SELF.AddHistoryField(?tra:E3,89)
  SELF.AddHistoryField(?tra:SBOnlineJobProgress,153)
  SELF.AddHistoryField(?tra:UseSBOnline,142)
  SELF.AddHistoryField(?tra:Print_Despatch_Notes,44)
  SELF.AddHistoryField(?tra:IgnoreDespatch,49)
  SELF.AddHistoryField(?tra:Print_Despatch_Complete,29)
  SELF.AddHistoryField(?tra:Print_Despatch_Despatch,30)
  SELF.AddHistoryField(?tra:HideDespAdd,74)
  SELF.AddHistoryField(?tra:Despatch_Note_Per_Item,47)
  SELF.AddHistoryField(?TRA:Summary_Despatch_Notes:2,50)
  SELF.AddHistoryField(?tra:IndividualSummary,84)
  SELF.AddHistoryField(?tra:Price_Despatch,25)
  SELF.AddHistoryField(?tra:Price_First_Copy_Only,26)
  SELF.AddHistoryField(?tra:Num_Despatch_Note_Copies,27)
  SELF.AddHistoryField(?tra:Despatch_Paid_Jobs,43)
  SELF.AddHistoryField(?tra:SetDespatchJobStatus,104)
  SELF.AddHistoryField(?tra:DespatchedJobStatus,106)
  SELF.AddHistoryField(?tra:Despatch_Invoiced_Jobs,42)
  SELF.AddHistoryField(?tra:SetInvoicedJobStatus,103)
  SELF.AddHistoryField(?tra:InvoicedJobStatus,105)
  SELF.AddHistoryField(?tra:Skip_Despatch,48)
  SELF.AddHistoryField(?tra:UseDespatchDetails,90)
  SELF.AddHistoryField(?tra:AltTelephoneNumber,91)
  SELF.AddHistoryField(?tra:AltFaxNumber,92)
  SELF.AddHistoryField(?tra:AltEmailAddress,93)
  SELF.AddHistoryField(?tra:UseCustDespAdd,38)
  SELF.AddHistoryField(?tra:RRCWaybillPrefix,145)
  SELF.AddHistoryField(?tra:UseTradeContactNo,85)
  SELF.AddHistoryField(?tra:OBFCompanyName,146)
  SELF.AddHistoryField(?tra:OBFAddress1,147)
  SELF.AddHistoryField(?tra:OBFAddress2,148)
  SELF.AddHistoryField(?tra:OBFSuburb,149)
  SELF.AddHistoryField(?tra:OBFContactName,150)
  SELF.AddHistoryField(?tra:OBFContactNumber,151)
  SELF.AddHistoryField(?tra:OBFEmailAddress,152)
  SELF.AddHistoryField(?tra:StartWorkHours,123)
  SELF.AddHistoryField(?tra:EndWorkHours,124)
  SELF.AddHistoryField(?tra:IncludeSaturday,125)
  SELF.AddHistoryField(?tra:SatStartWorkHours,131)
  SELF.AddHistoryField(?tra:SatEndWorkHours,132)
  SELF.AddHistoryField(?tra:SunStartWorkHours,133)
  SELF.AddHistoryField(?tra:SunEndWorkHours,134)
  SELF.AddHistoryField(?tra:IncludeSunday,126)
  SELF.AddHistoryField(?tra:Region,138)
  SELF.AddHistoryField(?tra:VCPWaybillPrefix,144)
  SELF.AddUpdateFile(Access:TRADEACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ASVACC.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:REGIONS.Open
  Relate:STATUS.Open
  Relate:STOCKTYP.Open
  Relate:SUBTRACC_ALIAS.Open
  Relate:TRADEAC2.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:COURIER.UseFile
  Access:LOCATION.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRADEACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW14.Init(?List:3,Queue:Browse:2.ViewPosition,BRW14::View:Browse,Queue:Browse:2,Relate:TRABUSHR,SELF)
  BRW34.Init(?List:2,Queue:Browse:1.ViewPosition,BRW34::View:Browse,Queue:Browse:1,Relate:TRAHUBS,SELF)
  BRW45.Init(?List:4,Queue:Browse:3.ViewPosition,BRW45::View:Browse,Queue:Browse:3,Relate:ASVACC,SELF)
  BRW47.Init(?List:5,Queue:Browse:4.ViewPosition,BRW47::View:Browse,Queue:Browse:4,Relate:SUBTRACC_ALIAS,SELF)
  BRW48.Init(?List:6,Queue:Browse:5.ViewPosition,BRW48::View:Browse,Queue:Browse:5,Relate:TRAHUBAC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?StockAccessLevels{prop:Hide} = 0
  End !Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  !Security Check
  !Does the user have access to Use Sub Accounts - 4346  (DBH: 21-06-2004)
  Do SubAccountAccess
  
  If SecurityCheck('INVOICE SUB ACCOUNTS')
      ?tra:Invoice_Sub_Accounts{prop:Disable} = 1
  Else !SecurityCheck('INVOICE SUB ACCOUNTS')
      ?tra:Invoice_Sub_Accounts{prop:Disable} = 0
  End !SecurityCheck('INVOICE SUB ACCOUNTS')
  
  If ThisWindow.Request = InsertRecord
      ?ReplicateFrom{prop:Hide} = 0
  End !ThisWindow.Request = InsertRecord
  
  
  Do ReplicatedFrom
  ! Inserting (DBH 04/08/2006) # 6643 - Find suburb? Was the postcode manually entered
  If ThisWindow.Request = ChangeRecord
      ! If edited the record and cannot find the suburb, assume that the postcode was manually entered. (DBH: 04/08/2006)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = tra:Address_Line3
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?tra:Postcode{prop:Skip} = True
          ?tra:Postcode{prop:ReadOnly} = True
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?tra:Postcode{prop:Skip} = False
          ?tra:Postcode{prop:ReadOnly} = False
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
  End ! If ThisWindow.Request = ChangeRecord
  ! End (DBH 04/08/2006) #6643
  if clip(tra:Account_Number) = 'AA20' then
      unhide(?tra2:UseSparesRequest)
  END
      !Look for the tradeac2 extention
      Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
      TRA2:Account_Number = TRA:Account_Number
      if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
          !look for a blank record
          Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = ''
          if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
              !not found - crete one now
              Access:TradeAc2.primerecord()
              TRA2:Account_Number = TRA:Account_Number
              Access:TradeAc2.tryinsert()
          ELSE
              !update the blank one
              TRA2:Account_Number = TRA:Account_Number
              Access:TradeAc2.update()
          END !if blank one exists
      END !if this account number does not eixst
  !TB13512 - J - 02/04/15 - don't let them make more Unstopped RRC than is licensed
  
      !the number RRC permitted by licence
      Set(Defaults)
      Access:Defaults.next()
      LicensedRRC = def:IrishVatRate / 0.42
  
      !number existing
      CountedRRC = 0
      ACCESS:TradeAcc_Alias.clearkey(tra_ali:RecordNumberKey)
      tra_ali:RecordNumber = 0
      Set(tra_ali:RecordNumberKey,tra_ali:RecordNumberKey)
      Loop
          if access:Tradeacc_Alias.next() then break.
          if tra_ali:Stop_Account = 'NO' and tra_ali:RemoteRepairCentre = 1 then
              CountedRRC += 1
          END
      END
  ! Save Window Name
   AddToLog('Window','Open','UpdateTRADEACC')
  ?List{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    ?Tab3{PROP:TEXT} = 'By Account Number'
    ?Tab5{PROP:TEXT} = 'By Branch'
    ?Tab4{PROP:TEXT} = 'By Company Name'
    ?List{PROP:FORMAT} ='67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#120L(2)|M~Company Name~@s40@#3#'
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tra:Courier_Incoming{Prop:Tip} AND ~?LookupIncomingCourier{Prop:Tip}
     ?LookupIncomingCourier{Prop:Tip} = 'Select ' & ?tra:Courier_Incoming{Prop:Tip}
  END
  IF ?tra:Courier_Incoming{Prop:Msg} AND ~?LookupIncomingCourier{Prop:Msg}
     ?LookupIncomingCourier{Prop:Msg} = 'Select ' & ?tra:Courier_Incoming{Prop:Msg}
  END
  IF ?tra:Courier_Outgoing{Prop:Tip} AND ~?LookupOutgoingCourier{Prop:Tip}
     ?LookupOutgoingCourier{Prop:Tip} = 'Select ' & ?tra:Courier_Outgoing{Prop:Tip}
  END
  IF ?tra:Courier_Outgoing{Prop:Msg} AND ~?LookupOutgoingCourier{Prop:Msg}
     ?LookupOutgoingCourier{Prop:Msg} = 'Select ' & ?tra:Courier_Outgoing{Prop:Msg}
  END
  IF ?tra:SiteLocation{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?tra:SiteLocation{Prop:Tip}
  END
  IF ?tra:SiteLocation{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?tra:SiteLocation{Prop:Msg}
  END
  IF ?tra:StoresAccount{Prop:Tip} AND ~?LookupStoresAccount{Prop:Tip}
     ?LookupStoresAccount{Prop:Tip} = 'Select ' & ?tra:StoresAccount{Prop:Tip}
  END
  IF ?tra:StoresAccount{Prop:Msg} AND ~?LookupStoresAccount{Prop:Msg}
     ?LookupStoresAccount{Prop:Msg} = 'Select ' & ?tra:StoresAccount{Prop:Msg}
  END
  IF ?tra:Region{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tra:Region{Prop:Tip}
  END
  IF ?tra:Region{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tra:Region{Prop:Msg}
  END
  IF ?TRA2:Pre_Book_Region{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?TRA2:Pre_Book_Region{Prop:Tip}
  END
  IF ?TRA2:Pre_Book_Region{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?TRA2:Pre_Book_Region{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,sub:Main_Branch_Key)
  BRW2.AddRange(sub:Main_Account_Number,Relate:SUBTRACC,Relate:TRADEACC)
  BRW2.AddLocator(BRW2::Sort2:Locator)
  BRW2::Sort2:Locator.Init(?sub:Branch,sub:Branch,1,BRW2)
  BRW2.AddSortOrder(,sub:Main_Name_Key)
  BRW2.AddRange(sub:Main_Account_Number,Relate:SUBTRACC,Relate:TRADEACC)
  BRW2.AddLocator(BRW2::Sort1:Locator)
  BRW2::Sort1:Locator.Init(?sub:Company_Name,sub:Company_Name,1,BRW2)
  BRW2.AddSortOrder(,sub:Main_Account_Key)
  BRW2.AddRange(sub:Main_Account_Number,tra:Account_Number)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(?sub:Account_Number,sub:Account_Number,1,BRW2)
  BRW2.AddField(sub:Account_Number,BRW2.Q.sub:Account_Number)
  BRW2.AddField(sub:Branch,BRW2.Q.sub:Branch)
  BRW2.AddField(sub:Company_Name,BRW2.Q.sub:Company_Name)
  BRW2.AddField(sub:RecordNumber,BRW2.Q.sub:RecordNumber)
  BRW2.AddField(sub:Main_Account_Number,BRW2.Q.sub:Main_Account_Number)
  BRW14.Q &= Queue:Browse:2
  BRW14.AddSortOrder(,tbh:EndTimeKey)
  BRW14.AddRange(tbh:RefNumber,Relate:TRABUSHR,Relate:TRADEACC)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,tbh:TheDate,1,BRW14)
  BIND('tmp:DayOff',tmp:DayOff)
  BRW14.AddField(tbh:TheDate,BRW14.Q.tbh:TheDate)
  BRW14.AddField(tbh:StartTime,BRW14.Q.tbh:StartTime)
  BRW14.AddField(tbh:EndTime,BRW14.Q.tbh:EndTime)
  BRW14.AddField(tmp:DayOff,BRW14.Q.tmp:DayOff)
  BRW14.AddField(tbh:RecordNumber,BRW14.Q.tbh:RecordNumber)
  BRW14.AddField(tbh:RefNumber,BRW14.Q.tbh:RefNumber)
  BRW34.Q &= Queue:Browse:1
  BRW34.AddSortOrder(,trh:HubKey)
  BRW34.AddRange(trh:TRADEACCAccountNumber,tra:Account_Number)
  BRW34.AddLocator(BRW34::Sort0:Locator)
  BRW34::Sort0:Locator.Init(,trh:Hub,1,BRW34)
  BRW34.AddField(trh:Hub,BRW34.Q.trh:Hub)
  BRW34.AddField(trh:RecordNumber,BRW34.Q.trh:RecordNumber)
  BRW34.AddField(trh:TRADEACCAccountNumber,BRW34.Q.trh:TRADEACCAccountNumber)
  BRW45.Q &= Queue:Browse:3
  BRW45.AddSortOrder(,ASV:TradeACCManufKey)
  BRW45.AddRange(ASV:TradeAccNo,tra:Account_Number)
  BRW45.AddLocator(BRW45::Sort0:Locator)
  BRW45::Sort0:Locator.Init(,ASV:Manufacturer,1,BRW45)
  BRW45.AddField(ASV:RecordNo,BRW45.Q.ASV:RecordNo)
  BRW45.AddField(ASV:TradeAccNo,BRW45.Q.ASV:TradeAccNo)
  BRW45.AddField(ASV:Manufacturer,BRW45.Q.ASV:Manufacturer)
  BRW45.AddField(ASV:ASVCode,BRW45.Q.ASV:ASVCode)
  BRW47.Q &= Queue:Browse:4
  BRW47.AddSortOrder(,sub_ali:GenericAccountKey)
  BRW47.AddRange(sub_ali:Generic_Account,GenericAccountFlag)
  BRW47.AddLocator(BRW47::Sort0:Locator)
  BRW47::Sort0:Locator.Init(,sub_ali:Account_Number,1,BRW47)
  BIND('GenericTag',GenericTag)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW47.AddField(GenericTag,BRW47.Q.GenericTag)
  BRW47.AddField(sub_ali:RecordNumber,BRW47.Q.sub_ali:RecordNumber)
  BRW47.AddField(sub_ali:Main_Account_Number,BRW47.Q.sub_ali:Main_Account_Number)
  BRW47.AddField(sub_ali:Account_Number,BRW47.Q.sub_ali:Account_Number)
  BRW47.AddField(sub_ali:Company_Name,BRW47.Q.sub_ali:Company_Name)
  BRW47.AddField(sub_ali:Generic_Account,BRW47.Q.sub_ali:Generic_Account)
  BRW48.Q &= Queue:Browse:5
  BRW48.AddSortOrder(,TRA1:HeadAccSubAccKey)
  BRW48.AddRange(TRA1:HeadAcc,tra:Account_Number)
  BRW48.AddLocator(BRW48::Sort0:Locator)
  BRW48::Sort0:Locator.Init(,TRA1:SubAcc,1,BRW48)
  BIND('BranchTag',BranchTag)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW48.AddField(BranchTag,BRW48.Q.BranchTag)
  BRW48.AddField(TRA1:RecordNo,BRW48.Q.TRA1:RecordNo)
  BRW48.AddField(TRA1:HeadAcc,BRW48.Q.TRA1:HeadAcc)
  BRW48.AddField(TRA1:SubAcc,BRW48.Q.TRA1:SubAcc)
  BRW48.AddField(TRA1:SubAccName,BRW48.Q.TRA1:SubAccName)
  IF ?tra:Stop_Account{Prop:Checked} = True
    UNHIDE(?TRA:Allow_Cash_Sales)
  END
  IF ?tra:Stop_Account{Prop:Checked} = False
    HIDE(?TRA:Allow_Cash_Sales)
  END
  IF ?tra:Use_Sub_Accounts{Prop:Checked} = True
    UNHIDE(?TRA:Invoice_Sub_Accounts)
    UNHIDE(?ReplicateSubAccount)
  END
  IF ?tra:Use_Sub_Accounts{Prop:Checked} = False
    HIDE(?TRA:Invoice_Sub_Accounts)
    HIDE(?ReplicateSubAccount)
  END
  IF ?tra:RefurbCharge{Prop:Checked} = True
    ENABLE(?Charge_Type_Group)
  END
  IF ?tra:RefurbCharge{Prop:Checked} = False
    DISABLE(?Charge_Type_Group)
  END
  IF ?tra:InvoiceAtDespatch{Prop:Checked} = True
    UNHIDE(?tra:InvoiceType)
  END
  IF ?tra:InvoiceAtDespatch{Prop:Checked} = False
    HIDE(?tra:InvoiceType)
  END
  IF ?tra:InvoiceAtCompletion{Prop:Checked} = True
    UNHIDE(?tra:InvoiceTypeComplete)
  END
  IF ?tra:InvoiceAtCompletion{Prop:Checked} = False
    HIDE(?tra:InvoiceTypeComplete)
  END
  IF ?tra:Print_Despatch_Notes{Prop:Checked} = True
    ENABLE(?TRA:Print_Despatch_Complete)
    ENABLE(?TRA:Print_Despatch_Despatch)
    ENABLE(?TRA:Price_Despatch)
    ENABLE(?tra:HideDespAdd)
    ENABLE(?tra:UseTradeContactNo)
    ENABLE(?tra:Price_First_Copy_Only)
    ENABLE(?tra:Num_Despatch_Note_Copies)
  END
  IF ?tra:Print_Despatch_Notes{Prop:Checked} = False
    tra:Summary_Despatch_Notes = 'NO'
    tra:Despatch_Note_Per_Item = 'NO'
    tra:Print_Despatch_Despatch = 'NO'
    tra:Print_Despatch_Complete = 'NO'
    DISABLE(?TRA:Print_Despatch_Complete)
    DISABLE(?TRA:Print_Despatch_Despatch)
    DISABLE(?TRA:Price_Despatch)
    DISABLE(?tra:HideDespAdd)
    DISABLE(?tra:UseTradeContactNo)
    DISABLE(?tra:Price_First_Copy_Only)
    DISABLE(?tra:Num_Despatch_Note_Copies)
  END
  IF ?tra:Print_Despatch_Despatch{Prop:Checked} = True
    ENABLE(?Despatch_note_type)
  END
  IF ?tra:Print_Despatch_Despatch{Prop:Checked} = False
    DISABLE(?Despatch_note_type)
  END
  IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = True
    UNHIDE(?tra:IndividualSummary)
  END
  IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = False
    HIDE(?tra:IndividualSummary)
  END
  IF ?tra:Price_Despatch{Prop:Checked} = True
    UNHIDE(?tra:Price_First_Copy_Only)
  END
  IF ?tra:Price_Despatch{Prop:Checked} = False
    HIDE(?tra:Price_First_Copy_Only)
  END
  IF ?tra:Despatch_Paid_Jobs{Prop:Checked} = True
    UNHIDE(?tra:SetDespatchJobStatus)
  END
  IF ?tra:Despatch_Paid_Jobs{Prop:Checked} = False
    tra:SetDespatchJobStatus = 0
    HIDE(?tra:SetDespatchJobStatus)
  END
  IF ?tra:SetDespatchJobStatus{Prop:Checked} = True
    UNHIDE(?tra:DespatchedJobStatus)
    UNHIDE(?CompletedStatus)
  END
  IF ?tra:SetDespatchJobStatus{Prop:Checked} = False
    HIDE(?tra:DespatchedJobStatus)
    HIDE(?CompletedStatus)
  END
  IF ?tra:Despatch_Invoiced_Jobs{Prop:Checked} = True
    UNHIDE(?tra:SetInvoicedJobStatus)
  END
  IF ?tra:Despatch_Invoiced_Jobs{Prop:Checked} = False
    tra:SetInvoicedJobStatus = 0
    HIDE(?tra:SetInvoicedJobStatus)
  END
  IF ?tra:SetInvoicedJobStatus{Prop:Checked} = True
    UNHIDE(?tra:InvoicedJobStatus)
    UNHIDE(?CompletedStatus:2)
  END
  IF ?tra:SetInvoicedJobStatus{Prop:Checked} = False
    HIDE(?tra:InvoicedJobStatus)
    HIDE(?CompletedStatus:2)
  END
  IF ?tra:UseDespatchDetails{Prop:Checked} = True
    ENABLE(?AlternativeContactNumbers)
  END
  IF ?tra:UseDespatchDetails{Prop:Checked} = False
    DISABLE(?AlternativeContactNumbers)
  END
  IF ?tra:IncludeSaturday{Prop:Checked} = True
    ENABLE(?SaturdayHoursOfBusiness)
  END
  IF ?tra:IncludeSaturday{Prop:Checked} = False
    DISABLE(?SaturdayHoursOfBusiness)
  END
  IF ?tra:IncludeSunday{Prop:Checked} = True
    ENABLE(?SundayHoursOfBusiness)
  END
  IF ?tra:IncludeSunday{Prop:Checked} = False
    DISABLE(?SundayHoursOfBusiness)
  END
  BRW2.AskProcedure = 7
  BRW14.AskProcedure = 8
  BRW34.AskProcedure = 9
  BRW45.AskProcedure = 10
  FDCB18.Init(tra:Labour_VAT_Code,?tra:Labour_VAT_Code,Queue:FileDropCombo:1.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:1,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo:1
  FDCB18.AddSortOrder(vat:Vat_code_Key)
  FDCB18.AddField(vat:VAT_Code,FDCB18.Q.vat:VAT_Code)
  FDCB18.AddField(vat:VAT_Rate,FDCB18.Q.vat:VAT_Rate)
  FDCB18.AddUpdateField(vat:VAT_Code,tra:Labour_VAT_Code)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB15.Init(tra:Parts_VAT_Code,?tra:Parts_VAT_Code,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(vat:Vat_code_Key)
  FDCB15.AddField(vat:VAT_Code,FDCB15.Q.vat:VAT_Code)
  FDCB15.AddField(vat:VAT_Rate,FDCB15.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB24.Init(tra:Loan_Stock_Type,?tra:Loan_Stock_Type,Queue:FileDropCombo:6.ViewPosition,FDCB24::View:FileDropCombo,Queue:FileDropCombo:6,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB24.Q &= Queue:FileDropCombo:6
  FDCB24.AddSortOrder(stp:Stock_Type_Key)
  FDCB24.SetFilter('Upper(stp:use_loan) = ''YES''')
  FDCB24.AddField(stp:Stock_Type,FDCB24.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB24.WindowComponent)
  FDCB24.DefaultFill = 0
  FDCB32.Init(tra:Exchange_Stock_Type,?tra:Exchange_Stock_Type,Queue:FileDropCombo:7.ViewPosition,FDCB32::View:FileDropCombo,Queue:FileDropCombo:7,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB32.Q &= Queue:FileDropCombo:7
  FDCB32.AddSortOrder(stp:Use_Exchange_Key)
  FDCB32.SetFilter('Upper(stp:use_exchange) = ''YES''')
  FDCB32.AddField(stp:Stock_Type,FDCB32.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB32.WindowComponent)
  FDCB32.DefaultFill = 0
  FDCB33.Init(tra:Turnaround_Time,?tra:Turnaround_Time,Queue:FileDropCombo:8.ViewPosition,FDCB33::View:FileDropCombo,Queue:FileDropCombo:8,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB33.Q &= Queue:FileDropCombo:8
  FDCB33.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB33.AddField(tur:Turnaround_Time,FDCB33.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB33.WindowComponent)
  FDCB33.DefaultFill = 0
  FDCB36.Init(tra:Retail_VAT_Code,?tra:Retail_VAT_Code,Queue:FileDropCombo:10.ViewPosition,FDCB36::View:FileDropCombo,Queue:FileDropCombo:10,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB36.Q &= Queue:FileDropCombo:10
  FDCB36.AddSortOrder(vat:Vat_code_Key)
  FDCB36.AddField(vat:VAT_Code,FDCB36.Q.vat:VAT_Code)
  FDCB36.AddField(vat:VAT_Rate,FDCB36.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB36.WindowComponent)
  FDCB36.DefaultFill = 0
  FDCB5.Init(tra:ChargeType,?tra:ChargeType,Queue:FileDropCombo:11.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:11,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:11
  FDCB5.AddSortOrder(cha:Warranty_Key)
  FDCB5.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB5.AddField(cha:Charge_Type,FDCB5.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB16.Init(tra:WarChargeType,?tra:WarChargeType,Queue:FileDropCombo:12.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:12,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:12
  FDCB16.AddSortOrder(cha:Warranty_Key)
  FDCB16.SetFilter('Upper(cha:warranty) = ''YES''')
  FDCB16.AddField(cha:Charge_Type,FDCB16.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  FDCB30.Init(tra:DespatchedJobStatus,?tra:DespatchedJobStatus,Queue:FileDropCombo.ViewPosition,FDCB30::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB30.Q &= Queue:FileDropCombo
  FDCB30.AddSortOrder(sts:JobKey)
  FDCB30.AddRange(sts:Job,tmp:Yes)
  FDCB30.AddField(sts:Status,FDCB30.Q.sts:Status)
  FDCB30.AddField(sts:Ref_Number,FDCB30.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB30.WindowComponent)
  FDCB30.DefaultFill = 0
  FDCB31.Init(tra:InvoicedJobStatus,?tra:InvoicedJobStatus,Queue:FileDropCombo:2.ViewPosition,FDCB31::View:FileDropCombo,Queue:FileDropCombo:2,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB31.Q &= Queue:FileDropCombo:2
  FDCB31.AddSortOrder(sts:JobKey)
  FDCB31.AddRange(sts:Job,tmp:Yes)
  FDCB31.AddField(sts:Status,FDCB31.Q.sts:Status)
  FDCB31.AddField(sts:Ref_Number,FDCB31.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB31.WindowComponent)
  FDCB31.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  BRW34.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW34.AskProcedure = 0
      CLEAR(BRW34.AskProcedure, 1)
    END
  END
  BRW45.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW45.AskProcedure = 0
      CLEAR(BRW45.AskProcedure, 1)
    END
  END
  BRW47.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW47.AskProcedure = 0
      CLEAR(BRW47.AskProcedure, 1)
    END
  END
  BRW48.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW48.AskProcedure = 0
      CLEAR(BRW48.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  brw2.popup.kill
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:5{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:6{Prop:Alrt,239} = SpaceKey
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:ASVACC.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:REGIONS.Close
    Relate:STATUS.Close
    Relate:STOCKTYP.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:TRADEAC2.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTRADEACC')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    tra:Invoice_Sub_Accounts = 'NO'
    tra:Stop_Account = 'NO'
    tra:Allow_Cash_Sales = 'NO'
    tra:Use_Sub_Accounts = 'NO'
    tra:Price_Despatch = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If Field() = ?Insert
    If tra:use_sub_accounts <> 'YES'
        do_update# = 0
    Else!If tra:use_sub_account <> 'YES'
        glo:Select12   = tra:account_number
    End!If tra:use_sub_account <> 'YES'
  
  End !If number <> 3
  If Field() = ?Insert Or Field() = ?Change
      If tra:invoice_sub_accounts = 'YES'
          glo:select2  = 'FINANCIAL'
      Else
          glo:Select2 = ''
      End!If tra:invoice_sub_accounts = 'YES'
  End !Field() = ?Insert Or Field() = ?Change
  
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickCouriers
      PickCouriers
      PickSiteLocations
      BrowseRegions
      PickSubAccountsAlias
      BrowseRegions
      UpdateSUBTRACC
      UpdateTradeHoursOfBusiness
      UpdateTradeHubs
      UpdateASVACC
    END
    ReturnValue = GlobalResponse
  END
     glo:Select1 = ''
     glo:select12 = ''
  End !If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?TRA2:UseSparesRequest
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRA2:UseSparesRequest, Accepted)
      !12031 - must have rapid stock allocation turned on at this location to use this
      if TRA2:UseSparesRequest = 'YES' then
          !A message must appear informing the User that the Rapid Stock Allocation is not ticked therefore AA20 can not use the spares requesting process.
          Access:location.clearkey(loc:Location_Key)
          loc:Location = tra:SiteLocation
          if access:Location.fetch(loc:Location_Key) = level:benign
              if loc:UseRapidStock = false then
                  miss# = missive('Rapid Stock Allocation is not ticked at this location therefore AA20 can not use the spares requesting process.','ServiceBase 3g','mexclam.jpg','OK')
                  TRA2:UseSparesRequest = 'NO'
                  display()
              END !if using rapid stock
          ELSE
              miss# = missive('Rapid Stock Allocation is not found at this location therefore AA20 can not use the spares requesting process.','ServiceBase 3g','mexclam.jpg','OK')
              TRA2:UseSparesRequest = 'NO'
              display()
          END !if location fetched
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRA2:UseSparesRequest, Accepted)
    OF ?tra:Use_Sub_Accounts
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Use_Sub_Accounts, Accepted)
      If tra:use_sub_accounts <> use_sub_account_temp
          error# = 0
          If tra:account_number = ''
              Case Missive('You have not entered an Account Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              error# = 1
              tra:use_sub_accounts = use_sub_account_temp
          End!If tra:account_number = ''
          If tra:company_name = '' And error# = 0
              Case Missive('You have not entered a Company Name.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              error# = 1
              tra:use_sub_accounts = use_sub_account_temp
          End!If tra:company_name = '' And error# = 0
          If error# = 0
              If tra:use_sub_accounts = 'YES'
                  Case Missive('Sub Accounts are used when invoices are sent to one location and units to another.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to use Sub Accounts?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          use_sub_account_temp = tra:use_sub_accounts
                          setcursor(cursor:wait)
      
                          save_sub_id = access:subtracc.savefile()
                          access:subtracc.clearkey(sub:main_account_key)
                          sub:main_account_number = tra:account_number
                          set(sub:main_account_key,sub:main_account_key)
                          loop
                              if access:subtracc.next()
                                 break
                              end !if
                              if sub:main_account_number <> tra:account_number      |
                                  then break.  ! end if
                              Delete(subtracc)
                          end !loop
                          access:subtracc.restorefile(save_sub_id)
                          setcursor()
      
                      Of 1 ! No Button
                          tra:use_sub_accounts = use_sub_account_temp
                  End ! Case Missive
              Else
                  found# = 0
      
                  setcursor(cursor:wait)
                  save_sub_id = access:subtracc.savefile()
                  access:subtracc.clearkey(sub:main_account_key)
                  sub:main_account_number = tra:account_number
                  set(sub:main_account_key,sub:main_account_key)
                  loop
                      if access:subtracc.next()
                         break
                      end !if
                      if sub:main_account_number <> tra:account_number      |
                          then break.  ! end if
                      found# = 1
                      Break
                  end !loop
                  access:subtracc.restorefile(save_sub_id)
                  setcursor()
      
                  delete_all# = 0
                  If found# = 1
                      Case Missive('You are about to delete ALL the Sub Accounts attached to this Trade Account.'&|
                        '<13,10>'&|
                        '<13,10>If you continue all the Sub Account information will be LOST!'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              Case Missive('Are you sure you want to DELETE all the Sub Account information for this Trade Account?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      delete_all# = 1
                                      use_sub_account_temp = tra:use_sub_accounts
      
                                  Of 1 ! No Button
      
                                      delete_all# = 0
                                      tra:use_sub_accounts = use_sub_account_temp
      
                              End ! Case Missive
      
                          Of 1 ! No Button
      
                              tra:use_sub_accounts = use_sub_account_temp
                              delete_all# = 0
                      End ! Case Missive
      
                  Else
                      use_sub_account_temp    = tra:use_sub_accounts
                      delete_all# = 1
                  End
                  If delete_all# = 1
                      Do Copy_Main_Account
                  End
              End
          End!End!If error# = 0
      End
      !Check to see if user can untick sub accounts? - 4346  (DBH: 21-06-2004)
      Do SubAccountAccess
      Do Hide_fields
      brw2.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Use_Sub_Accounts, Accepted)
    OF ?AddAccountButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddAccountButton, Accepted)
      If records(glo:Queue) = 0 then
          Case Missive('You must tag an account before you can continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
      
          Case Missive('This will add all the Tagged Accounts to the list.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  !add the records to the new file
                  loop x# = 1 to records(GLO:Queue)
                      Get(GLO:Queue,x#)
                      If error() then
                          Break
                      End
                      !now get the record from the sub acc file
                      Access:SubTraCC_Alias.Clearkey(sub_ali:Account_Number_Key)
                      sub_ali:Account_Number = GLO:Pointer
                      If Access:SubTraCC_Alias.fetch(sub_ali:Account_Number_Key) = level:benign then
                          !got the record - create a new one in the new file
                          TRA1:HeadAcc        = tra:Account_Number
                          TRA1:SubAcc         = sub_ali:Account_Number
                          TRA1:SubAccName     = sub_ali:Company_Name
                          TRA1:SubAccBranch   = sub_ali:Branch
      
                          Access:TraHubAc.insert()
                      End
                  End
                  Do DASBRW::49:DASUNTAGALL
      
              Of 1 ! No Button
          End ! Case Missive
      End
      BRW47.ResetSort(1)
      BRW48.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddAccountButton, Accepted)
    OF ?RemoveAccountButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveAccountButton, Accepted)
      If records(glo:Queue2) = 0 then
          Case Missive('You must tag an account before you can continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
      
          Case Missive('This will remove all the Tagged Accounts from this User Level.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  loop x# = 1 to records(glo:Queue2)
                      Get(glo:Queue2,x#)
                      If Error() then
                          break
                      End
                      Access:TraHubAc.Clearkey(TRA1:RecordNoKey)
                      TRA1:RecordNo = GLO:Pointer2
                      If Access:TraHubAc.fetch(TRA1:RecordNoKey) = Level:Benign then
                          Delete(TraHubAc)
                      End
                  End
              Of 1 ! No Button
          End ! Case Missive
      End
      BRW47.ResetSort(1)
      BRW48.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveAccountButton, Accepted)
    OF ?Button6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      glo:Select11 = tra:account_number
      glo:Select12 = 'TRADE'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ok_pressed_temp = 1
      
      If ThisWindow.Request = InsertRecord
          !will need to update the account number
          TRA2:Account_Number = tra:Account_Number
      END
      Access:TradeAc2.update()
      
      !TB13499 - J - 15/04/15 Email address becomes compulsory if the account is RRC
      if tra:RemoteRepairCentre = 1 then 
          !email becomes compulsory 
          if clip(tra:EmailAddress) = '' then
              miss# = missive('As this is marked as being RRC the email address must be completed.','ServiceBase 3g','mexclam.jpg','OK')
              cycle
          END
      END
      
      
      
          !message('Looking to reset relocate store '&tra:Account_number)
          !if we are resetting AA20 then reset the global
          if tra:Account_Number  = 'AA20'
              if tra2:UseSparesRequest = 'YES' then
                  glo:RelocateStore = true
                  glo:RelocatedFrom = tra:SiteLocation
                  
                  Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                  loc:Active   = 1
                  loc:Location = tra:SiteLocation
                  If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
                      !Found - remember the markup from this location
                      glo:RelocatedMark_Up  = loc:OutWarrantyMarkUp
                  END
      
      
              ELSE
                  glo:RelocateStore = false
                  glo:RelocatedFrom = ''
                  glo:RelocatedMark_Up = 0
              END !if use spares
          END !if AA20
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      ok_pressed_temp = 0
      
      If ThisWindow.Request = InsertRecord
          !Abandon the new tradeac2 record
          Access:TradeAc2.cancelAutoinc()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tra:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Accepted)
      If tra:account_number <> account_number_temp
          If account_number_temp <> ''
              Case Missive('Are you sure you want to change the Account Number?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      setcursor(cursor:wait)
                      save_sub_id = access:subtracc.savefile()
                      access:subtracc.clearkey(sub:main_account_key)
                      sub:main_account_number = account_number_temp
                      set(sub:main_account_key,sub:main_account_key)
                      loop
                          if access:subtracc.next()
                             break
                          end !if
                          if sub:main_account_number <> account_number_temp      |
                              then break.  ! end if
                          pos = Position(sub:main_account_key)
                          If tra:use_sub_accounts <> 'YES'
                              sub:account_number  = tra:account_number
                          End!If tra:use_sub_accounts <> 'YES'
                          sub:main_account_number = tra:account_number
                          access:subtracc.update()
                          reset(sub:main_account_key,pos)
                      end !loop
                      access:subtracc.restorefile(save_sub_id)
                      setcursor()
                      account_number_temp = tra:account_number
                  Of 1 ! No Button
                      tra:account_number = account_number_temp
              End ! Case Missive
          End!If account_number_temp <> ''
          account_number_temp = tra:account_number
      End!If tra:account_number <> account_number_temp
      brw2.resetsort(1)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Accepted)
    OF ?ReplicateFrom
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateFrom, Accepted)
      Case Missive('Are you sure you want to replicate all the Trade Account defaults from another account?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
      
              SaveRequest#      = GlobalRequest
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              PickHeadAccountsAlias
              If Globalresponse = RequestCompleted
                  If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                      !Found
                      RecordNumber$           = tra:RecordNumber
                      AccountNumber"          = tra:Account_Number
                      tra:Record              :=: tra_ali:Record
                      tra:RecordNumber        = RecordNumber$
                      tra:ReplicateAccount    = tra_ali:Account_Number
                      tra:Account_Number       = AccountNumber"
                      Display()
      
                      !Refresh check boxes
                      Loop x# = FirstField() To LastField()
                          If x#{prop:type} = create:Check
                              Post(Event:Accepted,x#)
                          End !If x#{prop:type} = create:Check
                      End !Loop x# = FirstField() To LastField()
                  Else ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              End
              GlobalRequest     = SaveRequest#
          Of 1 ! No Button
      End ! Case Missive
      Do ReplicatedFrom
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateFrom, Accepted)
    OF ?tra:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          postcode_routine(tra:postcode,tra:address_line1,tra:address_line2,tra:address_line3)
          Select(?tra:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Postcode, Accepted)
    OF ?tra:Address_Line3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Address_Line3, Accepted)
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = tra:Address_Line3
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              tra:Postcode = sur:Postcode
              tra:Hub = sur:Hub
              ?tra:Postcode{prop:Skip} = True
              ?tra:Postcode{prop:ReadOnly} = True
              ?tra:Hub{prop:Skip} = True
              ?tra:Hub{prop:ReadOnly} = True
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Case Missive('Cannot find the select Suburb. '&|
                '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the postcode?','ServiceBase 3g',|
                             'mexclam.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      tra:Address_Line3 = ''
                      tra:Postcode = ''
                      tra:Hub = ''
                      ?tra:Postcode{prop:Skip} = True
                      ?tra:Postcode{prop:ReadOnly} = True
                      ?tra:Hub{prop:Skip} = True
                      ?tra:Hub{prop:ReadOnly} = True
                      Bryan.CompFieldColour()
                      Display()
                      Select(?tra:Address_Line3)
                  Of 2 ! Pick Button
                      tra:Address_Line3 = ''
                      tra:Postcode = ''
                      tra:Hub = ''
                      Post(Event:Accepted,?LookupSuburb)
                  Of 1 ! Continue Button
                      ?tra:Postcode{prop:Skip} = False
                      ?tra:Postcode{prop:ReadOnly} = False
                      ?tra:Hub{prop:Skip} = False
                      ?tra:Hub{prop:ReadOnly} = False
                      Bryan.CompFieldColour()
                      Select(?tra:Postcode)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Address_Line3, Accepted)
    OF ?LookupSuburb
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tra:Address_Line3 = sur:Suburb
              tra:Postcode = sur:Postcode
              tra:Hub = sur:Hub
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      ?tra:Postcode{prop:Skip} = True
      ?tra:Postcode{prop:ReadOnly} = True
      ?tra:Hub{prop:Skip} = True
      ?tra:Hub{prop:ReadOnly} = True
      Bryan.CompFieldColour()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
    OF ?tra:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Telephone_Number, Accepted)
      
          temp_string = Clip(left(tra:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:Telephone_Number    = temp_string
          Display(?tra:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Telephone_Number, Accepted)
    OF ?tra:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Fax_Number, Accepted)
      
          temp_string = Clip(left(tra:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:Fax_Number    = temp_string
          Display(?tra:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Fax_Number, Accepted)
    OF ?tra:Courier_Incoming
      IF tra:Courier_Incoming OR ?tra:Courier_Incoming{Prop:Req}
        cou:Courier = tra:Courier_Incoming
        !Save Lookup Field Incase Of error
        look:tra:Courier_Incoming        = tra:Courier_Incoming
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tra:Courier_Incoming = cou:Courier
          ELSE
            !Restore Lookup On Error
            tra:Courier_Incoming = look:tra:Courier_Incoming
            SELECT(?tra:Courier_Incoming)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupIncomingCourier
      ThisWindow.Update
      cou:Courier = tra:Courier_Incoming
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tra:Courier_Incoming = cou:Courier
          Select(?+1)
      ELSE
          Select(?tra:Courier_Incoming)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:Courier_Incoming)
    OF ?tra:Courier_Outgoing
      IF tra:Courier_Outgoing OR ?tra:Courier_Outgoing{Prop:Req}
        cou:Courier = tra:Courier_Outgoing
        !Save Lookup Field Incase Of error
        look:tra:Courier_Outgoing        = tra:Courier_Outgoing
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tra:Courier_Outgoing = cou:Courier
          ELSE
            !Restore Lookup On Error
            tra:Courier_Outgoing = look:tra:Courier_Outgoing
            SELECT(?tra:Courier_Outgoing)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupOutgoingCourier
      ThisWindow.Update
      cou:Courier = tra:Courier_Outgoing
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tra:Courier_Outgoing = cou:Courier
          Select(?+1)
      ELSE
          Select(?tra:Courier_Outgoing)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:Courier_Outgoing)
    OF ?tra:Stop_Account
      IF ?tra:Stop_Account{Prop:Checked} = True
        UNHIDE(?TRA:Allow_Cash_Sales)
      END
      IF ?tra:Stop_Account{Prop:Checked} = False
        HIDE(?TRA:Allow_Cash_Sales)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Stop_Account, Accepted)
      !TB13512 - J - 02/04/15 - don't let them unstop an RRC unless within license
          if tra:Stop_account = 'NO' and tra:RemoteRepairCentre = 1
              !check number of licences
              if LicensedRRC <= CountedRRC
                  Miss# = missive('Insufficient RRC Licences Available','ServiceBase 3g','mexclam.jpg','OK')
                  tra:Stop_Account = 'YES'
                  display(?Tra:Stop_Account)
              END !if the number counted is already at or above the number licensed
          END !if unstopping an RRC account
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Stop_Account, Accepted)
    OF ?tra:RemoteRepairCentre
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RemoteRepairCentre, Accepted)
      !TB13512 - J - 02/04/15 - don't let them create an RRC unless within license (or stopped)
          if tra:Stop_account = 'NO' and tra:RemoteRepairCentre = 1
              !check number of licences
              if LicensedRRC <= CountedRRC
                  Miss# = missive('Insufficient RRC Licences Available','ServiceBase 3g','mexclam.jpg','OK')
                  tra:RemoteRepairCentre = 0
                  display(?tra:RemoteRepairCentre)
              END !if the number counted is already at or above the number licensed
          END !if unstopping an RRC account
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RemoteRepairCentre, Accepted)
    OF ?tra:SiteLocation
      IF tra:SiteLocation OR ?tra:SiteLocation{Prop:Req}
        loc:Location = tra:SiteLocation
        !Save Lookup Field Incase Of error
        look:tra:SiteLocation        = tra:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tra:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            tra:SiteLocation = look:tra:SiteLocation
            SELECT(?tra:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = tra:SiteLocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tra:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?tra:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:SiteLocation)
    OF ?buttonReturnExchange
      ThisWindow.Update
      ! Bryan Simple Lookup
      CLEAR(sub:Record)
      sub:Account_Number = tra:RtnExchangeAccount
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
          ! Failed, set the key instead
          Clear(sub:Record)
          sub:Account_Number = tra:RtnExchangeAccount
          SET(sub:Account_Number_Key,sub:Account_Number_Key)
          Access:SUBTRACC.TryNext()
      END ! IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
      GlobalRequest = SelectRecord
      SelectSubAccounts(tra:Account_Number)
      IF (GlobalResponse = RequestCompleted)
          CHANGE(?tra:RtnExchangeAccount,sub:Account_Number)
          POST(Event:Accepted,?tra:RtnExchangeAccount)
          SELF.Request = SELF.OriginalRequest
      ELSE ! IF (GlobalResponse = RequestCompleted)
          SELECT(?buttonReturnExchange)
          SELF.Request = SELF.OriginalRequest
      END ! IF (GlobalResponse = RequestCompleted)
    OF ?ReplicateSubAccount
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateSubAccount, Accepted)
      Case Missive('Are you sure you want to replicate the selected Sub Account to other Trade Accounts?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              !Save the record incase any changes have been made
              ReplicateToHeadAccounts(tra:Account_Number,brw2.q.sub:Account_Number)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateSubAccount, Accepted)
    OF ?tra:Use_Sub_Accounts
      IF ?tra:Use_Sub_Accounts{Prop:Checked} = True
        UNHIDE(?TRA:Invoice_Sub_Accounts)
        UNHIDE(?ReplicateSubAccount)
      END
      IF ?tra:Use_Sub_Accounts{Prop:Checked} = False
        HIDE(?TRA:Invoice_Sub_Accounts)
        HIDE(?ReplicateSubAccount)
      END
      ThisWindow.Reset
    OF ?TRA2:Pre_Book_Region
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRA2:Pre_Book_Region, Accepted)
      IF TRA2:Pre_Book_Region OR ?TRA2:Pre_Book_Region{Prop:Req}
        reg:Region = TRA2:Pre_Book_Region
        !Save Lookup Field Incase Of error
        look:TRA2:Pre_Book_Region        = TRA2:Pre_Book_Region
        IF Access:REGIONS.TryFetch(reg:AccountNumberKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            TRA2:Pre_Book_Region = reg:Region
          ELSE
            !Restore Lookup On Error
            TRA2:Pre_Book_Region = look:TRA2:Pre_Book_Region
            SELECT(?TRA2:Pre_Book_Region)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      select(?CallLookup:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRA2:Pre_Book_Region, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
      reg:Region = TRA2:Pre_Book_Region
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          TRA2:Pre_Book_Region = reg:Region
          Select(?+1)
      ELSE
          Select(?TRA2:Pre_Book_Region)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?TRA2:Pre_Book_Region)
      select(?CallLookup:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
    OF ?tra:RefurbCharge
      IF ?tra:RefurbCharge{Prop:Checked} = True
        ENABLE(?Charge_Type_Group)
      END
      IF ?tra:RefurbCharge{Prop:Checked} = False
        DISABLE(?Charge_Type_Group)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RefurbCharge, Accepted)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RefurbCharge, Accepted)
    OF ?tra:InvoiceAtDespatch
      IF ?tra:InvoiceAtDespatch{Prop:Checked} = True
        UNHIDE(?tra:InvoiceType)
      END
      IF ?tra:InvoiceAtDespatch{Prop:Checked} = False
        HIDE(?tra:InvoiceType)
      END
      ThisWindow.Reset
    OF ?tra:InvoiceAtCompletion
      IF ?tra:InvoiceAtCompletion{Prop:Checked} = True
        UNHIDE(?tra:InvoiceTypeComplete)
      END
      IF ?tra:InvoiceAtCompletion{Prop:Checked} = False
        HIDE(?tra:InvoiceTypeComplete)
      END
      ThisWindow.Reset
    OF ?tra:Labour_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Labour_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Labour_VAT_Code, Accepted)
    OF ?tra:Parts_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Parts_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Parts_VAT_Code, Accepted)
    OF ?tra:Retail_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Retail_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Retail_VAT_Code, Accepted)
    OF ?tra:StoresAccount
      IF tra:StoresAccount OR ?tra:StoresAccount{Prop:Req}
        sub_ali:Account_Number = tra:StoresAccount
        !Save Lookup Field Incase Of error
        look:tra:StoresAccount        = tra:StoresAccount
        IF Access:SUBTRACC_ALIAS.TryFetch(sub_ali:Account_Number_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tra:StoresAccount = sub_ali:Account_Number
          ELSE
            !Restore Lookup On Error
            tra:StoresAccount = look:tra:StoresAccount
            SELECT(?tra:StoresAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStoresAccount
      ThisWindow.Update
      sub_ali:Account_Number = tra:StoresAccount
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tra:StoresAccount = sub_ali:Account_Number
          Select(?+1)
      ELSE
          Select(?tra:StoresAccount)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:StoresAccount)
    OF ?tra:Print_Despatch_Notes
      IF ?tra:Print_Despatch_Notes{Prop:Checked} = True
        ENABLE(?TRA:Print_Despatch_Complete)
        ENABLE(?TRA:Print_Despatch_Despatch)
        ENABLE(?TRA:Price_Despatch)
        ENABLE(?tra:HideDespAdd)
        ENABLE(?tra:UseTradeContactNo)
        ENABLE(?tra:Price_First_Copy_Only)
        ENABLE(?tra:Num_Despatch_Note_Copies)
      END
      IF ?tra:Print_Despatch_Notes{Prop:Checked} = False
        tra:Summary_Despatch_Notes = 'NO'
        tra:Despatch_Note_Per_Item = 'NO'
        tra:Print_Despatch_Despatch = 'NO'
        tra:Print_Despatch_Complete = 'NO'
        DISABLE(?TRA:Print_Despatch_Complete)
        DISABLE(?TRA:Print_Despatch_Despatch)
        DISABLE(?TRA:Price_Despatch)
        DISABLE(?tra:HideDespAdd)
        DISABLE(?tra:UseTradeContactNo)
        DISABLE(?tra:Price_First_Copy_Only)
        DISABLE(?tra:Num_Despatch_Note_Copies)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Print_Despatch_Notes, Accepted)
      Post(Event:Accepted,?tra:Print_Despatch_Despatch)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Print_Despatch_Notes, Accepted)
    OF ?tra:Print_Despatch_Despatch
      IF ?tra:Print_Despatch_Despatch{Prop:Checked} = True
        ENABLE(?Despatch_note_type)
      END
      IF ?tra:Print_Despatch_Despatch{Prop:Checked} = False
        DISABLE(?Despatch_note_type)
      END
      ThisWindow.Reset
    OF ?TRA:Summary_Despatch_Notes:2
      IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = True
        UNHIDE(?tra:IndividualSummary)
      END
      IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = False
        HIDE(?tra:IndividualSummary)
      END
      ThisWindow.Reset
    OF ?tra:Price_Despatch
      IF ?tra:Price_Despatch{Prop:Checked} = True
        UNHIDE(?tra:Price_First_Copy_Only)
      END
      IF ?tra:Price_Despatch{Prop:Checked} = False
        HIDE(?tra:Price_First_Copy_Only)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Price_Despatch, Accepted)
      If tra:price_despatch = 'NO'
          setcursor(cursor:wait)
          save_tch_id = access:trachar.savefile()
          access:trachar.clearkey(tch:account_number_key)
          tch:account_number = tra:account_number
          set(tch:account_number_key,tch:account_number_key)
          loop
              if access:trachar.next()
                 break
              end !if
              if tch:account_number <> tra:account_number |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              Delete(trachar)
          end !loop
          access:trachar.restorefile(save_tch_id)
          setcursor()
      End!If tra:price_despatch_notes = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Price_Despatch, Accepted)
    OF ?tra:Despatch_Paid_Jobs
      IF ?tra:Despatch_Paid_Jobs{Prop:Checked} = True
        UNHIDE(?tra:SetDespatchJobStatus)
      END
      IF ?tra:Despatch_Paid_Jobs{Prop:Checked} = False
        tra:SetDespatchJobStatus = 0
        HIDE(?tra:SetDespatchJobStatus)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Despatch_Paid_Jobs, Accepted)
      Post(Event:Accepted,?tra:SetDespatchJobStatus)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Despatch_Paid_Jobs, Accepted)
    OF ?tra:SetDespatchJobStatus
      IF ?tra:SetDespatchJobStatus{Prop:Checked} = True
        UNHIDE(?tra:DespatchedJobStatus)
        UNHIDE(?CompletedStatus)
      END
      IF ?tra:SetDespatchJobStatus{Prop:Checked} = False
        HIDE(?tra:DespatchedJobStatus)
        HIDE(?CompletedStatus)
      END
      ThisWindow.Reset
    OF ?tra:Despatch_Invoiced_Jobs
      IF ?tra:Despatch_Invoiced_Jobs{Prop:Checked} = True
        UNHIDE(?tra:SetInvoicedJobStatus)
      END
      IF ?tra:Despatch_Invoiced_Jobs{Prop:Checked} = False
        tra:SetInvoicedJobStatus = 0
        HIDE(?tra:SetInvoicedJobStatus)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Despatch_Invoiced_Jobs, Accepted)
      Post(Event:Accepted,?tra:SetInvoicedJobStatus)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Despatch_Invoiced_Jobs, Accepted)
    OF ?tra:SetInvoicedJobStatus
      IF ?tra:SetInvoicedJobStatus{Prop:Checked} = True
        UNHIDE(?tra:InvoicedJobStatus)
        UNHIDE(?CompletedStatus:2)
      END
      IF ?tra:SetInvoicedJobStatus{Prop:Checked} = False
        HIDE(?tra:InvoicedJobStatus)
        HIDE(?CompletedStatus:2)
      END
      ThisWindow.Reset
    OF ?tra:UseDespatchDetails
      IF ?tra:UseDespatchDetails{Prop:Checked} = True
        ENABLE(?AlternativeContactNumbers)
      END
      IF ?tra:UseDespatchDetails{Prop:Checked} = False
        DISABLE(?AlternativeContactNumbers)
      END
      ThisWindow.Reset
    OF ?tra:AltTelephoneNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltTelephoneNumber, Accepted)
      
          temp_string = Clip(left(tra:AltTelephoneNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:AltTelephoneNumber    = temp_string
          Display(?tra:AltTelephoneNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltTelephoneNumber, Accepted)
    OF ?tra:AltFaxNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltFaxNumber, Accepted)
      
          temp_string = Clip(left(tra:AltFaxNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:AltFaxNumber    = temp_string
          Display(?tra:AltFaxNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltFaxNumber, Accepted)
    OF ?LookupOBFSuburb
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupOBFSuburb, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tra:OBFSuburb = sur:Suburb
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Bryan.CompFieldColour()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupOBFSuburb, Accepted)
    OF ?tra:IncludeSaturday
      IF ?tra:IncludeSaturday{Prop:Checked} = True
        ENABLE(?SaturdayHoursOfBusiness)
      END
      IF ?tra:IncludeSaturday{Prop:Checked} = False
        DISABLE(?SaturdayHoursOfBusiness)
      END
      ThisWindow.Reset
    OF ?INsert:4
      ThisWindow.Update
      InsertTradeHoursOfBusiness(tra:RecordNumber)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?INsert:4, Accepted)
      BRW14.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?INsert:4, Accepted)
    OF ?tra:IncludeSunday
      IF ?tra:IncludeSunday{Prop:Checked} = True
        ENABLE(?SundayHoursOfBusiness)
      END
      IF ?tra:IncludeSunday{Prop:Checked} = False
        DISABLE(?SundayHoursOfBusiness)
      END
      ThisWindow.Reset
    OF ?tra:Region
      IF tra:Region OR ?tra:Region{Prop:Req}
        reg:Region = tra:Region
        !Save Lookup Field Incase Of error
        look:tra:Region        = tra:Region
        IF Access:REGIONS.TryFetch(reg:AccountNumberKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            tra:Region = reg:Region
          ELSE
            !Restore Lookup On Error
            tra:Region = look:tra:Region
            SELECT(?tra:Region)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      reg:Region = tra:Region
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          tra:Region = reg:Region
          Select(?+1)
      ELSE
          Select(?tra:Region)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:Region)
    OF ?Button:InsertHub
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:InsertHub, Accepted)
      glo:Select1 = tra:Account_Number
      SaveRequest#      = GlobalRequest
      GlobalResponse    = RequestCancelled
      GlobalRequest     = SelectRecord
      BrowseHubs
      If Globalresponse = RequestCompleted
          Access:TRAHUBS.Clearkey(trh:HubKey)
          trh:TRADEACCAccountNumber = tra:Account_Number
          trh:Hub = hub:Hub
          If Access:TRAHUBS.TryFetch(trh:HubKey)
              If Access:TRAHUBS.PrimeRecord() = Level:Benign
                  trh:TRADEACCAccountNumber = tra:Account_Number
                  trh:Hub = hub:Hub
                  If Access:TRAHUBS.Insert() = Level:Benign
                      brw34.ResetSort(1)
                  Else ! If Access:TRAHUBS.Insert() = Level:Benign
                      Access:TRAHUBS.CancelAutoInc()
                  End ! If Access:TRAHUBS.Insert() = Level:Benign
              End ! If Access:TRAHUBS.PrimeRecord() = Level:Benign
          End ! If Access:TRAHUBS.TryFetch() = Level:Benign
      Else
          !Not Selected
      End
      Display()
      GlobalRequest     = SaveRequest#
      glO:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:InsertHub, Accepted)
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::52:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::52:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::52:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::49:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::49:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::49:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::52:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::52:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::49:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::49:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020297'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020297'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020297'&'0')
      ***
    OF ?Button6
      ThisWindow.Update
      Insert_Stardard_Charges
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      glo:Select1 = ''
      glo:Select2 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?FaultCodes
      ThisWindow.Update
      BrowseTradeFaultCodes
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If tra:Print_Despatch_Notes = 'YES'
      If tra:Print_Despatch_Despatch = 'YES'
          If tra:Despatch_Note_Per_Item <> 'YES' And tra:Summary_Despatch_Notes <> 'YES'
              Case Missive('You have selected to print a Despatch Note at Despatch not have not selected a Despatch Note Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?tra:Print_Despatch_Notes)
              Cycle
          End !If tra:Despatch_Note_Per_Item <> 'YES' And tra:Summary_Despatch_Notes <> 'YES'
      End !If tra:Print_Despatch_Despatch = 'YES'
  End !tra:Print_Despatch_Notes = 'YES'
  
  
  !If New Account
  If tra:use_sub_accounts = 'NO'
      If ThisWindow.Request = Insertrecord
  
          found# = 0
  !            setcursor(cursor:wait)
  !            save_sub_id = access:subtracc.savefile()
  !            access:subtracc.clearkey(sub:main_account_key)
  !            sub:main_account_number = tra:account_number
  !            set(sub:main_account_key,sub:main_account_key)
  !            loop
  !                if access:subtracc.next()
  !                   break
  !                end !if
  !                if sub:main_account_number <> tra:account_number      |
  !                    then break.  ! end if
  !                found# = 1
  !                Break
  !            end !loop
  !            access:subtracc.restorefile(save_sub_id)
  !            setcursor()
  !
  !            If found# = 1
              Do copy_main_account
  !            End
      End !If ThisWindow.Request = Insertrecord
  Else !If tra:use_sub_accounts = 'NO'
      found# = 0
      setcursor(cursor:wait)
      save_sub_id = access:subtracc.savefile()
      access:subtracc.clearkey(sub:main_account_key)
      sub:main_account_number = tra:account_number
      set(sub:main_account_key,sub:main_account_key)
      loop
          if access:subtracc.next()
             break
          end !if
          if sub:main_account_number <> tra:account_number      |
              then break.  ! end if
          found# = 1
          Break
      end !loop
      access:subtracc.restorefile(save_sub_id)
      setcursor()
      If found# = 0
          Case Missive('You have entered a Sub Account.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tra:account_number,1)
          Cycle
      End
  End !If tra:use_sub_accounts = 'NO'
  
  If tra:company_name <> company_name_temp Or tra:account_number <> account_number_temp
      If tra:use_sub_accounts <> 'YES'
          Do Copy_Main_Account
      End!If tra:use_sub_accounts <> 'YES'
  End!If tra:company_name <> company_name_temp Or tra:account_number <> account_number_temp
  !Account Changes
  If ThisWindow.Request <> InsertRecord
      !Has this account, been used anywhere else?
      Found# = 0
      Save_tra_ali_ID = Access:TRADEACC_ALIAS.SaveFile()
      Access:TRADEACC_ALIAS.ClearKey(tra_ali:ReplicateFromKey)
      tra_ali:ReplicateAccount = tra:Account_Number
      Set(tra_ali:ReplicateFromKey,tra_ali:ReplicateFromKey)
      Loop
          If Access:TRADEACC_ALIAS.NEXT()
             Break
          End !If
          If tra_ali:ReplicateAccount <> tra:Account_Number      |
              Then Break.  ! End If
          Found# = 1
          Break
      End !Loop
      Access:TRADEACC_ALIAS.RestoreFile(Save_tra_ali_ID)
  
      Access:TRADEACC_ALIAS.Clearkey(tra_ali:RecordNumberKey)
      tra_ali:RecordNumber    = tra:RecordNumber
      If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
          !Found
          If tra:Record <> tra_ali:Record
  
  
          !TB13335 - J - 21/07/14 - Log whoever it is that is changing names of trade accounts
          if tra:Company_Name <> tra_Ali:Company_Name
              !someone has just changed this - log it
              WinName = clip(path())&'\TradeAccNameChange.log'
              WinPrint.init(WinName,append_mode)
              WinPrint.write('Company name changed from: '&clip(tra_ali:Company_Name)&' To: '&clip(tra:Company_Name)&' On: '&format(today(),@d06)& ' At: ' & format(clock(),@t04) & ' by ' &clip(glo:UserCode)&'<13,10>')
              WinPrint.close()
          END
          !TB13335 - end
  
  
              If Found# = 1
                  Case Missive('The details of this account have been changed!'&|
                    '<13,10>'&|
                    '<13,10>Warning! Other accounts have been replicated from this accounts details.'&|
                    '<13,10>Do you want to propogate these new changes to those accounts?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          Case Missive('Warning! All details, apart from the address and account number, will be replicated to the other accounts.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
  
                                  Clear(AccountQueue)
                                  Free(AccountQueue)
                                  !Write the accounts to update, to a queue
  
                                  Save_tra_ali_ID = Access:TRADEACC_ALIAS.SaveFile()
                                  Access:TRADEACC_ALIAS.ClearKey(tra_ali:ReplicateFromKey)
                                  tra_ali:ReplicateAccount = tra:Account_Number
                                  Set(tra_ali:ReplicateFromKey,tra_ali:ReplicateFromKey)
                                  Loop
                                      If Access:TRADEACC_ALIAS.NEXT()
                                         Break
                                      End !If
                                      If tra_ali:ReplicateAccount <> tra:Account_Number      |
                                          Then Break.  ! End If
                                      accque:RecordNumber = tra_ali:RecordNumber
                                      Add(AccountQueue)
  
                                  End !Loop
                                  Access:TRADEACC_ALIAS.RestoreFile(Save_tra_ali_ID)
  
                                  sav:HeadRecordNumber    = tra:RecordNumber
                                  !Save the record
                                  Access:TRADEACC.Update()
  
                                  tmp:SaveState           = Getstate(TRADEACC)
  
  
                                  !Swap it round and get the alias file
  
                                  Access:TRADEACC_ALIAS.Clearkey(tra_ali:RecordNumberKey)
                                  tra_ali:RecordNumber   = sav:HeadRecordNumber
                                  If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
                                      !Found
                                      Loop x# = 1 To Records(AccountQueue)
                                          Get(AccountQueue,x#)
  
                                          Access:TRADEACC.Clearkey(tra:RecordNumberKey)
                                          tra:RecordNumber    = accque:RecordNumber
                                          If Access:TRADEACC.Tryfetch(tra:RecordNumberKey) = Level:Benign
                                              !Found
                                              sav:RecordNumber        = tra:RecordNumber
                                              sav:AccountNumber       = tra:Account_Number
                                              sav:CompanyName         = tra:Company_Name
                                              sav:Postcode            = tra:Postcode
                                              sav:AddressLine1        = tra:Address_Line1
                                              sav:AddressLine2        = tra:Address_Line2
                                              sav:AddressLine3        = tra:Address_Line3
                                              sav:TelephoneNumber     = tra:Telephone_Number
                                              sav:FaxNumber           = tra:Fax_Number
                                              sav:EmailAddress        = tra:EmailAddress
                                              sav:ContactName         = tra:Contact_Name
                                              sav:OutgoingCourier     = tra:Courier_Outgoing
                                              sav:BranchIdentification= tra:BranchIdentification
                                              sav:SiteLocation        = tra:SiteLocation
                                              sav:WebPassword1        = tra:WebPassword1
                                              sav:WebPassword2        = tra:Password
                                              sav:RemoteRepairCentre  = tra:RemoteRepairCentre
                                              sav:StoresAccount       = tra:StoresAccount
  
                                              tra:Record :=: tra_ali:Record
  
                                              tra:RecordNumber        = sav:RecordNumber
                                              tra:Account_Number      = sav:AccountNumber
                                              tra:Company_Name        = sav:CompanyName
                                              tra:Postcode            = sav:Postcode
                                              tra:Address_Line1       = sav:AddressLine1
                                              tra:Address_Line2       = sav:AddressLine2
                                              tra:Address_Line3       = sav:AddressLine3
                                              tra:Telephone_Number    = sav:TelephoneNumber
                                              tra:Fax_Number          = sav:FaxNumber
                                              tra:EmailAddress        = sav:EmailAddress
                                              tra:Contact_Name        = sav:ContactName
                                              tra:Courier_Outgoing    = sav:OutgoingCourier
                                              tra:ReplicateAccount    = tra_ali:Account_Number
                                              tra:BranchIdentification= sav:BranchIdentification
                                              tra:SiteLocation        = sav:SiteLocation
                                              tra:WebPassword1        = sav:WebPassword1
                                              tra:Password            = sav:WebPassword2
                                              tra:RemoteRepairCentre  = sav:RemoteRepairCentre
                                              tra:StoresAccount       = sav:StoresAccount
                                              Access:TRADEACC.TryUpdate()
  
                                          Else ! If Access:TRADEACC.Tryfetch(tra:RecordNumberKey) = Level:Benign
                                              !Error
                                          End !If Access:TRADEACC.Tryfetch(tra:RecordNumberKey) = Level:Benign
                                      End !Loop x# = 1 To Records(AccountQueue)
                                  Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
  
                                  RestoreState(TRADEACC,tmp:SaveState)
                                  FreeState(TRADEACC,tmp:SaveState)
                                  Case Missive('Accounts updated.','ServiceBase 3g',|
                                                 'midea.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
  
                              Of 1 ! No Button
                          End ! Case Missive
                      Of 1 ! No Button
                  End ! Case Missive
              End !If Found# = 1
          End !If tra:Record <> tra_ali:Record
      Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
  End !ThisWindow.Request <> InsertRecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tra:Courier_Incoming
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Courier_Outgoing
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Loan_Stock_Type
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Exchange_Stock_Type
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Turnaround_Time
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:ChargeType
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:WarChargeType
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Labour_VAT_Code
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Parts_VAT_Code
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:Retail_VAT_Code
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:DespatchedJobStatus
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tra:InvoicedJobStatus
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:5
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:6
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:4
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tra:Courier_Incoming
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Incoming, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Incoming, AlertKey)
    END
  OF ?tra:Courier_Outgoing
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Outgoing, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Outgoing, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
      CASE CHOICE(?CurrentTab)
        OF 1
          ?List{PROP:FORMAT} ='67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#120L(2)|M~Company Name~@s40@#3#'
          ?Tab3{PROP:TEXT} = 'By Account Number'
        OF 2
          ?List{PROP:FORMAT} ='100L(2)|M~Branch~@s30@#2#67L(2)|M~Account Number~@s15@#1#120L(2)|M~Company Name~@s40@#3#'
          ?Tab5{PROP:TEXT} = 'By Branch'
        OF 3
          ?List{PROP:FORMAT} ='120L(2)|M~Company Name~@s40@#3#67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#'
          ?Tab4{PROP:TEXT} = 'By Company Name'
      END
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::49:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:6
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:6{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:6{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::52:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:6{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ok_pressed_temp = 0
          setcursor(cursor:wait)
          found# = 0
          save_sub_id = access:subtracc.savefile()
          access:subtracc.clearkey(sub:main_account_key)
          sub:main_account_number = tra:account_number
          set(sub:main_account_key,sub:main_account_key)
          loop
              if access:subtracc.next()
                 break
              end !if
              if sub:main_account_number <> tra:account_number      |
                  then break.  ! end if
              found# = 1
              Break
          end !loop
          access:subtracc.restorefile(save_sub_id)
          setcursor()
          If found# = 0
              If tra:use_sub_accounts = 'YES'
                  Case Missive('You must either insert at least ONE Sub Account, or untick "Use Sub Accounts" before you can cancel.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              Else!If tra:use_sub_accounts = 'YES'
                  Do Copy_Main_Account
              End!If tra:use_sub_accounts = 'YES'
          End!If found# = 0
      End!If ok_pressed_temp = 0
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      
          temp_string = Clip(left(tra:InvoicedJobStatus))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:InvoicedJobStatus    = temp_string
          Display(?tra:InvoicedJobStatus)
      Do Hide_Fields
      Do Fill_Rates
      Post(Event:accepted,?tra:print_despatch_notes)
      Post(Event:accepted,?tra:RefurbCharge)
      
      Select(?tra:account_number)
      
      !Use Sub Account
      use_sub_account_temp    = tra:use_sub_accounts
      account_number_temp     = tra:account_number
      company_name_temp       = tra:company_name
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW2.ResetSort(1)
      FDCB15.ResetQueue(1)
      FDCB18.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW2.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW14.SetQueueRecord PROCEDURE

  CODE
  IF (tbh:ExceptionType = 1)
    tmp:DayOff = 'X'
  ELSE
    tmp:DayOff = ''
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:DayOff = tmp:DayOff                      !Assign formula result to display queue


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW34.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete:2
  END


BRW34.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW45.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:4
  END


BRW45.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW47.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub_ali:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      GenericTag = ''
    ELSE
      GenericTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (GenericTag = '*')
    SELF.Q.GenericTag_Icon = 2
  ELSE
    SELF.Q.GenericTag_Icon = 1
  END


BRW47.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW47.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW47.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW47::RecordStatus  BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(47, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  !filter out all the ones that have already been selected
  
  Access:TraHubAc.Clearkey(TRA1:HeadAccSubAccKey)
  TRA1:HeadAcc = tra:Account_Number
  TRA1:SubAcc  = sub_ali:Account_Number
  If Access:TraHubAc.fetch(TRA1:HeadAccSubAccKey) = Level:benign then
      Return Record:Filtered
  End
  BRW47::RecordStatus=ReturnValue
  IF BRW47::RecordStatus NOT=Record:OK THEN RETURN BRW47::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub_ali:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::49:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW47::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW47::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW47::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(47, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW48.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = TRA1:RecordNo
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      BranchTag = ''
    ELSE
      BranchTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Branchtag = '*')
    SELF.Q.BranchTag_Icon = 2
  ELSE
    SELF.Q.BranchTag_Icon = 1
  END


BRW48.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW48.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW48.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW48::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW48::RecordStatus=ReturnValue
  IF BRW48::RecordStatus NOT=Record:OK THEN RETURN BRW48::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = TRA1:RecordNo
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::52:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW48::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW48::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW48::RecordStatus
  RETURN ReturnValue

UpdateTradeFaultCodesLookup PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::tfo:Record  LIKE(tfo:RECORD),STATIC
QuickWindow          WINDOW('Update Trade Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend A Trade Fault Code'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Account Number'),AT(240,174),USE(?TFO:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(316,174,124,10),USE(tfo:AccountNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),UPR,READONLY
                           PROMPT('Field'),AT(240,198),USE(?TFO:Field:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(316,198,124,10),USE(tfo:Field),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Description'),AT(240,222),USE(?TFO:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s60),AT(316,222,124,10),USE(tfo:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020299'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTradeFaultCodesLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tfo:Record,History::tfo:Record)
  SELF.AddHistoryField(?tfo:AccountNumber,2)
  SELF.AddHistoryField(?tfo:Field,4)
  SELF.AddHistoryField(?tfo:Description,5)
  SELF.AddUpdateFile(Access:TRAFAULO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRAFAULO.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRAFAULO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?tfo:field:prompt{prop:text} = glo:Select3
  Display()
  ! Save Window Name
   AddToLog('Window','Open','UpdateTradeFaultCodesLookup')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRAFAULO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTradeFaultCodesLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    tfo:AccountNumber = glo:select1
    tfo:Field_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020299'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020299'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020299'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

