

   MEMBER('vodr0075.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


MainStoreBackOrderReport PROCEDURE                    !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGFLAG          BYTE(0)
DASBRW::3:TAGMOUSE         BYTE(0)
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
SummaryQueue         QUEUE,PRE(sq)
CompanyName          STRING(30)
JobCount             LONG
                     END
Local_Group          GROUP,PRE(LOC)
ARCDateBooked        DATE
ARCLocation          STRING(30)
RRCLocation          STRING(30)
StatusReceivedAtRRC  STRING(30)
Account_Name         STRING(30)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(255)
CompanyName          STRING(30)
CountAllJobs         LONG
CountJobs            LONG
DesktopPath          STRING(255)
ExchNumber           LONG
FileName             STRING(255)
JobNumber            LONG
JobsExtendedInSync   BYTE
LoanNumber           LONG
Path                 STRING(255)
ProgramName          STRING(50)
RepairCentreType     STRING(3)
SectionName          STRING(30)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(50)
Version              STRING('3.1.0000')
WorkingHours         DECIMAL(7,2)
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
OPTION1              SHORT
RecordCount          LONG
WebJOB_OK            BYTE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('C')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Sheet_Queue          QUEUE,PRE(shQ)
AccountNumber        STRING(15)
SheetName            STRING(30)
RecordCount          LONG
                     END
PartQueue            QUEUE,PRE(parque)
PartNumber           STRING(30)
DateBackOrdered      DATE
                     END
PartTotalQueue       QUEUE,PRE(partotque)
PartNumber           STRING(30)
Quantity             LONG
                     END
tmp:Tag              STRING(1)
tmp:OrderNumber      LONG
tmp:OrderDate        DATE
tmp:LoopCount        LONG
Automatic            BYTE(0)
BRW2::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Main Store Back Order Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Main Store Back Order Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           LIST,AT(240,154,204,204),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11LI@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(312,112,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(316,132,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(452,230),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(452,264),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(452,298),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       PROMPT('Start Date'),AT(255,114),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(331,114,64,10),USE(LOC:StartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(399,110),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(255,136),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(331,136,64,10),USE(LOC:EndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(399,132),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('printp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
DateToString        PROCEDURE( DATE ), STRING
GetDeliveryDateAtARC        PROCEDURE( LONG ), LONG ! BOOL
HoursBetween                PROCEDURE  (TIME, TIME),LONG

NumberToColumn PROCEDURE( LONG ), STRING
SetSheetTo              PROCEDURE( STRING )
UpdateSummaryQueue  PROCEDURE( STRING )
WriteColumn PROCEDURE( STRING, LONG = False )
WriteDebug      PROCEDURE( STRING )

WorkingDaysBetween          PROCEDURE  (DATE, DATE, BYTE, BYTE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, LONG, LONG),LONG
    END !MAP
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::3:QUEUE = glo:Queue
    ADD(DASBRW::3:QUEUE)
  END
  FREE(glo:Queue)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------------------
OKButtonPressed         ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! DespatchToCustomer     = DESPATCHED
        !
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !-----------------------------------------------------------------
        DO InitColumns
        !-----------------------------------------------------------------
        !WriteDebug('OKButtonPressed(Option1="' & Option1 & '"')

        !Insert Loop Code Here!

          DO ExportSetup
!          IF LOC:FileName = '' THEN CYCLE.
          DO ExportBody
          DO ExportFinalize

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active  = True
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName = '' THEN
            EXIT
        END !IF

        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
            LOC:FileName = CLIP(LOC:FileName) & '.xls'
        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        FREE(SummaryQueue)
        CLEAR(SummaryQueue)

        !-----------------------------------------------------------------
!        DO CreateTitleSheet
        DO CreateDataSheets
!        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateTitleSheet                ROUTINE !Created Summary Sheet
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        !WriteDebug('CreateTitleSheet START')

        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:' & sheet:HeadLastCol & '").ColumnWidth'} = 20
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}

        !WriteDebug('CreateTitleSheet END')
        !-----------------------------------------------------------------
    EXIT
CreateDataSheets                ROUTINE !Created Detail Sheets
    DATA
    CODE
        !---------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        !WriteDebug('CreateDataSheets(START Option1="' & Option1 & '"')
        !---------------------------------------------------------
        If Automatic = True
            !Get account criteria for automated ini  (DBH: 06-05-2004)
            Free(glo:Queue)
            Set(tra:Account_Number_Key)
            Loop
                If Access:TRADEACC.NEXT()
                   Break
                End !If
                If tra:BranchIdentification = ''
                    Cycle
                End !If tra:BranchIdentification = ''

                If GETINI('BACKORDER',tra:Account_Number,,CLIP(Path()) & '\REPAUTO.INI') = 1
                    glo:Pointer = tra:Account_Number
                    Add(glo:Queue)
                End !If GETINI('TAT','tra:Account_Number',,CLIP(Path()) & '\REPAUTO.INI') = 1
            End !Loop

        End !If Automatic = True

        Sort(glo:Queue,glo:Pointer)
        Loop tmp:LoopCount = 1 To Records(glo:Queue)
            Get(glo:Queue,tmp:LoopCount)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = glo:Pointer
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                loc:CompanyName = Clip(tra:Company_Name) & '  ' & Clip(tra:Account_Number)
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
            DO CreateDataSheet
        End !Loop x# = 1 To Records(glo:Queue)

        !WriteDebug('CreateDataSheets End')
        !---------------------------------------------------------
    EXIT

CreateDataSheet                                                      ROUTINE
    DATA
local:CountPartsOnOrder Long
local:PartQueueCount Long
    CODE
        !---------------------------------------------------------
        !WriteDebug('CreateDataSheet(' & CLIP(LOC:CompanyName) & ')')
        !---------------------------------------------------------
        !Progress:Text    = CLIP(tra_ali:Account_Number) & '-' & CLIP(LOC:CompanyName)
        RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 100

        DO ProgressBar_LoopPre
        !---------------------------------------------------------
        If tmp:LoopCount <> 1
            !Don't create a sheet the first time
            DO XL_AddSheet
        End !If tmp:LoopCount = 1
        
        DO XL_SetWorksheetLandscape

        LOC:SectionName   = LEFT(CLIP(LOC:CompanyName), 30)
        IF CLIP(LOC:SectionName) = ''
            LOC:SectionName = 'Detailed'
        END !IF
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        LOC:CountJobs = 0
        !---------------------------------------------------------
        !Code Loop Here

        Clear(PartTotalQueue)
        Free(PartTotalQueue)
        Clear(PartQueue)
        Free(PartQueue)

        !Already going through the Account Number Queue.
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = glo:Pointer
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            !Get the stores account, and then go through all the sales for that account
            Access:RETSALES.ClearKey(ret:Date_Booked_Key)
            ret:date_booked = loc:StartDate
            Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
            Loop
                If Access:RETSALES.NEXT()
                   Break
                End !If
                If ret:date_booked > loc:EndDate       |
                    Then Break.  ! End If
                DO ProgressBar_LoopPost
                IF CancelPressed
                    EXIT
                END !IF

                If ret:Account_Number <> tra:StoresAccount
                    Cycle
                End !If ret:Account_Number <> tra:StoresAccount

                !Ok, found a sale for the right account. Let's go
                !through the parts attached and try and find some back order ones
                Access:RETSTOCK.ClearKey(res:Despatched_Key)
                res:Ref_Number           = ret:Ref_Number
                res:Despatched           = 'PEN'
                Set(res:Despatched_Key,res:Despatched_Key)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Ref_Number           <> ret:Ref_Number      |
                    Or res:Despatched           <> 'PEN'      |
                        Then Break.  ! End If
                    !Now check the Stock Part to find the supplier
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:ref_Number  = res:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found a part, add it to the queue to print the report

                        parque:PartNumber   = sto:Part_Number
                        parque:DateBackOrdered  = ret:Date_Booked
                        Add(PartQueue)

                        !Also add this part, or increment the total queue.
                        !This is so we can have a total for this part, for this customer
                        LOC:CountJobs += 1
                        Sort(PartTotalQueue,partotque:PartNumber)
                        partotque:PartNumber    = sto:Part_Number
                        Get(PartTotalQueue,partotque:PartNumber)
                        If Error()
                            partotque:PartNumber  = sto:Part_Number
                            partotque:Quantity    = res:Quantity
                            Add(PartTotalQueue)
                        Else !If Error()
                            partotque:Quantity += res:Quantity
                            Put(PartTotalQueue)
                        End !If Error()

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
            End !Loop
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        !Ok, go through the list of parts
        Sort(PartQueue,parque:PartNumber)
        Loop local:PartQueueCount = 1 To Records(PartQueue)
            Get(PartQueue,local:PartQueueCount)
            Access:STOCK.Clearkey(sto:Location_Key)
            sto:Location    = MainStoreLocation()
            sto:Part_Number = parque:PartNumber
            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                !Found the part in main store
                DO ProgressBar_LoopPost
                IF CancelPressed
                    EXIT
                END !IF

                !Now look up other orders for this part/supplier
                local:CountPartsOnOrder = 0
                tmp:OrderNumber = 0
                tmp:ORderDate   = 0

                Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
                orp:All_Received = 'NO'
                orp:Part_Number  = sto:Part_Number
                Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
                Loop
                    If Access:ORDPARTS.NEXT()
                       Break
                    End !If
                    If orp:All_Received <> 'NO'      |
                    Or orp:Part_Number  <> sto:Part_Number      |
                        Then Break.  ! End If
                    !Check the supplier from the Order
                    Access:ORDERS.ClearKey(ord:Order_Number_Key)
                    ord:Order_Number = orp:Order_Number
                    If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                        !Found
                        If ord:Supplier <> sto:Supplier
                            Cycle
                        End !If ord:Supplier <> sto:Supplier
                        local:CountPartsOnOrder += 1
                        loc:CountJobs += 1
                        tmp:OrderNumber = ord:Order_Number
                        tmp:OrderDate   = ord:Date
                        !-------------------------------------------------
                        DO WriteColumns
                        !-------------------------------------------------

                    Else!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                        !Error
                    End!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
                End !Loop

                If local:CountPartsOnOrder = 0
                    loc:CountJobs += 1
                    !Couldn't find any existing order, then write the line anyway
                    !-------------------------------------------------

                    DO WriteColumns
                    !-------------------------------------------------
                End !If local:CountPartsOnOrder = 0

            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                !Error
            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
        End !Loop x# = 1 To Records(PartQueue)

        !---------------------------------------------------------
        RecordCount = LOC:CountJobs

        DO WriteDataSummary
        !---------------------------------------------------------
        !WriteDebug('CreateDataSheet END')
        !---------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader               ROUTINE  !***SHEET HEADER***
    DATA
CurrentRow LONG(4)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("C3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Font.Size'} = 8

        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.FormulaR1C1'}      = 'Start Date:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'End Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = FORMAT(TODAY(), @D8)

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow + 2
        sheet:DataHeaderRow  = CurrentRow + 4

        sheet:HeadSummaryRow = CurrentRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
InitColumns       ROUTINE  !***SET UP COLUMN HEADINGS***
    DATA
    CODE
        !-----------------------------------------------
        !Format
        !Short Number:  '###0'
        !Long Number:   '##########'
        !String:        Chr(64)
        !Date:          excel:DateFormat

        FREE(HeaderQueue)

        head:ColumnName       = 'Part Number'
            head:ColumnWidth  = 35.00
            head:NumberFormat = Chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Description'
            head:ColumnWidth  = 35.00
            head:NumberFormat = Chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = Chr(64)
            ADD(HeaderQueue)


        head:ColumnName       = 'Stock On Hand @ Main Store'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '######0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Main Store Bin Location'
            head:ColumnWidth  = 20.00
            head:NumberFormat = Chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Qty On Backorder For Specific Customer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '######0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Backordered'
            head:ColumnWidth  = 20.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Qty on Order From Supplier'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '#####0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Supplier Name'
            head:ColumnWidth  = 35.00
            head:NumberFormat = '#####0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Main Store Order Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '#####0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Main Store Order Date'
            head:ColumnWidth  = 20.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                ROUTINE !***WRITE INTO COLUMNS***
    DATA
StatusWorkingDays LONG
    CODE
!        !-----------------------------------------------
!        !WriteDebug('WriteColumns(' & trb:Ref_Number & ')')
!
        DO ProgressBar_Loop

        IF CancelPressed
            EXIT
        END !IF
!        !-----------------------------------------------
!
        RecordCount      += 1
        !-----------------------------------------------
        LOC:CountJobs    += 1
        LOC:CountAllJobs += 1
!        
!
!        UpdateSummaryQueue(LOC:CompanyName)
!        UpdateModelQueue(job:Manufacturer, job:Model_Number)
!            !-------------------------------------------
        !Part Number
        WriteColumn('''' & sto:Part_Number)
        !Description
        WriteColumn(sto:Description)
        !Manufacturer
        WriteColumn(sto:Manufacturer)
        !Stock On Hand @ Main Store
        WriteColumn(sto:Quantity_Stock)
        !Main Store Bin Location
        WriteColumn(sto:Shelf_Location)
        !Qty On Backorder For Specific Customer
        Sort(PartTotalQueue,partotque:PartNumber)
        partotque:PartNumber = sto:Part_Number
        Get(PartTotalQueue,partotque:PartNumber)
        If Not Error()
            WriteColumn(partotque:Quantity)
        End !If Not Error()
        !Date Backordered
        WriteColumn(Format(parque:DateBackOrdered,@d06b))
        !Qty On Order From Supplier By Main Store
        WriteColumn(orp:Quantity)
        !Supplier Name
        WriteColumn(sto:Supplier)
        !Main Store ORder Number
        If tmp:OrderNumber = 0

        Else !If tmp:OrderNumber = 0
            WriteColumn(tmp:OrderNumber)
            !Main Store Order Date
            WriteColumn(Format(tmp:OrderDate,@d06b))
        End !If tmp:OrderNumber = 0

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        IF LOC:CountAllJobs < 1
            Excel{'ActiveCell.Formula'} = 'No Third Party Repairers Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
WriteSummary                                        ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

TitleRow     LONG
FirstRow     LONG
LastRow      LONG
TotalRow     LONG
    CODE
        !-----------------------------------------------------------------
        SORT(SummaryQueue, +sq:CompanyName)
        ResultsCount = RECORDS(SummaryQueue)
        !WriteDebug('WriteSummary(' & ResultsCount  & ')')
        !-----------------------------------------------------------------
        TitleRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + ResultsCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & TitleRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Third Party Repairer'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Count'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}          = 'No Despatches Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF

        LOOP QueueIndex = 1 TO RECORDS(SummaryQueue)
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = sq:CompanyName
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = sq:JobCount

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        ! Totals
        Excel{'Range("A' & TotalRow & ':' & sheet:HeadLastCol & TotalRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Total'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = '=SUM(B' & FirstRow & ':B' & LastRow & ')'

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT

ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! *** SET EXCEL FILENAME  ***
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE

    excel:ProgramName = 'National Store Backorder Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

!    If Exists(excel:FileName & '.xls')
!        Remove(excel:FileName & '.xls')
!        If Error()
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('Cannot get access to the selected document:'&|
!                '|' & Clip(excel:FileName) & ''&|
!                '|'&|
!                '|Ensure the file is not in use and try again.','ServiceBase',|
!                           'mstop.jpg','/&OK')
!                Of 1 ! &OK Button
!            End!Case Message
!            Exit
!        End !If Error()
!    End !If Exists(excel:FileName)
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & ' VODR0075 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
!        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'
!
!        IF ?progress:pcttext{prop:text} <> newPctText
!            ?progress:pcttext{prop:text} = newPctText
!
!            Display()
!        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = Clip(loc:CompanyName) !CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF 0{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------------------------------------------
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE  !**SET REPORT TITLE**
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

!        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020621'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MainStoreBackOrderReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:RETSALES.Open
  Relate:STOCK_ALIAS.Open
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      !** SET REPORT NAME**
      LOC:ProgramName       = 'National Store Backorder Report'  ! Job=2234    Cust=Vodacom
      0{PROP:Text} = CLIP(LOC:ProgramName)
      !?Tab1{PROP:Text}      = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible = False
      debug:Active  = False
  
      LOC:EndDate        = TODAY()
      LOC:StartDate      = DATE( MONTH(LOC:EndDate), 1, YEAR(LOC:EndDate) )
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      !SELECT(?Option1:Radio1)
      !Option1 = 1
  
      IF GUIMode = 1 THEN
         LocalTimeOut             = 500
         DoAll                    = 'Y'
         0{PROP:ICONIZE} = TRUE
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
      !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
      If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
          LOC:UserName = 'AUTOMATED PROCEDURE'
          Automatic = True
      End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  
      DISPLAY
  
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,tra:Account_Number_Key)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,tra:Account_Number,1,BRW2)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(tmp:Tag,BRW2.Q.tmp:Tag)
  BRW2.AddField(tra:Account_Number,BRW2.Q.tra:Account_Number)
  BRW2.AddField(tra:Company_Name,BRW2.Q.tra:Company_Name)
  BRW2.AddField(tra:RecordNumber,BRW2.Q.tra:RecordNumber)
      !Set the report to automatic run  (DBH: 07-05-2004)
  
      IF Automatic = TRUE
          IF loc:startdate = ''
            LOC:StartDate = TODAY()
            LOC:EndDate = TODAY()
          END
  
          !Daily and Weekly Report not specified  (DBH: 07-05-2004)
  
          If Command('/DAILY')
              loc:StartDate = Today()
              loc:EndDate = Today()
          End !If Command('/DAILY')
  
          !Monthly Report to run from 1st to last day of month  (DBH: 07-05-2004)
  
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      loc:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
              Loop day# = Today() To Today() + 7
                  IF day# %7 = 0
                      loc:EndDate = day#
                      Break
                  End !IF day# %7 = 0
              End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
          !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
          If Command('/MONTHLY')
              loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
          DO OKButtonPressed
          POST(Event:CloseWindow)
      END
  
  BRW2.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:RETSALES.Close
    Relate:STOCK_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020621'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020621'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020621'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      If Records(glo:Queue) = 0
          Case Missive('You must select at least one account.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else !Records(glo:Queue) = 0
          Do OKButtonPressed
      End !Records(glo:Queue) = 0
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::3:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
    ! DateKey                  KEY( lot:RefNumber,   lot:TheDate     ),DUP,NOCASE
    ! NewLocationKey           KEY( lot:RefNumber,   lot:NewLocation ),DUP,NOCASE
    ! RecordNumberKey          KEY( lot:RecordNumber                 ),NOCASE,PRIMARY

    SentToARC# = False
    LOC:ARCDateBooked = ''
    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop 
        If Access:LOCATLOG.Next()
            Break
        End !
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
        Break
        End!    
      
    End !Loop

    Return SentToARC#



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-----------------------------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-----------------------------------------------------------------
SetSheetTo              PROCEDURE( IN:SectionName )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        CLEAR(Sheet_Queue)
            shQ:AccountNumber = IN:SectionName
        GET(Sheet_Queue, +shQ:AccountNumber)

        CASE ERRORCODE()
        OF 00
            IF shQ:AccountNumber = IN:SectionName
                ! Found
                Excel{'Sheets("' & CLIP(shQ:SheetName) & '").Select'}

                RETURN
            ELSE
                ! NOT Found, Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------
        IF DoADD = True
            DO XL_AddSheet
            DO XL_SetWorksheetLandscape

            LOC:SectionName   = LEFT(CLIP(IN:SectionName), 30)
            IF CLIP(LOC:SectionName) = ''
                LOC:SectionName = 'Detailed'
            END !IF
            excel:CommentText = ''
            DO CreateSectionHeader

            !DO SetColumns

            CLEAR(Sheet_Queue)
                shQ:AccountNumber = IN:SectionName
                shQ:SheetName     = Excel{'ActiveSheet.Name'}
            ADD(Sheet_Queue, +shQ:AccountNumber)

            RETURN
        END !IF
        !-----------------------------------------------
!        DO XL_AddSheet
!            DO XL_SetWorksheetLandscape
!
!        LOC:SectionName   = LEFT(CLIP(LOC:CompanyName), 30)
!        IF CLIP(LOC:SectionName) = ''
!            LOC:SectionName = 'Detailed'
!        END !IF
!        excel:CommentText = ''
!        sheet:TempLastCol = sheet:DataLastCol
!
!        DO CreateSectionHeader
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        !WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
        !-------------------------------------------
        IF (IN:StartDate = 0)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
            RETURN 0
        ELSIF (IN:EndDate = 0)
            !WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
            RETURN 0
        ELSIF (IN:StartDate = IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
            RETURN 0
        ELSIF (IN:StartDate > IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday
            DaysPerWeek += 1
        END !IF

        IF IN:IncludeSunday
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        
        LOOP
            If (TempDate1 = IN:EndDate) Then
                !WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
                RETURN Days
            End !If

            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        
        LOOP
            If (TempDate1 = TempDate2) Then
                !WriteDebug('WorkingDaysBetween(If (TempDate1 = TempDate2) Then)')
                RETURN Days
            End !If

            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )
        !WriteDebug('WorkingDaysBetween(OK="' & Days + (Weeks * DaysPerWeek) & '")')
        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
DaysBetween   LONG
Hours         LONG
    CODE
        DaysBetween = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        Hours       = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * LOC:WorkingHours

            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        END !IF

        RETURN Hours
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
UpdateSummaryQueue  PROCEDURE( IN:CompanyName )
    CODE
        !-----------------------------------------------
        sq:CompanyName = IN:CompanyName
        GET(SummaryQueue, +sq:CompanyName)

        CASE ERRORCODE()
        OF 00
            ! Found
        OF 30
            ! NOT Found
            CLEAR(SummaryQueue)
                sq:CompanyName = IN:CompanyName
                sq:JobCount    = 0
            ADD(SummaryQueue, +sq:CompanyName)
        ELSE
            CancelPressed = True

            RETURN
        END !CASE

        sq:JobCount += 1
        PUT(SummaryQueue, +sq:CompanyName)
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!-----------------------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue

